﻿using AIS.Core;
using AIS.DataLogger.Entity;
using AIS.DataLogger.Interfaces;
using Com;
using log4net;
using SharpAIS;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AIS.Receiver
{
    public class AisPacket
    {
        public string Message { get; set; }
        public DateTime Timestamps { get; set; }
    }

    public class SaveDBPacket
    {
        public Hashtable rs { get; set; }
        public ShipInfo shipInfo { get; set; }
    }

    public class ReceiverEngine
    {
        internal readonly ILog logger = LogManager.GetLogger(typeof(ReceiverEngine));
        private bool _useLogger;
        private ManualResetEvent _aisReceivedEvent;
        private Queue<AisPacket> _aisReceivedQueues = new Queue<AisPacket>();
        private BlockingCollection<SaveDBPacket> _shipPositionQueues = new BlockingCollection<SaveDBPacket>();
        private BlockingCollection<Hashtable> _shipInfoQueues = new BlockingCollection<Hashtable>(200);

        private IFactory _factoryLogPos;
        private IFactory _factoryPostGIS;

        private bool _haveIntersection = false;
        private int _stationID;
        private string _stationName;
        private bool _start;

        private Dictionary<int, Entities.NavigationalStatus> _navStatus;
        private Dictionary<int, Entities.ShipType> _shipType;

        private JavaScriptSerializer _serializer = new JavaScriptSerializer();
        
        public ReceiverEngine(int stationID, SerialPort232 comTransponder, UdpChannel otherAisDecoder, bool haveIntersection)
        {
            ReceiverEngineMain(stationID, comTransponder, null, otherAisDecoder, haveIntersection);
        }

        public ReceiverEngine(int stationID, UdpChannel udp, UdpChannel otherAisDecoder, bool haveIntersection)
        {
            ReceiverEngineMain(stationID, null, udp, otherAisDecoder, haveIntersection);
        }

        private void ReceiverEngineMain(int stationID, SerialPort232 comTransponder, UdpChannel udp, UdpChannel otherAisDecoder, bool haveIntersection)
        {  
            _aisReceivedEvent = new ManualResetEvent(false);
            _haveIntersection = haveIntersection;

            _factoryLogPos = Program.WindsorContainer.Resolve <IFactory>("FactoryLogPos");
            _factoryPostGIS = Program.WindsorContainer.Resolve<IFactory>("FactorySpatialData");

            _useLogger = bool.Parse(ConfigurationManager.AppSettings["logger"]);
            Console.WriteLine("logger " + _useLogger);

            // cache master
            _navStatus = FillNavStatus();
            _shipType = FillShipType();

            //set station
            _stationID = stationID;
            _stationName = _factoryLogPos.GetAISReceiverPos().GetAll()[stationID].arp_name;

            Start();
            
            Console.WriteLine(DateTime.Now.ToString("G") + "-" + "Station " + _stationName + " finish initialize");

            if (comTransponder != null)
            {
                comTransponder.incommingData += new EventHandler(comTransponder_incommingData);
            }
            else if (udp != null)
            {
                udp.Received += new UdpChannel.MessageEventHandler(udp_Received);
            }
            
            List<Incoming_AIS_Fields> msgs = new List<Incoming_AIS_Fields>();
            Parallel.ForEach(_shipPositionQueues.GetConsumingEnumerable(), new ParallelOptions { MaxDegreeOfParallelism = Math.Max(Environment.ProcessorCount / 2, 1) },
                item =>
                {
                    try
                    {
                        ShipInfo si = item.shipInfo;
                        Incoming_AIS_Fields msg;
                        if (si == null)
                        {
                            si = new ShipInfo
                            {
                                mmsi = int.Parse(item.rs["MMSI"].ToString()),
                                timestampreceiver = double.Parse(item.rs["TimeStampReceiver"].ToString()),
                                received_on = DateTime.ParseExact(item.rs["TimeStampReceiver"].ToString(), "yyyyMMddHHmmssffffff", CultureInfo.InvariantCulture)
                            };

                            _factoryLogPos.GetShipInfo().InsertShipInfo(si);
                        }
                        msg = PreDBSaving(item.rs, false, ref si);
                        msgs.Add(msg);
                        if (msgs.Count > 100)
                        {
                            List<Incoming_AIS_Fields> newMsgs = new List<Incoming_AIS_Fields>(msgs);
                            Task.Factory.StartNew(() => SavingLogPos(newMsgs));
                            msgs.Clear();

                            Console.WriteLine("Antrian SaveDBQueue " + _shipPositionQueues.Count);
                            Console.WriteLine("Antrian msgs " + msgs.Count);
                        }

                        item.shipInfo = null;
                        item.rs = null;
                        item = null;
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Parallel ForEach saveDbQueue error " + ex.Message, ex);
                    }
                });
        }

        public void Start()
        {
            _start = true;

            Task.Factory.StartNew(() => DequeueAisMessage());
            Task.Factory.StartNew(() => DequeueShipInfo());
            Task.Factory.StartNew(() => KeepAisLogForAFewMonth());
        }

        public void Stop()
        {
            _start = false;
        }
        
        private void TcpChannel_incommingData(object sender, EventArgs e)
        {
            Console.WriteLine((string)sender);
        }

        private void udp_Received(string mmsID, string data)
        {
            try
            {
                if (!_start) return;
                AisPacket aisPacket = new AisPacket() { Message = data, Timestamps = DateTime.Now };
                if (_useLogger) logger.Debug(data);
                lock (_aisReceivedQueues)
                {
                    _aisReceivedQueues.Enqueue(aisPacket);
                }
                _aisReceivedEvent.Set();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }

        private Dictionary<int, Entities.NavigationalStatus> FillNavStatus()
        {
            Dictionary<int, Entities.NavigationalStatus> navStatus = _factoryLogPos.GetMasterNavStatus().GetAll();
            return navStatus;
        }

        private Dictionary<int, Entities.ShipType> FillShipType()
        {
            Dictionary<int, Entities.ShipType> shipType = _factoryLogPos.GetMasterShipType().GetAll();
            return shipType;
        }

        private void comTransponder_incommingData(object sender, EventArgs e)
        {
            if (!_start) return;
            string raw = (string)sender;

            AisPacket aisPacket = new AisPacket() { Message = raw, Timestamps = DateTime.Now };
            if (_useLogger) logger.Debug(raw);
            //antrikan message ais position
            try
            {
                lock (_aisReceivedQueues)
                {
                    _aisReceivedQueues.Enqueue(aisPacket);
                }
                _aisReceivedEvent.Set();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + " " + ex.StackTrace);
            }
        }

        private void DequeueAisMessage()
        {
            AisPacket aisPacket;
            Hashtable rs = null;
            int type;
            int mmsi = 0;
            Parser parser = new Parser();
            ShipInfo shipInfo = null;

            while (_start)
            {
                try
                {
                    lock (_aisReceivedQueues)
                    {
                        aisPacket = _aisReceivedQueues.Dequeue();
                    }
                    try
                    {
                        rs = parser.Parse(aisPacket.Message);
                    }
                    catch (Exception exParse)
                    {
                        logger.Error(exParse.Message, exParse);
                    }

                    if (rs != null)
                    {
                        if (rs.ContainsKey("MessageType") && rs.ContainsKey("MMSI"))
                        {
                            if (_useLogger) logger.Info("Receive " + rs["MMSI"]);
                            if (int.TryParse(rs["MMSI"].ToString(), out mmsi) == false) continue;
                            if (mmsi == 0) continue;
                            AddNavigationalStatusDescription(rs);
                            type = int.Parse(rs["MessageType"].ToString());

                            //cek sudah ada atau belum di tabel shipinfo
                            shipInfo = GetShipFromRepo(mmsi);

                            rs["TimeStampReceiver"] = aisPacket.Timestamps.ToString("yyyyMMddHHmmssffffff");
                            if (IsPositionType(type))
                            {
                                if (_useLogger) logger.Info("lon" + rs["Longitude"].ToString() + " lat " + rs["Latitude"].ToString());
                                rs["ZoneID"] = 0;
                                rs["ZoneName"] = "";
                                _shipPositionQueues.Add(new SaveDBPacket() { rs = rs, shipInfo = shipInfo });
                            }
                            else if (IsShipInfoType(type))
                            {
                                _shipInfoQueues.Add(rs);
                            }
                        }
                    }
                }
                catch (InvalidOperationException invEx)
                {
                    _aisReceivedEvent.Reset();
                    _aisReceivedEvent.WaitOne();
                }
                catch (Exception ex)
                {
                    // Biasanya karena ada error get value of rs dictionary. Safe to ignore.
                    logger.Error(ex.Message, ex);
                }
            }
        }

        private void DequeueShipInfo()
        {
            Hashtable rs;
            Incoming_AIS_Fields msg;
            int type;

            foreach (var item in _shipInfoQueues.GetConsumingEnumerable())
            {
                try
                {
                    rs = item;
                    if (int.TryParse(rs["MMSI"].ToString(), out int mmsi) == false) continue;
                    type = int.Parse(rs["MessageType"].ToString());
                    //cek sudah ada atau belum di tabel ShipInfo
                    var shipInfo = GetShipFromRepo(mmsi);
                    bool shipInfoNull = shipInfo == null;
                    AddShipTypeDescription(rs);
                    msg = PreDBSaving(rs, true, ref shipInfo);
                    //int? OIDShip = _factoryLogPos.GetShipInfo().GetOIDShipInfoByMMSI(int.Parse(rs["MMSI"].ToString()));
                    if (shipInfoNull)
                    {
                        _factoryLogPos.GetShipInfo().InsertShipInfo(shipInfo);
                        Console.WriteLine("Insert static " + rs["VesselName"]);
                    }
                    else
                    {
                        //if (type == 24) //-> filter it only type 24 karena type 5 sudah punya vessel name, shiptype, dll.
                        //{
                        //    Hashtable currLog = _serializer.Deserialize<Hashtable>(_factoryLogPos.GetShipInfo().GetShipInfo(OIDShip.Value).Log_AIS);
                        //    Hashtable newLog = _serializer.Deserialize<Hashtable>(msg.Log_AIS);
                        //    if (newLog.ContainsKey("VesselName") && currLog.ContainsKey("ShipType"))
                        //    {
                        //        newLog.Add("ShipType", currLog["ShipType"]);
                        //        newLog.Add("VendorID", currLog["VendorID"]);
                        //        newLog.Add("CallSign", currLog["CallSign"]);
                        //        newLog.Add("DimensionToBow", currLog["DimensionToBow"]);
                        //        newLog.Add("DimensionToStern", currLog["DimensionToStern"]);
                        //        newLog.Add("DimensionToPort", currLog["DimensionToPort"]);
                        //        newLog.Add("DimensionToStarboard", currLog["DimensionToStarboard"]);
                        //        newLog.Add("Spare", currLog["Spare"]);
                        //    }
                        //    else if (newLog.ContainsKey("ShipType") && currLog.ContainsKey("VesselName"))
                        //    {
                        //        newLog.Add("VesselName", currLog["VesselName"]);
                        //        Console.WriteLine("Update static " + newLog["VesselName"]);
                        //    }
                        //    msg.Log_AIS = _serializer.Serialize(newLog);
                        //}
                        //else
                        //{
                        //    Console.WriteLine("Update static " + rs["VesselName"]);
                        //}
                        _factoryLogPos.GetShipInfo().UpdateShipInfo(shipInfo, mmsi);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
            }
        }

        //private IQueryable<Entities.ShipFeature> GetShipWithNoName()
        //{
        //    return (IQueryable<Entities.ShipFeature>)_shipFeatureRepo.FindAll(
        //                c => c.ShipFeature.Properties.TimeStampReceiver >
        //                    double.Parse(DateTime.Now.AddMinutes(-10).ToString("yyyyMMddHHmmssffffff"))
        //                    && (c.ShipFeature.Properties.ShipName == null || c.ShipFeature.Properties.ShipName == "")
        //                    && c.ShipFeature.Properties.MMSI != 0
        //                    ).Select(c => c.ShipFeature);
        //}

        private string[] TemplateHashType5()
        {
            return new string[] { "MMSI", "VesselName", "Flagship", "CallSign", "IMONumber", "ShipType", "ShipTypeDescription",
            "DimensionToBow","DimensionToStern","Length","DimensionToPort","DimensionToStarboard","Beam","ETAMinute","ETAHour",
            "ETADay","ETAMonth","Draught","Destination","MessageType"};
        }

        public ShipInfo GetShipFromRepo(int MMSI)
        {
            try
            {
                var shipInfo = _factoryLogPos.GetShipInfo().GetShipInfoByMMSI(MMSI);
                return shipInfo;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return null;
            }
        }

        //public bool ThisShipStillServedByOtherAIServer(Entities.ShipFeatureMongoEntity shipFeatureMongo,
        //    int myStationID)
        //{
        //    double deviationTillNow = 0;
        //    if (shipFeatureMongo != null)
        //    {//jika sudah ada di cache mongodb
        //        //jika stationid == my stationid maka sudah dilayani oleh my stationid
        //        if (shipFeatureMongo.ShipFeature.Properties.StationID == myStationID)
        //        {
        //            return false;
        //        }
        //        else
        //        {//hitung waktu update terakhir
        //            double lastTimeStampDouble = shipFeatureMongo.ShipFeature.Properties.TimeStampReceiver;
        //            DateTime lastTimeStamp = DateTime.ParseExact(lastTimeStampDouble.ToString("00000000000000000000"), "yyyyMMddHHmmssffffff", CultureInfo.InvariantCulture);
        //            deviationTillNow = (DateTime.Now - lastTimeStamp).TotalMinutes;
        //        }
        //    }
        //    //Asumsi sebuah kapal masih dilayani ais station server adalah jika deviationTillNow masih <= 5 menit
        //    //Jika > 5 menit segera ambil alih
        //    return deviationTillNow <= 5;
        //}

        /// <summary>
        /// Asumsi rs.Contains("NavigationalStatus") == true
        /// </summary>
        /// <param name="rs"></param>
        private void AddNavigationalStatusDescription(Hashtable rs)
        {
            if (rs["NavigationalStatus"] == null) return;
            try
            {
                rs["NavigationalStatusDescription"] = _navStatus.First(c => c.Key == int.Parse(rs["NavigationalStatus"].ToString())).Value.description;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }

        /// <summary>
        /// Asumsi rs.Contains("ShipType") == true
        /// </summary>
        /// <param name="rs"></param>
        private void AddShipTypeDescription(Hashtable rs)
        {
            if (rs["ShipType"] == null) return;
            if (rs["ShipTypeDescription"] != null) return;
            try
            {
                Entities.ShipType stmEnt = _shipType.First(c => c.Key == int.Parse(rs["ShipType"].ToString())).Value;
                if (stmEnt != null)
                {
                    rs["ShipTypeDescription"] = stmEnt.description;
                }
                else { rs["ShipTypeDescription"] = rs["ShipType"]; }
            }
            catch (System.InvalidOperationException invalidEx)
            {
                logger.Error("ShipType tidak ada di koleksi deskripsi " + invalidEx.Message, invalidEx);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }

        private Incoming_AIS_Fields PreDBSaving(Hashtable rs, bool IsType5, ref ShipInfo shipInfo)
        {
            Incoming_AIS_Fields msg = new Incoming_AIS_Fields();

            // Tambahkan rekaman type5, ToPort, ToStarBoard, ToBow, ToStern, Draught, Destination didapatkan dari tabel ShipInfo
            if (!IsType5 && shipInfo != null)
            {
                msg.dimensiontoport = shipInfo.dimensiontoport;
                msg.dimensiontostarboard = shipInfo.dimensiontostarboard;
                msg.dimensiontobow = shipInfo.dimensiontobow;
                msg.dimensiontostern = shipInfo.dimensiontostern;
                msg.draught = shipInfo.draught;
                msg.destination = shipInfo.destination;

                msg.etaminute = shipInfo.etaminute;
                msg.etahour = shipInfo.etahour;
                msg.etaday = shipInfo.etaday;
                msg.etamonth = shipInfo.etamonth;
            }

            if (shipInfo == null && IsType5)
            {
                shipInfo = new ShipInfo();
                shipInfo.mmsi = msg.mmsi;
                shipInfo.dimensiontoport = (rs["DimensionToPort"] == null) ? 0 : double.Parse(rs["DimensionToPort"].ToString());
                shipInfo.dimensiontostarboard = (rs["DimensionToStarboard"] == null) ? 0 : double.Parse(rs["DimensionToStarboard"].ToString());
                shipInfo.dimensiontobow = (rs["DimensionToBow"] == null) ? 0 : double.Parse(rs["DimensionToBow"].ToString());
                shipInfo.dimensiontostern = (rs["DimensionToStern"] == null) ? 0 : double.Parse(rs["DimensionToStern"].ToString());

                if (rs.ContainsKey("ShipType")) {
                    int.TryParse(rs["ShipType"].ToString(), out int shipType);
                    shipInfo.shiptype = shipType;
                }               

                if (rs["ShipTypeDescription"] != null)
                {
                    shipInfo.shiptypedesc = rs["ShipTypeDescription"].ToString();
                }
            }

            if (IsType5)
            {
                // Asumsi Vessel Name, callsign, dan imo number BISA berubah
                shipInfo.vesselname = (rs["VesselName"] == null) ? "" : rs["VesselName"].ToString().Trim().Replace("'", "").Replace(" ", "");
                shipInfo.callsign = (rs["CallSign"] == null) ? "" : rs["CallSign"].ToString().Trim().Replace("'", "").Replace(" ", "");
                if (rs.ContainsKey("IMONumber"))
                {
                    int.TryParse(rs["IMONumber"].ToString().Trim().Replace("'", "").Replace(" ", ""), out int imo);
                    shipInfo.imonumber = imo;
                }
                    

                shipInfo.etamonth = (rs["ETAMonth"] == null) ? 0 : int.Parse(rs["ETAMonth"].ToString());
                shipInfo.etaday = (rs["ETADay"] == null) ? 0 : int.Parse(rs["ETADay"].ToString());
                shipInfo.etahour = (rs["ETAHour"] == null) ? 0 : int.Parse(rs["ETAHour"].ToString());
                shipInfo.etaminute = (rs["ETAMinute"] == null) ? 0 : int.Parse(rs["ETAMinute"].ToString());
                shipInfo.draught = (rs["Draught"] == null) ? 0 : double.Parse(rs["Draught"].ToString());
                shipInfo.destination = (rs["Destination"] == null) ? "" : rs["Destination"].ToString().Trim().Replace("'", "").Replace(" ", "");
            }
            else
            {
                msg.trueheading = double.Parse(rs["TrueHeading"].ToString());
                msg.speedoverground = double.Parse(rs["SpeedOverGround"].ToString());

                if (rs.ContainsKey("NavigationalStatus"))
                {
                    msg.navigationalstatus = int.Parse(rs["NavigationalStatus"].ToString());
                    msg.navstatusdesc = rs["NavigationalStatusDescription"].ToString();
                }

                if (rs.ContainsKey("RateOfTurn")) msg.rateofturn = double.Parse(rs["RateOfTurn"].ToString());
                msg.courseoverground = double.Parse(rs["CourseOverGround"].ToString());
                msg.zona_id = int.Parse(rs["ZoneID"].ToString());
                msg.zona_name = rs["ZoneName"] == null ? "" : rs["ZoneName"].ToString();

                msg.longitude = double.Parse(rs["Longitude"].ToString());
                msg.latitude = double.Parse(rs["Latitude"].ToString());

                if (rs.ContainsKey("NavigationalStatus"))
                {
                    msg.navigationalstatus = int.Parse(rs["NavigationalStatus"].ToString());
                    msg.navstatusdesc = rs["NavigationalStatusDescription"].ToString();
                }

                if (rs.ContainsKey("RateOfTurn")) msg.rateofturn = double.Parse(rs["RateOfTurn"].ToString());
                msg.courseoverground = double.Parse(rs["CourseOverGround"].ToString());
                msg.zona_id = int.Parse(rs["ZoneID"].ToString());
                msg.zona_name = rs["ZoneName"] == null ? "" : rs["ZoneName"].ToString();
            }
            
            msg.mmsi = int.Parse(rs["MMSI"].ToString());
            msg.received_on = DateTime.ParseExact(rs["TimeStampReceiver"].ToString(), "yyyyMMddHHmmssffffff", CultureInfo.InvariantCulture);
            msg.timestampreceiver = double.Parse(rs["TimeStampReceiver"].ToString());

            shipInfo.received_on = msg.received_on;
            shipInfo.timestampreceiver = msg.timestampreceiver;

            //Station
            msg.arp_id = _stationID;

            return msg;
        }

        private void SavingLogPos(List<Incoming_AIS_Fields> msgs)
        {
            logger.Info("SavingLogPos " + msgs.Count + " logs");
            //_factoryLogPos.GetAISLog().InsertBulkIncomingAIS(msgs);
            _factoryLogPos.GetAISLog().InsertBulkIncomingAISByCopyIn(msgs);
            Console.WriteLine(DateTime.Now.ToString("G") + "-" + "Saving " + _stationName + " " + msgs.Count + " data success");
            msgs.Clear(); msgs = null;
        }

        private void KeepAisLogForAFewMonth()
        {
            int lastmonthstokeep = 0 - int.Parse(ConfigurationManager.AppSettings["LastMonthsToKeep"].ToString());
            DateTime startDate = DateTime.Now.AddMonths(-7);
            logger.Info("Start Date to delete aislog.incoming_ais is " + startDate.ToShortDateString());
            while (startDate < DateTime.Now.AddMonths(lastmonthstokeep))
            {
                try
                {
                    var result = _factoryLogPos.GetAISLog().DeleteBetweenDates(startDate.AddDays(-1), startDate);
                    if (result > 0) logger.Info("Sukses hapus aislog antara tgl " + startDate.AddDays(-1).ToShortDateString() + " dan " + startDate.ToShortDateString() + " result " + result);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    startDate = startDate.AddDays(1);
                }
            }
            Thread.Sleep(60000);
            KeepAisLogForAFewMonth();
        }

        private bool IsPositionType(int type)
        {
            return ((((type == 1) || (type == 2)) || (type == 3)) || (type == 18) || (type == 19)); //1,2,3,18,19
        }

        private bool IsShipInfoType(int type)
        {
            return ((type == 5) || (type == 24)); //5,24
        }
    }
}