﻿using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;
using Com;
using log4net;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace AIS.Receiver
{
    internal class Program : IContainerAccessor
    {
        internal static readonly ILog logger = LogManager.GetLogger(typeof(Program));
        private static IWindsorContainer windsorContainer;
        //private static int STATION_PUSAT_ID;
        //private static int STATION_KJ_ID;

        public static IWindsorContainer WindsorContainer
        {
            get { return windsorContainer; }
        }

        public IWindsorContainer Container
        {
            get { return windsorContainer; }
        }

        private static void SetRegionalSettingCultureToUS()
        {
            var culture = new CultureInfo("en-US");
            culture.DateTimeFormat.ShortDatePattern = "M/d/yyyy";
            culture.DateTimeFormat.LongDatePattern = "dddd, MMMM dd, yyyy";
            culture.DateTimeFormat.ShortTimePattern = "HH:mm";
            culture.DateTimeFormat.LongTimePattern = "HH:mm:ss";

            //berikutnya NUMBER
            culture.NumberFormat.NumberDecimalSeparator = ".";
            //dan seterusnya

            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
        }

        private static void Main(string[] args)
        {
            SetRegionalSettingCultureToUS();
            /*uncomment to go service*/
            if (!bool.Parse(ConfigurationManager.AppSettings["RunAsService"].ToString()))
            {
                AISReceiverService temp = new AISReceiverService();
                InitIoC();
                temp.GoStart(args);
            }
            else
            {
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
                AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
                InitIoC();

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new AISReceiverService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }

        private static void InitIoC()
        {
            // Create the Windsor Container for IoC.
            // Supplying "XmlInterpreter" as the parameter tells Windsor
            // to look at web.config for any necessary configuration.
            windsorContainer = new WindsorContainer(new XmlInterpreter());
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            logger.Info("Closing application : " + DateTime.Now.ToString("G"));
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            logger.Error("Domain catch error : " + ex.Message, ex);
        }
    }

    public class AISReceiverService : ServiceBase
    {
        private static int STATION_PUSAT_ID;
        private ReceiverEngine _receiverPusatUseSerial;
        private ReceiverEngine _receiverPusatUseTCP;

        public AISReceiverService()
        {
            this.ServiceName = "AIS Receiver";
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;
        }

        public void GoStart(string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);

            STATION_PUSAT_ID = int.Parse(ConfigurationManager.AppSettings["STATION_PUSAT_ID"].ToString());
            if (Environment.UserInteractive) Console.Title = "AIS.Parser v" + FileVersionInfo.GetVersionInfo(
                    Assembly.GetExecutingAssembly().ManifestModule.FullyQualifiedName).ProductVersion;
            InitReceiverPusat();
            this.EventLog.WriteEntry("AIS Receiver Start.");

            //sleep to wait all thread process working
            Thread.Sleep(30000);
            if (!bool.Parse(ConfigurationManager.AppSettings["RunAsService"].ToString()))
            {
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
        }

        protected override void OnStop()
        {
            if (_receiverPusatUseSerial != null) _receiverPusatUseSerial.Stop();
            if (_receiverPusatUseTCP != null) _receiverPusatUseTCP.Stop();
            this.EventLog.WriteEntry("AIS Receiver Stop.");
        }

        private void InitReceiverPusat()
        {
            SerialPort232 comTransponderPusat;
            UdpChannel udpPusat;

            if (ConfigurationManager.AppSettings["ReceiveAISUseSerialOrTCP"].ToString() == "Serial")
            {
                if (_receiverPusatUseSerial != null)
                {
                    _receiverPusatUseSerial.Start();
                }
                else
                {
                    int baudRate = int.Parse(ConfigurationManager.AppSettings["ComTransponderBaudRate"].ToString());
                    comTransponderPusat = new SerialPort232(ConfigurationManager.AppSettings["ComTransponder"].ToString(), baudRate);
                    _receiverPusatUseSerial = new ReceiverEngine(STATION_PUSAT_ID, comTransponderPusat, null, false);
                }
            }
            else
            {
                if (_receiverPusatUseTCP != null)
                {
                    _receiverPusatUseTCP.Start();
                }
                else
                {
                    udpPusat = new UdpChannel(int.Parse(ConfigurationManager.AppSettings["TCPPort"].ToString()), UdpChannel.ActAs.Receiver,
                        bool.Parse(ConfigurationManager.AppSettings["ReceiveUDPAsAscii"]));
                    _receiverPusatUseTCP = new ReceiverEngine(STATION_PUSAT_ID, udpPusat, null, false);
                }
            }
        }
        
    }
}