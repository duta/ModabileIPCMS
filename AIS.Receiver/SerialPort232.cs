﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace AIS.Receiver
{
    public class SerialPort232 : IDisposable 
    {
        SerialPort _comm;
        public event EventHandler incommingData;
        AutoResetEvent evnGo;
        System.Threading.Thread _listenThread;
        private object _mutex;

        public SerialPort232(string PortName, int baudRate)
        {
            evnGo = new AutoResetEvent(false);
            _comm = new SerialPort(PortName, baudRate);

            if (!_comm.IsOpen)
            {
                // Open the port for communications
                _comm.Open();
            }
           
            setCommNewLine();
            afterInitiateComm(); 
        }

        private void afterInitiateComm()
        {
            createListenThread();
            createMutex();
        }

        private void createListenThread()
        {
            _listenThread = new System.Threading.Thread(Execute);
            _listenThread.Start();
        }

        
        private void createMutex()
        {
            this._mutex = new object();
        }

        private void setCommNewLine()
        {
            _comm.NewLine = "\r\n";
        }
                
         private void Execute()
        {
            Thread currentThread = Thread.CurrentThread;
            while (currentThread == _listenThread)
            {
                try
                {
                    evnGo.WaitOne(500, true);
                    string Data;
                    while (_comm.IsOpen)
                    {
                        Data = _comm.ReadLine();
                        if (Data != null && Data != "")
                        {
                            incommingData(Data, new EventArgs());
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Error SerialPort232.Execute : " + ex.Message.ToUpper());
                }
            }
        }

         public bool sendData(string data)
         {
             string errMsg;

             lock (this._mutex)
             {
                 if (this._comm != null)
                 {
                     if (!_comm.IsOpen)
                     {
                         // Open the port for communications
                         _comm.Open();
                     }

                     try
                     {
                         _comm.Write(data);
                     }
                     catch (TimeoutException e)
                     {
                         errMsg = "Error SerialPort232.sendData : " + e +
                                 Environment.NewLine + _comm.PortName;
                         System.Diagnostics.Debug.WriteLine(errMsg);
                         //Logger.Log("SerialPortComm", Thread.CurrentThread.ManagedThreadId, errMsg);
                         return false;
                     }
                 }
             }

             return true;
         }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // ASB : The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                lock (this._mutex)
                {
                    try
                    {
                        if (this.evnGo != null)
                        {
                            this.evnGo.Close();
                            this.evnGo = null;
                        }

                        if (_comm.IsOpen) _comm.Close();
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine("SerialPort232 Dispose : " + ex.Message);
                    }
                    finally
                    {
                        _comm.Dispose();
                        _comm = null;
                        _listenThread.Abort();
                    }

                }

            }

        }

        // ASB :  Use C# finalizer syntax for finalization code.
        ~SerialPort232()
        {
            // Simply call Dispose(false).
            Dispose(false);
        }

        #endregion
    }
}
