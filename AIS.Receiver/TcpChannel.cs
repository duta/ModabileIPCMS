﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace AIS.Receiver
{
    // State object for reading client data asynchronously
    public class StateObject
    {
        // Client  socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 1024;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }
    
    public class TcpChannel
    {
        // Thread signal.
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        public static event EventHandler incommingData;

        public TcpChannel()
        {
        }

        public static void StartListening(int port)
        {
        // Create a new listener on port 8000. 
            TcpListener listener = 
                new TcpListener(IPAddress.Parse("10.9.3.12"), port); 
 
            Console.WriteLine("About to initialize port."); 
            listener.Start(); 
            Console.WriteLine("Listening for a connection..."); 
 
            try 
            {
                while (true)
                {
                    // Wait for a connection request, and return a TcpClient 
                    // initialized for communication. 
                    using (TcpClient client = listener.AcceptTcpClient())
                    {                        
                        //Console.WriteLine("Connection accepted.");
                        // Retrieve the network stream. 
                        NetworkStream stream = client.GetStream();

                        // Create a BinaryWriter for writing to the stream. 
                        using (BinaryWriter w = new BinaryWriter(stream))
                        {                            
                            // Create a BinaryReader for reading from the stream. 
                            using (BinaryReader r = new BinaryReader(stream))
                            {
                                string readStr = r.ReadString();
                                Console.WriteLine(readStr);
                                //incommingData(r.ReadString() , new EventArgs());
                                //if (r.ReadString() ==
                                //    Recipe10_10Shared.RequestConnect)
                                //{
                                //    w.Write(Recipe10_10Shared.AcknowledgeOK);
                                //    Console.WriteLine("Connection completed.");

                                //    while (r.ReadString() !=
                                //        Recipe10_10Shared.Disconnect) { }

                                //    Console.WriteLine(Environment.NewLine);
                                //    Console.WriteLine("Disconnect request received.");
                                //}
                                //else
                                //{
                                //    Console.WriteLine("Can't complete connection.");
                                //}
                            }
                        }
                    }
    
                }
                
                Console.WriteLine("Connection closed.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                // Close the underlying socket (stop listening for new requests). 
                listener.Stop();
                Console.WriteLine("Listener stopped.");
            }

            // Wait to continue. 
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("Main method complete. Press Enter");
            Console.ReadLine();
        }

        //public static void StartListening(int port)
        //{
        //    // Data buffer for incoming data.
        //    byte[] bytes = new Byte[1024];

        //    // Establish the local endpoint for the socket.
        //    // The DNS name of the computer
        //    // running the listener is "localhost".
        //    IPAddress ipAddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        //    IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);

        //    // Create a TCP/IP socket.
        //    Socket listener = new Socket(AddressFamily.InterNetwork,
        //        SocketType.Stream, ProtocolType.Tcp);

        //    // Bind the socket to the local endpoint and listen for incoming connections.
        //    try
        //    {
        //        listener.Bind(localEndPoint);
        //        listener.Listen(100);

        //        while (true)
        //        {
        //            // Set the event to nonsignaled state.
        //            allDone.Reset();

        //            // Start an asynchronous socket to listen for connections.
        //            Console.WriteLine("Listening IP " + ipAddress.ToString () + " port "+ port);
        //            Console.WriteLine("Waiting for a connection...");
        //            listener.BeginAccept(new AsyncCallback(AcceptCallback),listener);

        //            // Wait until a connection is made before continuing.
        //            allDone.WaitOne();
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //    }
        //}

        //public static void AcceptCallback(IAsyncResult ar)
        //{
        //    // Signal the main thread to continue.
        //    allDone.Set();

        //    // Get the socket that handles the client request.
        //    Socket listener = (Socket)ar.AsyncState;
        //    Socket handler = listener.EndAccept(ar);

        //    // Create the state object.
        //    StateObject state = new StateObject();
        //    state.workSocket = handler;
        //    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
        //        new AsyncCallback(ReadCallback), state);
        //}

        //public static void ReadCallback(IAsyncResult ar)
        //{
        //    String content = String.Empty;

        //    // Retrieve the state object and the handler socket
        //    // from the asynchronous state object.
        //    StateObject state = (StateObject)ar.AsyncState;
        //    Socket handler = state.workSocket;

        //    // Read data from the client socket. 
        //    int bytesRead = handler.EndReceive(ar);

        //    if (bytesRead > 0)
        //    {
        //        // There  might be more data, so store the data received so far.
        //        state.sb.Append(Encoding.ASCII.GetString(
        //            state.buffer, 0, bytesRead));

        //        // Check for end-of-file tag. If it is not there, read 
        //        // more data.
        //        content = state.sb.ToString();
        //        if (content.IndexOf("\r\n") > -1)
        //        {
        //            // All the data has been read from the 
        //            // client. Display it on the console.
        //            Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
        //                content.Length, content);
        //            // Echo the data back to the client.
        //            //Send(handler, content);
        //            //if (!string.IsNullOrEmpty(content))
        //            //{
        //                //incommingData(content, new EventArgs());
        //                //allDone.Set();
        //            //}
        //        }
        //        else
        //        {
        //            // Not all data received. Get more.
        //            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
        //            new AsyncCallback(ReadCallback), state);
        //        }
        //    }
        //}
    }

}
