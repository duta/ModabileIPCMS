﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using DBProvider;
using Npgsql;
using AIS.DataLogger.Interfaces;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger
{
    internal class ShipType : Interfaces.IShipType  
    {
          PostgreSQL _ps;

          public ShipType(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }


        public Dictionary<int, Core.Entities.ShipType> GetAll()
        {
            Dictionary<int, Core.Entities.ShipType> result = new Dictionary<int, Core.Entities.ShipType>();
            Core.Entities.ShipType shiptype_item;
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id_shiptype, description");
            sb.Append(" from ship_type ");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    shiptype_item = new Core.Entities.ShipType();
                    shiptype_item.id_shiptype = int.Parse(reader["id_shiptype"].ToString());
                    shiptype_item.description = reader["description"].ToString();
                    result[shiptype_item.id_shiptype] = shiptype_item;
                }
            }

            reader.Close();
            _ps.CloseConnection();

            return result;
        }
    }
}
