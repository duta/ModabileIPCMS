﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using DBProvider;

namespace AIS.DataLogger
{
    internal class AISReceiverPos : Interfaces.IAISReceiverPos
    {
        PostgreSQL _ps;

        public AISReceiverPos(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public Dictionary<int, Core.Entities.AISReceiverPos> GetAll()
        {
            Dictionary<int, Core.Entities.AISReceiverPos> result = new Dictionary<int, Core.Entities.AISReceiverPos>();
            Core.Entities.AISReceiverPos item;
            StringBuilder sb = new StringBuilder();
            sb.Append(" select arp_id, arp_name");
            sb.Append(" from ais_receiver_pos ");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    item = new Core.Entities.AISReceiverPos();
                    item.arp_id = int.Parse(reader["arp_id"].ToString());
                    item.arp_name = reader["arp_name"].ToString();
                    result[item.arp_id] = item;
                }
            }

            reader.Close();
            _ps.CloseConnection();

            return result;
        }
    }
}
