﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using DBProvider;
using Npgsql;
using AIS.DataLogger.Interfaces;
using AIS.DataLogger.Entity;
using Dapper;

namespace AIS.DataLogger
{    
    internal class ShipInfo : Interfaces .IShipInfo
    {
        PostgreSQL _ps;

        public ShipInfo(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public bool IsThereShipInfoByMMSI(int mmsi)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(*) jml ");
            sb.Append(" from ship_info ");
            sb.Append(" where mmsi = '"+ mmsi + "' ");

            _ps.OpenConnectionAndCreateCommand();
            string result = _ps.ExecuteScalar(sb.ToString());
            return (result != "0");
        }

        public int?  GetOIDShipInfoByMMSI(int mmsi)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select oid ");
            sb.Append(" from ship_info ");
            sb.Append(" where mmsi::text = '" + mmsi + "' ");

            _ps.OpenConnectionAndCreateCommand();
            string result = _ps.ExecuteScalar(sb.ToString());
            if (result == "") return null;
            return int.Parse (result);
        }

		public Entity.Incoming_AIS GetShipInfo(int oid)
		{
			Entity.Incoming_AIS result = new Entity.Incoming_AIS();
			StringBuilder sb = new StringBuilder();
			sb.Append("select * from ship_info where oid = '" + oid + "'");

			_ps.OpenConnectionAndCreateCommand();
			NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

			if(reader.HasRows)
			{
				if(reader.Read())
				{
					result.Log_AIS = reader["log_ais"].ToString();
					result.MMSI = int.Parse(reader["mmsi"].ToString());
					result.Receive_On = (DateTime)reader["received_on"];
				}
			}

			reader.Close();
			_ps.CloseConnection();
			return result;
		}

        public Entity.ShipInfo GetShipInfoByMMSI(int mmsi)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from ship_info where mmsi = :mmsi");
            _ps.OpenConnectionAndCreateCommand();
             var result = _ps.Conn.Query<Entity.ShipInfo>(sb.ToString(), new { mmsi = mmsi }).FirstOrDefault();
            _ps.CloseConnection();
            return result;
        }

        public int InsertShipInfo(Entity.ShipInfo shipinfo)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" INSERT INTO ship_info( ");
            sb.Append("             received_on, mmsi, etaminute, repeatindicator, etamonth, dte,  ");
            sb.Append("             spare, dimensiontostern, positionfixtype, messagetype, draught,  ");
            sb.Append("             vesselname, destination, etaday, shiptypedesc, dimensiontoport,  ");
            sb.Append("             timestampreceiver, imonumber, dimensiontobow, etahour, aisversion,  ");
            sb.Append("             callsign, dimensiontostarboard, shiptype) ");
            sb.Append("     VALUES (:received_on, :mmsi, :etaminute, :repeatindicator, :etamonth, :dte,  ");
            sb.Append("             :spare, :dimensiontostern, :positionfixtype, :messagetype, :draught,  ");
            sb.Append("             :vesselname, :destination, :etaday, :shiptypedesc, :dimensiontoport,  ");
            sb.Append("             :timestampreceiver, :imonumber, :dimensiontobow, :etahour, :aisversion,  ");
            sb.Append("             :callsign, :dimensiontostarboard, :shiptype ) ");

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.Conn.Execute(sb.ToString(), new
            {
                received_on = shipinfo.received_on,
                mmsi = shipinfo.mmsi,
                etaminute = shipinfo.etaminute,
                repeatindicator = shipinfo.repeatindicator,
                etamonth = shipinfo.etamonth,
                dte = shipinfo.dte,
                spare = shipinfo.spare,
                dimensiontostern = shipinfo.dimensiontostern,
                positionfixtype = shipinfo.positionfixtype,
                messagetype = shipinfo.messagetype,
                draught = shipinfo.draught,
                vesselname = shipinfo.vesselname,
                destination = shipinfo.destination,
                etaday = shipinfo.etaday,
                shiptypedesc = shipinfo.shiptypedesc,
                dimensiontoport = shipinfo.dimensiontoport,
                timestampreceiver = shipinfo.timestampreceiver,
                imonumber = shipinfo.imonumber,
                dimensiontobow = shipinfo.dimensiontobow,
                etahour = shipinfo.etahour,
                aisversion = shipinfo.aisversion,
                callsign = shipinfo.callsign,
                dimensiontostarboard = shipinfo.dimensiontostarboard,
                shiptype = shipinfo.shiptype
            });
            _ps.CloseConnection();
            return returnValue;
        }

        public int UpdateShipInfo(Entity.ShipInfo shipinfo, int mmsi)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" UPDATE ship_info ");
            sb.Append("    SET received_on=:received_on, etaminute=:etaminute,  etamonth=:etamonth,  ");
            sb.Append("        draught=:draught, destination=:destination, etaday=:etaday,  ");
            sb.Append("        timestampreceiver=:timestampreceiver,   ");
            sb.Append("        etahour=:etahour   ");
            sb.Append("  WHERE mmsi = :mmsi ");
            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.Conn.Execute(sb.ToString(), new
            {
                received_on = shipinfo.received_on,
                etaminute = shipinfo.etaminute,
                etamonth = shipinfo.etamonth,
                draught = shipinfo.draught,
                destination = shipinfo.destination,
                etaday = shipinfo.etaday,
                timestampreceiver = shipinfo.timestampreceiver,
                etahour = shipinfo.etahour,
                mmsi = mmsi
            });
            _ps.CloseConnection();
            return returnValue;
        }


        public List<Incoming_AIS> GetDetectedShipsInCurrentTime()
        {
            Incoming_AIS result;
            List<Incoming_AIS> results = new List<Incoming_AIS>();
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append("     log_ais ");
            sb.Append(" from ship_info ");
            sb.Append(" where ");
            sb.Append("     received_on > now()  - interval '1 hour' ");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result = new Incoming_AIS();
                    result.Log_AIS = reader["log_ais"].ToString();                   
                    results.Add(result);
                }
            }

            reader.Close();
            _ps.CloseConnection();
            
            return results;
        }


        public LabelValue[] GetLabelValueOfMMSIShipName(string filter)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select mmsi|| ' ' ||vesselname lbl from ship_info  ");
            sb.Append(" where mmsi::text like '%" + filter + "%' ");
            sb.Append(" union ");
            sb.Append(" select mmsi|| ' ' ||vesselname lbl from ship_info  ");
            sb.Append(" where vesselname like '%" + filter.ToUpper() + "%' ");

            LabelValue[] result;
            List<LabelValue> results = new List<LabelValue>();
            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var lblValue = new LabelValue();
                    lblValue.label = reader["lbl"].ToString().Trim();
                    lblValue.value = reader["lbl"].ToString().Trim();
                    results.Add(lblValue);
                }
            }

            reader.Close();
            _ps.CloseConnection();

            result = results.ToArray();
            return result;
        }



        public int Delete(int mmsi)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from ship_info ");
            sb.Append(" where mmsi = :valmmsi ");

            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valmmsi", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params = { paramId };
            paramId.Value = mmsi;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }


        public string GetKodeKplFromKplAISSiuk(string CallSign)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select mkpl_kode ");
            sb.Append(" from kapal_ais_siuk ");
            sb.Append(" where mkpl_call_sign = :valmkplcallsign ");
            
            NpgsqlParameter paramCS;
            paramCS = new NpgsqlParameter("valmkplcallsign", NpgsqlTypes.NpgsqlDbType.Text);
            NpgsqlParameter[] Params = { paramCS };
            paramCS.Value = CallSign;

            _ps.OpenConnectionAndCreateCommand();
            string result = _ps.ExecuteScalar(sb.ToString(), Params);
            return result.Trim();

        }

        List<Entity.ShipInfo> IShipInfo.GetAll()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append("     * ");
            sb.Append(" from ship_info ");
            sb.Append(" where mmsi not in (525015977, 525015867)  ");
            _ps.OpenConnectionAndCreateCommand();
            List<Entity.ShipInfo> results = _ps.Conn.Query<Entity.ShipInfo>(sb.ToString()).ToList();
            _ps.CloseConnection();
            return results;
        }
    }
}
