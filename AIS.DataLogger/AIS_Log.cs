﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Data;
using DBProvider;
using Npgsql;
using Dapper;
using AIS.DataLogger.Interfaces;
using AIS.DataLogger.Entity;
using AIS.Core; 

namespace AIS.DataLogger
{
    internal class AIS_Log : IAIS_Log
    {
        PostgreSQL _ps;

        public AIS_Log(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        #region IAIS_Log Members

        public int DeleteLTDate(DateTime tglAkhirLog)
        {
            int returnValue;
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from incoming_ais ");
            sb.Append(" where received_on <  :valtglakhir  ");
            _ps.OpenConnectionAndCreateCommand();
            _ps.Comm.Parameters.AddWithValue(":valtglakhir", tglAkhirLog);
            returnValue = _ps.ExecuteNonQuery(sb.ToString());
            return returnValue;
        }

        public int InsertBulkIncomingAIS(List<Incoming_AIS> incoming)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into incoming_ais (log_ais, received_on, mmsi, zona_id, arp_id) ");
            sb.Append(" values (:vallogais, :valreceivedon, :valmmsi, :valzonaid, :valarpid) ");

            NpgsqlParameter paramLogAIS, paramReceivedOn, paramMMSI, paramZonaID, paramArpId;
            paramLogAIS = new NpgsqlParameter("vallogais", NpgsqlTypes.NpgsqlDbType.Text);
            paramReceivedOn = new NpgsqlParameter("valreceivedon", NpgsqlTypes.NpgsqlDbType.Timestamp);
            paramMMSI = new NpgsqlParameter("valmmsi", NpgsqlTypes.NpgsqlDbType.Integer);
            paramZonaID = new NpgsqlParameter("valzonaid", NpgsqlTypes.NpgsqlDbType.Integer);
			paramArpId = new NpgsqlParameter("valarpid", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params = { paramLogAIS, paramReceivedOn, paramMMSI, paramZonaID,paramArpId };

            NpgsqlTransaction trans = null;

			try
			{
				_ps.OpenConnectionAndCreateCommand();
				trans = _ps.Comm.Connection.BeginTransaction();
				_ps.Comm.Transaction = trans;
				_ps.Comm.Parameters.AddRange(Params);
				_ps.Comm.CommandText = sb.ToString();

				foreach(var item in incoming)
				{
                    if (item == null) continue;
                    paramLogAIS.Value = item.Log_AIS;
                    paramReceivedOn.Value = item.Receive_On;
                    paramMMSI.Value = item.MMSI;
                    paramZonaID.Value = item.Zone.ID;
                    paramArpId.Value = item.Arp_Id;
                    returnValue += _ps.Comm.ExecuteNonQuery();                    					
				}
				trans.Commit();
			}
			catch(Exception ex)
			{
				trans.Rollback();
				throw new ApplicationException("Gagal bulk insert position", ex);
			}
			finally
			{
				_ps.CloseConnection();
			}

            return returnValue; 
        }

        public int InsertBulkIncomingAIS(List<Incoming_AIS_Fields> incoming)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into incoming_ais (log_ais, received_on, mmsi, zona_id, arp_id) ");
            sb.Append(" values (:vallogais, :valreceivedon, :valmmsi, :valzonaid, :valarpid) ");

            NpgsqlParameter paramLogAIS, paramReceivedOn, paramMMSI, paramZonaID, paramArpId;
            paramLogAIS = new NpgsqlParameter("vallogais", NpgsqlTypes.NpgsqlDbType.Text);
            paramReceivedOn = new NpgsqlParameter("valreceivedon", NpgsqlTypes.NpgsqlDbType.Timestamp);
            paramMMSI = new NpgsqlParameter("valmmsi", NpgsqlTypes.NpgsqlDbType.Integer);
            paramZonaID = new NpgsqlParameter("valzonaid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramArpId = new NpgsqlParameter("valarpid", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params = { paramLogAIS, paramReceivedOn, paramMMSI, paramZonaID, paramArpId };

            NpgsqlTransaction trans = null;

            try
            {
                _ps.OpenConnectionAndCreateCommand();
                trans = _ps.Comm.Connection.BeginTransaction();
                _ps.Comm.Transaction = trans;
                _ps.Comm.Parameters.AddRange(Params);
                _ps.Comm.CommandText = sb.ToString();

                foreach (var item in incoming)
                {
                    //paramLogAIS.Value = item.log_ais;
                    paramReceivedOn.Value = item.received_on;
                    paramMMSI.Value = item.mmsi;
                    paramZonaID.Value = item.zona_id;
                    paramArpId.Value = item.arp_id;
                    returnValue += _ps.Comm.ExecuteNonQuery();
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw new ApplicationException("Gagal bulk insert position", ex);
            }
            finally
            {
                _ps.CloseConnection();
            }

            return returnValue;
        }

        public int InsertBulkIncomingAISByCopyIn(List<Incoming_AIS_Fields> incoming)
        {
            int returnValue = 0;
            _ps.OpenConnectionAndCreateCommand();
            StringBuilder sb = new StringBuilder();
            sb.Append(" COPY incoming_ais ( ");
            sb.Append("     received_on, zona_id, mmsi, arp_id, messagetype, slottimeout,  ");
            sb.Append("     iais_timestamp, dimensiontobow, navigationalstatus, trueheading,  ");
            sb.Append("     positionaccuracy, repeatindicator, longitude, courseoverground,  ");
            sb.Append("     maneuverindicator, navstatusdesc, zonename, dimensiontostern,  ");
            sb.Append("     destination, dimensiontostarboard, spare, syncstate, timestampreceiver,  ");
            sb.Append("     submessage, dimensiontoport, draught, rateofturn, speedoverground,  ");
            sb.Append("     latitude, raimflag, etamonth, etaday, etahour, etaminute   ");
            sb.Append(" ) FROM STDIN ");
            _ps.Comm.CommandText = sb.ToString();
            NpgsqlCopySerializer serializer = new NpgsqlCopySerializer(_ps.Conn);
            NpgsqlCopyIn copyIn = new NpgsqlCopyIn(_ps.Comm, _ps.Conn, serializer.ToStream);

            try
            {
                copyIn.Start();
                foreach (var item in incoming)
                {
                    serializer.AddDateTime(item.received_on);
                    serializer.AddInt32(item.zona_id);
                    serializer.AddInt32(item.mmsi);
                    serializer.AddInt32(item.arp_id);
                    serializer.AddInt32(item.messagetype);
                    serializer.AddInt32(item.slottimeout);
                    serializer.AddInt32(item.iais_timestamp);
                    serializer.AddNumber(item.dimensiontobow);
                    serializer.AddInt32(item.navigationalstatus);
                    serializer.AddNumber(item.trueheading);
                    serializer.AddInt32(item.positionaccuracy);
                    serializer.AddInt32(item.repeatindicator);
                    serializer.AddNumber(item.longitude);
                    serializer.AddNumber(item.courseoverground);
                    serializer.AddInt32(item.maneuverindicator);
                    serializer.AddString(item.navstatusdesc);
                    serializer.AddString(item.zona_name);
                    serializer.AddNumber(item.dimensiontostern);
                    serializer.AddString(item.destination == null ? "" : item.destination);
                    serializer.AddNumber(item.dimensiontostarboard);
                    serializer.AddInt32(item.spare);
                    serializer.AddInt32(item.syncstate);
                    serializer.AddNumber(item.timestampreceiver);
                    serializer.AddString(item.submessage == null ? "" : item.submessage);
                    serializer.AddNumber(item.dimensiontoport);
                    serializer.AddNumber(item.draught);
                    serializer.AddNumber(item.rateofturn);
                    serializer.AddNumber(item.speedoverground);
                    serializer.AddNumber(item.latitude);
                    serializer.AddNumber(item.raimflag);
                    serializer.AddInt32(item.etamonth);
                    serializer.AddInt32(item.etaday);
                    serializer.AddInt32(item.etahour);
                    serializer.AddInt32(item.etaminute);

                    serializer.EndRow();
                    serializer.Flush();
                }
                copyIn.End();
                serializer.Close();
            }
            catch (Exception ex)
            {
                try
                {
                    copyIn.Cancel("Undo copy on exception.");
                }
                catch (NpgsqlException e2)
                {
                    // we should get an error in response to our cancel request:
                    if (!e2.BaseMessage.Contains("Undo copy on exception."))
                    {
                        throw new Exception("Failed to cancel copy:" + e2 + " upon failure: " + ex);
                    }
                }
            }
            finally
            {
                _ps.CloseConnection();
            }
            return returnValue;
        }

        public int InsertBulkIncomingAISTemp(List<Incoming_AIS_Fields> incoming)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into incoming_ais_temp (log_ais, received_on, mmsi, zona_id, arp_id) ");
            sb.Append(" values (:vallogais, :valreceivedon, :valmmsi, :valzonaid, :valarpid) ");

            NpgsqlParameter paramLogAIS, paramReceivedOn, paramMMSI, paramZonaID, paramArpId;
            paramLogAIS = new NpgsqlParameter("vallogais", NpgsqlTypes.NpgsqlDbType.Text);
            paramReceivedOn = new NpgsqlParameter("valreceivedon", NpgsqlTypes.NpgsqlDbType.Timestamp);
            paramMMSI = new NpgsqlParameter("valmmsi", NpgsqlTypes.NpgsqlDbType.Integer);
            paramZonaID = new NpgsqlParameter("valzonaid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramArpId = new NpgsqlParameter("valarpid", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params = { paramLogAIS, paramReceivedOn, paramMMSI, paramZonaID, paramArpId };

            NpgsqlTransaction trans = null;

            try
            {
                _ps.OpenConnectionAndCreateCommand();
                trans = _ps.Comm.Connection.BeginTransaction();
                _ps.Comm.Transaction = trans;
                _ps.Comm.Parameters.AddRange(Params);
                _ps.Comm.CommandText = sb.ToString();

                foreach (var item in incoming)
                {
                    //paramLogAIS.Value = item.log_ais;
                    paramReceivedOn.Value = item.received_on;
                    paramMMSI.Value = item.mmsi;
                    paramZonaID.Value = item.zona_id;
                    paramArpId.Value = item.arp_id;
                    returnValue += _ps.Comm.ExecuteNonQuery();
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw new ApplicationException("Gagal bulk insert position", ex);
            }
            finally
            {
                _ps.CloseConnection();
            }

            return returnValue;
        }

         public Incoming_AIS LastPositionByMMSI(string mmsi)
        {
            Incoming_AIS result = null;
            StringBuilder sb = new StringBuilder();
            sb.Append(" select log_ais, max(substring(log_ais from position('TimeStampReceiver' in log_ais) + 20 for 14)) max_date "); 
            sb.Append(" from incoming_ais "); 
            sb.Append(" where log_ais like  '%\\\"MMSI\\\":" + mmsi + "%'  "); 
            sb.Append("       and received_on = (select max(received_on) "); 
            sb.Append(" 	from incoming_ais ");
            sb.Append(" 	where log_ais like  '%\\\"MMSI\\\":" + mmsi + "%' ) 	 "); 
            sb.Append(" group by log_ais "); 
            sb.Append(" order by max_date desc limit 1  ");

            _ps.OpenConnectionAndCreateCommand();
            string log_ais = _ps.ExecuteScalar(sb.ToString());
            if (log_ais != "")
            {
                result = new Incoming_AIS();
                result.Log_AIS = log_ais;
            }
            return result;
        }

        public Entities.ShipDisplayFeatureCollection RecordedPositionByMMSIAndDate(string mmsi, string from, string to)
        {
            Entities.ShipDisplayFeatureCollection result = new Entities.ShipDisplayFeatureCollection();
            Entities.ShipProperty shipProperty;
            Entities.ShipFeature shipFeature;
            List<Entities.ShipFeature> shipFeatures = new List<Entities.ShipFeature>();
            GeoJSON.Point shipPoint;
            StringBuilder sb = new StringBuilder();

            sb.Append(" select si.mmsi, si.vesselname ship_name, logis.timestampreceiver,  ");
            sb.Append("          trueheading,speedoverground,si.dimensiontobow,si.dimensiontostern,si.dimensiontoport,si.dimensiontostarboard,navigationalstatus,navstatusdesc, ");
            sb.Append("          rateofturn,courseoverground, latitude, longitude,         ");
            sb.Append("      si.shiptypedesc ship_type, logis.received_on, si.callsign, si.grt, logis.destination, si.draught,        ");
            sb.Append("      logis.etamonth, logis.etaday, logis.etahour, logis.etaminute  ");
            sb.Append("   from ship_info si join ( ");
            sb.Append("             select mmsi,  ");
            sb.Append("                             timestampreceiver, ");
            sb.Append("                             trueheading, ");
            sb.Append("                             speedoverground, ");
            sb.Append("                             navigationalstatus, ");
            sb.Append("                             navstatusdesc, ");
            sb.Append("                             rateofturn, ");
            sb.Append("                             courseoverground, ");
            sb.Append("                             latitude, ");
            sb.Append("                             longitude, ");
            sb.Append("                             received_on, ");
            sb.Append("                             destination, ");
            sb.Append("                             etamonth, ");
            sb.Append("                             etaday, ");
            sb.Append("                             etahour, ");
            sb.Append("                             etaminute ");
            sb.Append("             from incoming_ais  ");
            sb.Append("             where received_on between @from and @to  ");
            if (!mmsi.Equals("-1")) sb.Append(" and  mmsi = @mmsi   ");
            sb.Append("         order by mmsi, received_on asc limit 1 ) logis  ");
            sb.Append("        on si.mmsi = logis.mmsi ");
            sb.Append("        order by ship_name ");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            _ps.OpenConnectionAndCreateCommand();

            if (mmsi.Equals("-1"))
            {
                _ps.Comm.Parameters.AddWithValue("@from", from);
                _ps.Comm.Parameters.AddWithValue("@to", to);
            }
            else
            {
                _ps.Comm.Parameters.AddWithValue("@from", from);
                _ps.Comm.Parameters.AddWithValue("@to", to);
                _ps.Comm.Parameters.AddWithValue("@mmsi", mmsi);
            }

            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    shipProperty = new Entities.ShipProperty
                    {
                        MMSI = int.Parse(reader["mmsi"].ToString()),
                        ShipTypeDescription = reader["ship_type"].ToString(),
                        ShipName = reader["ship_name"].ToString(),
                        TimeStampReceiver = double.Parse(reader["TimeStampReceiver"].ToString()),
                        HDG = reader["TrueHeading"].ToString(),
                        SOG = reader["SpeedOverGround"].ToString(),
                        ToBow = reader["DimensionToBow"].ToString(),
                        ToStern = reader["DimensionToStern"].ToString(),
                        ToPort = reader["DimensionToPort"].ToString(),
                        ToStarBoard = reader["DimensionToStarboard"].ToString(),
                        NavStatus = int.Parse(reader["NavigationalStatus"].ToString()),
                        NavStatusDescription = reader["navstatusdesc"].ToString(),
                        ROT = reader["RateOfTurn"].ToString(),
                        COG = reader["CourseOverGround"].ToString(),

                        CallSign = reader["callsign"].ToString(),
                        Grt = (reader["grt"] as decimal?).GetValueOrDefault(),
                        Destination = reader["destination"].ToString(),
                        Draught = reader["draught"].ToString(),

                        ETAMonth = reader["etamonth"].ToString(),
                        ETADay = reader["etaday"].ToString(),
                        ETAHour = reader["etahour"].ToString(),
                        ETAMinute = reader["etaminute"].ToString()
                    };

                    shipPoint = new GeoJSON.Point
                    {
                        Coordinates = new double[] { double.Parse(reader["Longitude"].ToString()), double.Parse(reader["Latitude"].ToString()) }
                    };

                    shipFeature = new Entities.ShipFeature
                    {
                        Geometry = shipPoint,
                        Properties = shipProperty
                    };

                    shipFeatures.Add(shipFeature);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            result.Features = shipFeatures.ToArray<Entities.ShipFeature>();
            return result;
        }


        public Entities.ShipDisplayFeatureCollection RecordedPositionByDate(string mmsi, string date, string trunc)
         {
             Entities.ShipDisplayFeatureCollection result = new Entities.ShipDisplayFeatureCollection();
             Entities.ShipProperty shipProperty;
             Entities.ShipFeature shipFeature;
             List<Entities.ShipFeature> shipFeatures = new List<Entities.ShipFeature>();
             GeoJSON.Point shipPoint;
             StringBuilder sb = new StringBuilder();

             sb.Append(" select aw.log_ais, ");
             sb.Append(" substring(si.log_ais from 'VesselName\\\"\\:\\\"(.*?)\\\",') \"ship_name\", ");
             sb.Append(" substring(si.log_ais from 'ShipTypeDescription\\\"\\:\\\"(.*?)\\\"') \"ship_type\" from (select mmsi, log_ais from   ");
             sb.Append(" (  	select mmsi, log_ais, received_on, rank() over (partition by mmsi order by received_on ASC) ranks from incoming_ais    ");
             sb.Append(" where date_trunc('" + trunc + "', received_on) = '" + date + "' ");
             sb.Append(" ) ow where ranks = 1 ");
             if (!mmsi.Equals("-1")) sb.Append(" and  mmsi = '" + mmsi + "'   ");
             sb.Append(" ) aw ");
             sb.Append(" left join ship_info si on aw.mmsi = si.mmsi ");

             JavaScriptSerializer serializer = new JavaScriptSerializer();
             _ps.OpenConnectionAndCreateCommand();
             NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

             Hashtable item;
             if (reader.HasRows)
             {
                 while (reader.Read())
                 {
                     item = serializer.Deserialize<Hashtable>(reader["log_ais"].ToString());
                     shipPoint = new GeoJSON.Point();
                     shipPoint.Coordinates = new double[] { double.Parse(item["Longitude"].ToString()), double.Parse(item["Latitude"].ToString()) };

                     shipProperty = new Entities.ShipProperty();
                     shipProperty.ShipTypeDescription = reader["ship_type"].ToString();
                     shipProperty.ShipName = reader["ship_name"].ToString();
                     shipProperty.MMSI = int.Parse(item["MMSI"].ToString());
                     shipProperty.TimeStampReceiver = double.Parse(item["TimeStampReceiver"].ToString());
                     shipProperty.HDG = item["TrueHeading"].ToString();
                     shipProperty.SOG = item["SpeedOverGround"].ToString();
                     if (item.ContainsKey("NavigationalStatus"))
                     {
                         shipProperty.NavStatus = int.Parse(item["NavigationalStatus"].ToString());
                         shipProperty.NavStatusDescription = item["NavigationalStatusDescription"].ToString();
                     }
                     if (item.ContainsKey("RateOfTurn")) shipProperty.ROT = item["RateOfTurn"].ToString();
                     shipProperty.COG = item["CourseOverGround"].ToString();

                     shipFeature = new Entities.ShipFeature();
                     shipFeature.Geometry = shipPoint;
                     shipFeature.Properties = shipProperty;

                     shipFeatures.Add(shipFeature);
                 }
             }
             reader.Close();
             _ps.CloseConnection();

             result.Features = shipFeatures.ToArray<Entities.ShipFeature>();
             return result;
         }
        
         public int Delete(int mmsi)
         {
             int returnValue = 0;
             StringBuilder sb = new StringBuilder();
             sb.Append(" delete from incoming_ais ");
             sb.Append(" where mmsi = :valmmsi ");

             NpgsqlParameter paramId;
             paramId = new NpgsqlParameter("valmmsi", NpgsqlTypes.NpgsqlDbType.Integer);
             NpgsqlParameter[] Params = { paramId };
             paramId.Value = mmsi;

             _ps.OpenConnectionAndCreateCommand();
             returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
             return returnValue;
         }

         public Entities.ShipDisplayFeatureCollection RecordedSmoothByMMSIAndDate(string mmsi, string from, string to)
         {
             Entities.ShipDisplayFeatureCollection result = new Entities.ShipDisplayFeatureCollection();
             Entities.ShipProperty shipProperty;
             Entities.ShipFeature shipFeature;
             List<Entities.ShipFeature> shipFeatures = new List<Entities.ShipFeature>();
             GeoJSON.Point shipPoint;
             StringBuilder sb = new StringBuilder();


             sb.Append(" select si.mmsi, ia.log_ais logis, ");
             sb.Append(" substring(si.log_ais from 'VesselName\\\"\\:\\\"(.*?)\\\",') ship_name, ");
             sb.Append(" substring(si.log_ais from 'ShipTypeDescription\\\"\\:\\\"(.*?)\\\"') ship_type from incoming_ais ia ");
             sb.Append(" join ship_info si on ia.mmsi = si.mmsi");
             sb.Append(" where ia.received_on between '" + from + "' and '" + to + "' ");
             if (!mmsi.Equals("-1")) sb.Append(" and  ia.mmsi = '" + mmsi + "'   ");
             sb.Append(" order by ia.received_on asc ");

             JavaScriptSerializer serializer = new JavaScriptSerializer();
             _ps.OpenConnectionAndCreateCommand();
             NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

             Hashtable item;
             if (reader.HasRows)
             {
                 while (reader.Read())
                 {
                     item = serializer.Deserialize<Hashtable>(reader["logis"].ToString());
                     shipProperty = new Entities.ShipProperty();
                     shipProperty.MMSI = int.Parse(item["MMSI"].ToString());
                     shipProperty.ShipTypeDescription = reader["ship_type"].ToString();
                     shipProperty.ShipName = reader["ship_name"].ToString();
                     shipProperty.TimeStampReceiver = double.Parse(item["TimeStampReceiver"].ToString());
                     shipProperty.HDG = item["TrueHeading"].ToString();
                     shipProperty.SOG = item["SpeedOverGround"].ToString();
                     if (item.ContainsKey("NavigationalStatus"))
                     {
                         shipProperty.NavStatus = int.Parse(item["NavigationalStatus"].ToString());
                         shipProperty.NavStatusDescription = item["NavigationalStatusDescription"].ToString();
                     }
                     if (item.ContainsKey("RateOfTurn")) shipProperty.ROT = item["RateOfTurn"].ToString();
                     shipProperty.COG = item["CourseOverGround"].ToString();

                     shipPoint = new GeoJSON.Point();
                     shipPoint.Coordinates = new double[] { double.Parse(item["Longitude"].ToString()), double.Parse(item["Latitude"].ToString()) };


                     shipFeature = new Entities.ShipFeature();
                     shipFeature.Geometry = shipPoint;
                     shipFeature.Properties = shipProperty;

                     shipFeatures.Add(shipFeature);
                 }
             }
             reader.Close();
             _ps.CloseConnection();

             result.Features = shipFeatures.ToArray<Entities.ShipFeature>();
             return result;
         }
        
         public List<double[]> RecordedCoordinatesByMMSIAndDate(string mmsi, string from, string to)
         {
             StringBuilder sb = new StringBuilder();

             sb.Append("  select log_ais from incoming_ais ia where ia.received_on between '" + from + "' and '" + to + "' and mmsi = '" + mmsi + "' order by received_on asc");

             JavaScriptSerializer serializer = new JavaScriptSerializer();
             _ps.OpenConnectionAndCreateCommand();
             NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

             List<double[]> items = new List<double[]>();
             if (reader.HasRows)
             {
                 while (reader.Read())
                 {
                     Hashtable item = serializer.Deserialize<Hashtable>(reader["log_ais"].ToString());
                     items.Add(new double[] { double.Parse(item["Longitude"].ToString()), double.Parse(item["Latitude"].ToString()) });
                 }
             }

             reader.Close();
             _ps.CloseConnection();

             return items;
         }

         public double CountDistance(List<double[]> coords)
         {
             double res = 0;
             for (int i = 0; i < coords.Count - 1; i++)
             {
                 _ps.OpenConnectionAndCreateCommand();
                 res += double.Parse(_ps.ExecuteScalar("SELECT ST_Distance(" +
         "ST_GeomFromText('POINT(" + coords[i][0] + " " + coords[i][1] + ")',4326)," +
         "ST_GeomFromText('POINT(" + coords[i + 1][0] + " " + coords[i + 1][1] + ")', 4326));"));
             }
             return res;
         }

         public int DropAisLogTempTable()
         {
             StringBuilder sb = new StringBuilder();
             _ps.OpenConnectionAndCreateCommand();
             sb.Append("drop table if exists incoming_ais_temp");
             _ps.ExecuteNonQuery(sb.ToString());
             return 1;
         }

         public int CreateAisLogTempTable()
         {
             DropAisLogTempTable();
             StringBuilder sb = new StringBuilder();
             _ps.OpenConnectionAndCreateCommand();
             sb.Append("CREATE TABLE incoming_ais_temp(LIKE incoming_ais INCLUDING INDEXES INCLUDING CONSTRAINTS)");
             _ps.ExecuteNonQuery(sb.ToString());
             return 2;
         }

         public List<Incoming_AIS_Fields> GetAISByLimitOffset(DateTime start, double limit, double offset)
         {
             StringBuilder sb = new StringBuilder();
             sb.Append(" select * from incoming_ais ");
             sb.Append(" where received_on >  :start  ");
             sb.Append(" order by received_on  limit :limit offset :offset ");
             _ps.OpenConnectionAndCreateCommand();
             List<Incoming_AIS_Fields> result = _ps.Conn.Query<Incoming_AIS_Fields>(sb.ToString(), new { start = start, limit = limit, offset = offset }).ToList();
             _ps.CloseConnection();
             return result;
         }

         public int TruncateIncomingAIS()
         {
             int returnValue = 0;
             StringBuilder sb = new StringBuilder();
             sb.Append(" truncate incoming_ais ");
             _ps.OpenConnectionAndCreateCommand();
             returnValue = _ps.ExecuteNonQuery(sb.ToString());
             return returnValue;
         }


         public List<Incoming_AIS_Fields> GetAISTempByLimitOffset(double limit, double offset)
         {
             StringBuilder sb = new StringBuilder();
             sb.Append(" select * from incoming_ais_temp ");
             sb.Append(" order by received_on  limit :limit offset :offset ");
             _ps.OpenConnectionAndCreateCommand();
             List<Incoming_AIS_Fields> result = _ps.Conn.Query<Incoming_AIS_Fields>(sb.ToString(), new { limit = limit, offset = offset }).ToList();
             _ps.CloseConnection();
             return result;
         }

         public void VacuumFull()
         {
             StringBuilder sb = new StringBuilder();
             _ps.OpenConnectionAndCreateCommand();
             sb.Append("vacuum full");
             _ps.ExecuteNonQuery(sb.ToString());
         }

         public int DeleteBetweenDates(DateTime start, DateTime end)
         {
             int returnValue;
             StringBuilder sb = new StringBuilder();
             sb.Append(" delete from incoming_ais ");
             sb.Append(" where received_on between :start and :end  ");
             _ps.OpenConnectionAndCreateCommand();
             _ps.Comm.Parameters.AddWithValue(":start", start);
             _ps.Comm.Parameters.AddWithValue(":end", end);
             returnValue = _ps.ExecuteNonQuery(sb.ToString());
             return returnValue;
        }
        public Entities.ShipDisplayFeatureCollection RecordedPositionByMMSILast10(string mmsi, int topx)
        {
            Entities.ShipDisplayFeatureCollection result = new Entities.ShipDisplayFeatureCollection();
            Entities.ShipProperty shipProperty;
            Entities.ShipFeature shipFeature;
            List<Entities.ShipFeature> shipFeatures = new List<Entities.ShipFeature>();
            GeoJSON.Point shipPoint;
            StringBuilder sb = new StringBuilder();

            sb.Append(" SELECT   logis.ROW_NUMBER, si.mmsi, si.vesselname ship_name, ");
            sb.Append("          logis.timestampreceiver, trueheading, speedoverground, ");
            sb.Append("          si.dimensiontobow, si.dimensiontostern, si.dimensiontoport, ");
            sb.Append("          si.dimensiontostarboard, navigationalstatus, navstatusdesc, ");
            sb.Append("          rateofturn, courseoverground, latitude, longitude, ");
            sb.Append("          si.shiptypedesc ship_type, logis.received_on, si.callsign, si.grt, ");
            sb.Append("          logis.destination, si.draught, logis.etamonth, logis.etaday, ");
            sb.Append("          logis.etahour, logis.etaminute ");
            sb.Append("     FROM ship_info si ");
            sb.Append("          JOIN ");
            sb.Append("          (SELECT   mmsi, received_on, timestampreceiver, trueheading, ");
            sb.Append("                    speedoverground, navigationalstatus, navstatusdesc, ");
            sb.Append("                    rateofturn, courseoverground, latitude, longitude, ");
            sb.Append("                    MAX (received_on), destination, etamonth, etaday, etahour, ");
            sb.Append("                    etaminute, ");
            sb.Append("                    ROW_NUMBER () OVER (ORDER BY received_on DESC) ROW_NUMBER ");
            sb.Append("               FROM incoming_ais ia ");
            sb.Append("              WHERE ia.mmsi = :mmsi ");
            sb.Append("           GROUP BY mmsi, ");
            sb.Append("                    timestampreceiver, ");
            sb.Append("                    trueheading, ");
            sb.Append("                    speedoverground, ");
            sb.Append("                    navigationalstatus, ");
            sb.Append("                    navstatusdesc, ");
            sb.Append("                    rateofturn, ");
            sb.Append("                    courseoverground, ");
            sb.Append("                    latitude, ");
            sb.Append("                    longitude, ");
            sb.Append("                    destination, ");
            sb.Append("                    etamonth, ");
            sb.Append("                    etaday, ");
            sb.Append("                    etahour, ");
            sb.Append("                    etaminute, ");
            sb.Append("                    received_on) logis ");
            sb.Append("          ON logis.ROW_NUMBER <= :topx AND logis.mmsi = si.mmsi ");
            sb.Append("    WHERE si.mmsi = :mmsi ");
            sb.Append(" ORDER BY logis.ROW_NUMBER DESC ");

            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { new NpgsqlParameter("mmsi", mmsi), new NpgsqlParameter("topx", topx) };

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(), Params);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    shipProperty = new Entities.ShipProperty();
                    shipProperty.MMSI = int.Parse(reader["mmsi"].ToString());
                    shipProperty.ShipTypeDescription = reader["ship_type"].ToString();
                    shipProperty.ShipName = reader["ship_name"].ToString();
                    shipProperty.TimeStampReceiver = double.Parse(reader["TimeStampReceiver"].ToString());
                    shipProperty.HDG = reader["TrueHeading"].ToString();
                    shipProperty.SOG = reader["SpeedOverGround"].ToString();
                    shipProperty.ToBow = reader["DimensionToBow"].ToString();
                    shipProperty.ToStern = reader["DimensionToStern"].ToString();
                    shipProperty.ToPort = reader["DimensionToPort"].ToString();
                    shipProperty.ToStarBoard = reader["DimensionToStarboard"].ToString();
                    shipProperty.NavStatus = int.Parse(reader["NavigationalStatus"].ToString());
                    shipProperty.NavStatusDescription = reader["navstatusdesc"].ToString();
                    shipProperty.ROT = reader["RateOfTurn"].ToString();
                    shipProperty.COG = reader["CourseOverGround"].ToString();
                    shipProperty.CallSign = reader["callsign"].ToString();
                    decimal grt = 0;
                    decimal.TryParse(reader["grt"].ToString(), out grt);
                    shipProperty.Grt = grt;
                    shipProperty.Destination = reader["destination"].ToString();
                    shipProperty.Draught = reader["draught"].ToString();

                    shipProperty.ETAMonth = reader["etamonth"].ToString();
                    shipProperty.ETADay = reader["etaday"].ToString();
                    shipProperty.ETAHour = reader["etahour"].ToString();
                    shipProperty.ETAMinute = reader["etaminute"].ToString();
                                      
                    shipPoint = new GeoJSON.Point();
                    shipPoint.Coordinates = new double[] { double.Parse(reader["Longitude"].ToString()), double.Parse(reader["Latitude"].ToString()) };

                    shipFeature = new Entities.ShipFeature();
                    shipFeature.Geometry = shipPoint;
                    shipFeature.Properties = shipProperty;

                    shipFeatures.Add(shipFeature);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            result.Features = shipFeatures.ToArray<Entities.ShipFeature>();
            return result;
        }

        #endregion

    }
}
