﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBProvider;
using Npgsql;
using AIS.DataLogger.Interfaces;

namespace AIS.DataLogger
{
    internal class Mail : IMail
    {
        PostgreSQL _ps;

        public Mail(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public Entity.MailConfig GetMailConfig()
        {
            Entity.MailConfig result = new Entity.MailConfig();
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from mail_config where row_limit=1");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    result.server = (string)reader["server"];
                    result.port = (int)reader["port"];
                    result.username = (string)reader["username"];
                    result.password = (string)reader["password"];
                    result.enableSSL = (bool)reader["enable_ssl"];
                }
            }

            reader.Close();
            _ps.CloseConnection();
            return result;
        }

        public int UpdateMailConfig(Entity.MailConfig c)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" update mail_config ");
            sb.Append(" set server = :server ");
            sb.Append("     ,port = :port ");
            sb.Append("     ,username = :username ");
            sb.Append("     ,password = :password ");
            sb.Append("     ,enable_ssl = :enable_ssl ");
            sb.Append(" where row_limit = 1 ");

            NpgsqlParameter[] Params = { new NpgsqlParameter("server", c.server), 
                                           new NpgsqlParameter("port", c.port),
                                           new NpgsqlParameter("username", c.username),
                                           new NpgsqlParameter("password", c.password),
                                           new NpgsqlParameter("enable_ssl", c.enableSSL) };

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public List<Entity.MailTarget> GetMailTargets()
        {
            List<Entity.MailTarget> result = new List<Entity.MailTarget>();
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from mail_targets");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Entity.MailTarget curr = new Entity.MailTarget();
                    curr.address = (string)reader["target_mail"];
                    curr.name = (string)reader["target_name"];
                    result.Add(curr);
                }
            }

            reader.Close();
            _ps.CloseConnection();
            return result;
        }

        public int AddMailTarget(Entity.MailTarget t)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" insert into mail_targets (target_mail, target_name) ");
            sb.Append(" values (:target_mail,:target_name) ");

            NpgsqlParameter[] Params = { new NpgsqlParameter("target_mail", t.address), 
                                           new NpgsqlParameter("target_name", t.name) };

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int UpdateMailTarget(Entity.MailTarget t)
        {
            throw new NotImplementedException();
        }

        public int DeleteMailTarget(Entity.MailTarget t)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from mail_targets ");
            sb.Append(" where target_mail=:target_mail and target_name=:target_name ");

            NpgsqlParameter[] Params = { new NpgsqlParameter("target_mail", t.address), 
                                           new NpgsqlParameter("target_name", t.name) };

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }
    }
}
