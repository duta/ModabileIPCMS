﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using NpgsqlTypes;
using DBProvider;
using AIS.DataLogger.Interfaces;

namespace AIS.DataLogger
{
    internal class ZoneKT : IZoneKT
    {

        PostgreSQL _ps;

        public ZoneKT(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public int Insert(int id, string ksid, string mmsi, DateTime dateAwal, DateTime dateAkhir)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" INSERT INTO zone_kt_allowed(zone_id, ksid, mmsi, date_start, date_end) ");
            sb.Append(" values (:id,:ksid, :mmsi,:date_start,:date_end) ");

            NpgsqlParameter[] Params = { new NpgsqlParameter(":id", id), 
                                           new NpgsqlParameter(":ksid", ksid), 
                                           new NpgsqlParameter(":mmsi", mmsi), 
                                           new NpgsqlParameter(":date_start", dateAwal), 
                                           new NpgsqlParameter(":date_end", dateAkhir) };

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int Delete(int id, string ksid, DateTime dateAwal, DateTime dateAkhir)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" DELETE FROM zone_kt_allowed WHERE zone_id=:id and ksid=:ksid and date_start=:date_start and date_end=:date_end ");

            NpgsqlParameter[] Params = { new NpgsqlParameter(":id", id), 
                                           new NpgsqlParameter(":ksid", ksid), 
                                           new NpgsqlParameter(":date_start", dateAwal), 
                                           new NpgsqlParameter(":date_end", dateAkhir) };

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public List<Dictionary<string, string>> Find(DateTime dateAwal, DateTime dateAkhir)
        {
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT zone.name \"name\", zone_id, date_start, date_end, ksid, mmsi  FROM zone zone, zone_kt_allowed zka where zone.id = zka.zone_id and date_start >= :dateAwal and date_end <= :dateAkhir");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(), new[] {
                new NpgsqlParameter("dateAwal", dateAwal),
                new NpgsqlParameter("dateAkhir", dateAkhir)
            });

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, string> res = new Dictionary<string, string>();
                    res.Add("name", reader["name"].ToString());
                    res.Add("zone_id", reader["zone_id"].ToString());
                    res.Add("date_start", ((DateTime)reader["date_start"]).ToString("dd-MM-yyyy"));
                    res.Add("date_end", ((DateTime)reader["date_end"]).ToString("dd-MM-yyyy"));
                    res.Add("ksid", reader["ksid"].ToString());
                    res.Add("mmsi", reader["mmsi"].ToString());
                    result.Add(res);
                }
            }

            reader.Close();
            _ps.CloseConnection();
            return result;
        }

        public bool Check(int id, string mmsi, DateTime date)
        {
            bool result = false;
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT * FROM zone_kt_allowed zka where date_start <= :date and date_end >= :date and mmsi = :mmsi");

            _ps.OpenConnectionAndCreateCommand();

            NpgsqlParameter[] Params = { new NpgsqlParameter(":id", id), 
                                           new NpgsqlParameter(":mmsi", mmsi), 
                                           new NpgsqlParameter(":date", date) };
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(), Params);

            if (reader.HasRows)
                while (reader.Read())
                    if (id == int.Parse(reader["zone_id"].ToString())) result = true;

            reader.Close();
            _ps.CloseConnection();
            return result;
        }
    }
}
