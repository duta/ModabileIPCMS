﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBProvider;
using Npgsql;
using AIS.DataLogger.Interfaces;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger
{
    internal class ShipClose : IShipClose
    {
        PostgreSQL _ps;

        public ShipClose(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public List<Entity.ShipCloseData> GetLatestDangers(int limit)
        {
            List<Entity.ShipCloseData> results = new List<ShipCloseData>();
            StringBuilder sb = new StringBuilder();
            sb.Append(" SELECT mmsi_ship,  ");
            sb.Append("        (select  ");
            sb.Append("     log_ais::json->>'VesselName' ");
            sb.Append(" from ship_info ");
            sb.Append("     where mmsi = mmsi_ship ) namakpl, ");
            sb.Append("        mmsi_other_ship,  ");
            sb.Append("        (select  ");
            sb.Append("     log_ais::json->>'VesselName' ");
            sb.Append(" from ship_info ");
            sb.Append("     where mmsi = mmsi_other_ship ) namakpllain, ");
            sb.Append("        distance,  ");
            sb.Append("        is_head   ");
            sb.Append(" FROM kapal_berdekatan_last ");
            sb.Append(" ORDER BY datetime DESC LIMIT :limit ");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(),
                new NpgsqlParameter[] { new NpgsqlParameter("limit", limit) });

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Entity.ShipCloseData result = new Entity.ShipCloseData();
                    result.mmsi = (int)reader["mmsi_ship"];
                    result.other_mmsi = (int)reader["mmsi_other_ship"];
                    result.is_heading = (bool)reader["is_head"];
                    result.distance = (int)reader["distance"];
                    results.Add(result);
                }
            }

            reader.Close();
            _ps.CloseConnection();
            return results;
        }

        public int SaveLatestDanger(Entity.ShipCloseData data)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" INSERT INTO kapal_berdekatan_last(mmsi_ship, mmsi_other_ship, distance, is_head) ");
            sb.Append(" values (:mmsi_ship,:mmsi_other_ship,:distance,:is_head) ");

            NpgsqlParameter[] Params = { new NpgsqlParameter("mmsi_ship", data.mmsi), 
                                           new NpgsqlParameter("mmsi_other_ship", data.other_mmsi), 
                                           new NpgsqlParameter("distance", data.distance), 
                                           new NpgsqlParameter("is_head", data.is_heading) };

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int DeleteLatestDanger(Entity.ShipCloseData data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from kapal_berdekatan ");
            sb.Append(" where mmsi_ship=:mmsi and mmsi_other_ship=:mmsi_other_ship ");

            NpgsqlParameter[] Params = { new NpgsqlParameter("mmsi", data.mmsi), new NpgsqlParameter("mmsi_other_ship", data.other_mmsi) };

            _ps.OpenConnectionAndCreateCommand();
            return _ps.ExecuteNonQuery(sb.ToString(), Params);
        }

        public Entity.ShipCloseData GetMaxDanger(int mmsi)
        {
            Entity.ShipCloseData result = null;
            StringBuilder sb = new StringBuilder();
            sb.Append(" SELECT mmsi_ship, max(distance) \"distance\", bool_or(can_collide) \"can_collide\" FROM kapal_berdekatan ");
            sb.Append(" WHERE mmsi_ship=:mmsi group by mmsi_ship ");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(),
                new NpgsqlParameter[] { new NpgsqlParameter("mmsi", mmsi) });

            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    result = new Entity.ShipCloseData();
                    result.mmsi = mmsi;
                    result.is_heading = (bool)reader["can_collide"];
                    result.distance = (int)reader["distance"];
                }
            }

            reader.Close();
            _ps.CloseConnection();
            return result;
        }

        public Entity.ShipCloseData GetDanger(int mmsi, int mmsi_other)
        {
            Entity.ShipCloseData result = null;
            StringBuilder sb = new StringBuilder();
            sb.Append(" SELECT mmsi_ship, mmsi_other_ship, distance, can_collide  FROM kapal_berdekatan ");
            sb.Append(" WHERE mmsi_ship=:mmsi AND mmsi_other_ship=:mmsi_other_ship ");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(),
                new NpgsqlParameter[] { new NpgsqlParameter("mmsi", mmsi), new NpgsqlParameter("mmsi_other_ship", mmsi_other) });

            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    result = new Entity.ShipCloseData();
                    result.mmsi = mmsi;
                    result.other_mmsi = mmsi_other;
                    result.is_heading = (bool)reader["can_collide"];
                    result.distance = (int)reader["distance"];
                }
            }

            reader.Close();
            _ps.CloseConnection();
            return result;
        }

        public int DeleteDanger(int mmsi, int mmsi_other)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from kapal_berdekatan ");
            sb.Append(" where mmsi_ship=:mmsi and mmsi_other_ship=:mmsi_other_ship ");

            NpgsqlParameter[] Params = { new NpgsqlParameter("mmsi", mmsi), new NpgsqlParameter("mmsi_other_ship", mmsi_other) };

            _ps.OpenConnectionAndCreateCommand();
            return _ps.ExecuteNonQuery(sb.ToString(), Params);
        }

        public int DeleteByMMSI(int mmsi)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from kapal_berdekatan ");
            sb.Append(" where mmsi_ship=:mmsi ");

            NpgsqlParameter[] Params = { new NpgsqlParameter("mmsi", mmsi) };

            _ps.OpenConnectionAndCreateCommand();
            return _ps.ExecuteNonQuery(sb.ToString(), Params);
        }

        public int DeleteBeforeDate(DateTime date)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from kapal_berdekatan ");
            sb.Append(" where datetime<:date; ");
            sb.Append(" delete from kapal_berdekatan_last ");
            sb.Append(" where datetime<:date; ");

            NpgsqlParameter[] Params = { new NpgsqlParameter("date", date) };

            _ps.OpenConnectionAndCreateCommand();
            return _ps.ExecuteNonQuery(sb.ToString(), Params);
        }

        public ShipCloseConfig GrabConfig()
        {
            Entity.ShipCloseConfig result = new Entity.ShipCloseConfig();
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from kapal_berdekatan_config where row_limit=1");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    result.yellow_range = (int)reader["yellow_range"];
                    result.red_range = (int)reader["red_range"];
                    result.check_range = (int)reader["check_range"];
                    result.prediction_interval = (int)reader["prediction_interval"];
                    result.prediction_length = (int)reader["prediction_length"];
                }
            }

            reader.Close();
            _ps.CloseConnection();
            return result;
        }

        public int SaveConfig(ShipCloseConfig c)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" UPDATE kapal_berdekatan_config ");
            sb.Append(" SET yellow_range=:yellow_range, ");
            sb.Append(" red_range=:red_range, ");
            sb.Append(" prediction_interval=:prediction_interval, ");
            sb.Append(" prediction_length=:prediction_length, ");
            sb.Append(" check_range=:check_range ");
            sb.Append(" where row_limit = 1 ");

            NpgsqlParameter[] Params = { new NpgsqlParameter("yellow_range", c.yellow_range), 
                                           new NpgsqlParameter("red_range", c.red_range),
                                           new NpgsqlParameter("check_range", c.check_range),
                                           new NpgsqlParameter("prediction_interval", c.prediction_interval),
                                           new NpgsqlParameter("prediction_length", c.prediction_length) };

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int SaveDanger(ShipCloseData data)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" INSERT INTO kapal_berdekatan(mmsi_ship, mmsi_other_ship, distance, can_collide) ");
            sb.Append(" values (:mmsi_ship,:mmsi_other_ship,:distance,:can_collide) ");

            NpgsqlParameter[] Params = { new NpgsqlParameter("mmsi_ship", data.mmsi), 
                                           new NpgsqlParameter("mmsi_other_ship", data.other_mmsi), 
                                           new NpgsqlParameter("distance", data.distance), 
                                           new NpgsqlParameter("can_collide", data.is_heading) };

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }
    }
}
