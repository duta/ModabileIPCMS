﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger.Interfaces
{
    public interface IShipClose
    {
        List<Entity.ShipCloseData> GetLatestDangers(int limit);
        int SaveLatestDanger(Entity.ShipCloseData data);
        int DeleteLatestDanger(Entity.ShipCloseData data);
        ShipCloseConfig GrabConfig();
        int SaveConfig(ShipCloseConfig conf);
        ShipCloseData GetDanger(int mmsi, int mmsi_other);
        ShipCloseData GetMaxDanger(int mmsi);
        int DeleteDanger(int mmsi, int mmsi_other);
        int SaveDanger(ShipCloseData data);
        int DeleteByMMSI(int mmsi);
		int DeleteBeforeDate(DateTime date);
    }
}
