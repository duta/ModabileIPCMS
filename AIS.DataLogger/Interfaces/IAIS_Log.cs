﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.Core;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger.Interfaces
{
    public interface IAIS_Log
    {
        int InsertBulkIncomingAIS(List < Entity.Incoming_AIS> incoming);
        int InsertBulkIncomingAIS(List<Incoming_AIS_Fields> incoming);
        int InsertBulkIncomingAISTemp(List<Incoming_AIS_Fields> incoming);
        int InsertBulkIncomingAISByCopyIn(List<Incoming_AIS_Fields> incoming);
        Entity.Incoming_AIS LastPositionByMMSI(string mmsi);
        Entities.ShipDisplayFeatureCollection RecordedPositionByMMSIAndDate(string mmsi, string from, string to);
        Entities.ShipDisplayFeatureCollection RecordedPositionByDate(string mmsi, string date, string trunc);
        int Delete(int mmsi);
        int DeleteLTDate(DateTime tglAkhirLog);
        Entities.ShipDisplayFeatureCollection RecordedSmoothByMMSIAndDate(string mmsi, string from, string to);
        List<double[]> RecordedCoordinatesByMMSIAndDate(string mmsi, string from, string to);
        double CountDistance(List<double[]> coords);
        int DropAisLogTempTable();
        int CreateAisLogTempTable();
        List<Incoming_AIS_Fields> GetAISByLimitOffset(DateTime start, double limit, double offset);
        int TruncateIncomingAIS();
        List<Incoming_AIS_Fields> GetAISTempByLimitOffset(double limit, double offset);
        void VacuumFull();
        int DeleteBetweenDates(DateTime start, DateTime end);
        Entities.ShipDisplayFeatureCollection RecordedPositionByMMSILast10(string v, int topx);
    }
}
