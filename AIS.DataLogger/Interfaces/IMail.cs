﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger.Interfaces
{
    public interface IMail
    {
        Entity.MailConfig GetMailConfig();
        int UpdateMailConfig(Entity.MailConfig c);
        List<Entity.MailTarget> GetMailTargets();
        int AddMailTarget(Entity.MailTarget t);
        int UpdateMailTarget(Entity.MailTarget t);
        int DeleteMailTarget(Entity.MailTarget t);
    }
}
