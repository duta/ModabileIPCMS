﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger.Interfaces
{
    public interface IZone
    {
         bool ThisPointWithinZone(decimal lon, decimal lat, int zoneID);
         Entity.Zone GetZoneByID(int ID);
         Entity.Zone GetZoneByPoint(decimal lon, decimal lat);
         List<Entity.Zone> GetAllZone();
         int Update(Entity.Zone zone);
         int Insert(Entity.Zone zone);
         int Delete(int id);
    }
}
