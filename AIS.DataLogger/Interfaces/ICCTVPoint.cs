﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger.Interfaces
{
    public interface ICCTVPoint
    {
        List<CCTVPoint> GetCCTVs();
        int InsertCCTV(CCTVPoint c);
        int DeleteCCTV(CCTVPoint c);
        int UpdateCCTV(CCTVPoint c);
    }
}
