﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger.Interfaces
{
    public interface IShipInfo
    {
        bool IsThereShipInfoByMMSI(int mmsi);
        int? GetOIDShipInfoByMMSI(int mmsi);
		Entity.Incoming_AIS GetShipInfo(int oid);
        Entity.ShipInfo GetShipInfoByMMSI(int mmsi);
        int InsertShipInfo(Entity.ShipInfo incoming);
        int UpdateShipInfo(Entity.ShipInfo incoming, int mmsi);
        List<Entity.Incoming_AIS> GetDetectedShipsInCurrentTime();
        LabelValue[] GetLabelValueOfMMSIShipName(string filter);
        int Delete(int mmsi);
        string GetKodeKplFromKplAISSiuk(string CallSign);
        List<Entity.ShipInfo> GetAll();
    }
}
