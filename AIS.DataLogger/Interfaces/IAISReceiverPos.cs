﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.Core;

namespace AIS.DataLogger.Interfaces
{
    public interface IAISReceiverPos
    {
        Dictionary<int, Entities.AISReceiverPos> GetAll();  
    }
}
