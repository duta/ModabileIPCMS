﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;
using System.Data;

namespace AIS.DataLogger.Interfaces
{
    public interface IZonaIn
    {
        double GetZonaInID();
        double? GetMaxZonaInIdByMMSI(int mmsi);
        int Insert(Zona_In zonaIn);
        int Update(Zona_In zonaIn);
        int[] GetMooredZoneID();
        Zona_In GetDetailShipsOnZonaByMMSIZonaID(int mmsi, int zonaID);
        DataTable GetListUrutanKedatanganKapal(DateTime dateFrom, DateTime dateTo);
        DataTable GetTravelInZones(int mmsi, DateTime dateFrom, DateTime dateTo, Dictionary<string, string> postgisConn);
    }
}
