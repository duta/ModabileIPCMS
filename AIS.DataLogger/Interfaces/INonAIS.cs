﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger.Interfaces
{
    public interface INonAIS
    {
        int? GetOIDByMMSI(int mmsi);
        List<Entity.NonAIS> GetListNonAIS();
        List<Entity.NonAIS> GetListNonAISWithLastPosition();
        int Upsert(int mmsi, DateTime last_draw, string log_ais, int zona_id, bool keluar_zona);
        int Delete(int mmsi);
    }
}
