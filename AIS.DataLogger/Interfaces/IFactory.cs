﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIS.DataLogger.Interfaces
{
    public interface IFactory
    {
        string ConnectionString {get;}
        IAIS_Log GetAISLog();
        IZone GetZone();
        IShipInfo GetShipInfo();
        INavStatus GetMasterNavStatus();
        IShipType GetMasterShipType();
        IZonaIn GetZonaIn();
        INonAIS GetNonAIS();
        IAISReceiverPos GetAISReceiverPos();
        IMail GetMail();
        IShipClose GetShipClose();
        IZoneKT GetZoneKT();
        IAssortedStuff GetAssortedStuff();
        IVideoCCTV GetVideoCCTV();
        ICCTVPoint GetCCTVPoint();
    }
}
