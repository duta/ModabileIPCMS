﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.Core;

namespace AIS.DataLogger.Interfaces
{
    public interface INavStatus
    {
        Dictionary<int, Entities.NavigationalStatus> GetAll();  
    }
}
