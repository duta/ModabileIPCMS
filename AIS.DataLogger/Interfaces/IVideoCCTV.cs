﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;
using System.Data;

namespace AIS.DataLogger.Interfaces
{
    public interface IVideoCCTV
    {
        int Insert(VideoCCTV videoCctv);
        int Delete(int vc_id);
        List<VideoCCTV> GetByDateRange(DateTime from, DateTime to);
    }
}
