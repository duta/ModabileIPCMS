﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger.Interfaces
{
    public interface IAssortedStuff
    {
        List<Entity.ASPoint> GetASPoints();
    }
}
