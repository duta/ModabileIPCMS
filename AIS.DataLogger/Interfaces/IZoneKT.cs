﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger.Interfaces
{
    public interface IZoneKT
    {
		int Insert(int id, string ksid, string mmsi, DateTime dateAwal, DateTime dateAkhir);
        int Delete(int id, string ksid, DateTime dateAwal, DateTime dateAkhir);
        List<Dictionary<string, string>> Find(DateTime dateAwal, DateTime dateAkhir);
        bool Check(int id, string mmsi, DateTime date);
    }
}
