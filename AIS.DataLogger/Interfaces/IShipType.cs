﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.Core;

namespace AIS.DataLogger.Interfaces
{
    public interface IShipType
    {
        Dictionary<int, Entities.ShipType> GetAll();  
    }
}
