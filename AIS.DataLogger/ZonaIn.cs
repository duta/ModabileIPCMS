﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using NpgsqlTypes;
using DBProvider;
using AIS.DataLogger.Interfaces;

namespace AIS.DataLogger
{
    internal class ZonaIn : IZonaIn     
    {

        PostgreSQL _ps;

        public ZonaIn(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public double GetZonaInID()
        {
            return double.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfffffff"));
        }

        public double? GetMaxZonaInIdByMMSI(int mmsi)
        {
            double returnValue;
            StringBuilder sb = new StringBuilder();
            sb.Append(" select max(zona_in_id) from zona_in where mmsi= :valmmsi ");
            NpgsqlParameter paramMMSI;
            paramMMSI = new NpgsqlParameter("valmmsi", NpgsqlDbType.Integer);
            paramMMSI.Value = mmsi;
            NpgsqlParameter[] Params = { paramMMSI };

            _ps.OpenConnectionAndCreateCommand();
            double.TryParse(_ps.ExecuteScalar(sb.ToString(), Params), out returnValue);
            double? result = null;
            result = returnValue == 0.0 ? result : returnValue;
            return result;
        }

        public int Insert(Entity.Zona_In zonaIn)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();

            sb.Append(" insert into zona_in ");
            sb.Append("             (zona_in_id, masuk,  ");
            sb.Append("              masuk_posisi, masuk_zona_id, ");
            sb.Append("              mmsi ");
            sb.Append("             ) ");
            sb.Append("      values (nextval('zona_in_id_seq'), :masuk, ");
            sb.Append("              :masuk_posisi, :masuk_zona_id, ");
            sb.Append("              :mmsi ");
            sb.Append("             ) ");

            NpgsqlParameter paramMasuk, paramMasukPosisi, paramMasukZonaID, paramMMSI;

            paramMasuk = new NpgsqlParameter("masuk", NpgsqlDbType.Timestamp);
            paramMasukPosisi = new NpgsqlParameter("masuk_posisi", NpgsqlDbType.Text);
            paramMasukZonaID = new NpgsqlParameter("masuk_zona_id", NpgsqlDbType.Integer);
            paramMMSI = new NpgsqlParameter("mmsi", NpgsqlDbType.Integer);

            NpgsqlParameter[] Params = { paramMasuk, paramMasukPosisi, paramMasukZonaID, paramMMSI };

            paramMasuk.Value = zonaIn.Masuk.Value;
            paramMasukPosisi.Value = zonaIn.MasukPosisi;
            paramMasukZonaID.Value = zonaIn.MasukZonaID;
            paramMMSI.Value = zonaIn.MMSI;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int Update(Entity.Zona_In zonaIn)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" update zona_in ");
            sb.Append("    set ");
            if (zonaIn.Berhenti.HasValue) sb.Append("        berhenti = :berhenti").Append(",");
            if (zonaIn.Berangkat.HasValue) sb.Append("        berangkat = :berangkat").Append(",");
            if (zonaIn.Keluar.HasValue) sb.Append("        keluar = :keluar").Append(",");
            if (zonaIn.AnchorIn.HasValue) sb.Append("        anchor_in = :anchor_in").Append(",");
            if (zonaIn.AnchorOut.HasValue) sb.Append("        anchor_out = :anchor_out").Append(",");
            if (zonaIn.MooredIn.HasValue) sb.Append("        moored_in = :moored_in").Append(",");
            if (zonaIn.MooredOut.HasValue) sb.Append("        moored_out = :moored_out").Append(",");
            if (!string.IsNullOrEmpty(zonaIn.BerhentiPosisi)) sb.Append("        berhenti_posisi = :berhenti_posisi").Append(",");
            if (!string.IsNullOrEmpty(zonaIn.KeluarPosisi)) sb.Append("        keluar_posisi = :keluar_posisi").Append(",");
            if (!string.IsNullOrEmpty(zonaIn.AnchorInPosisi)) sb.Append("        anchor_in_posisi = :anchor_in_posisi").Append(",");
            if (!string.IsNullOrEmpty(zonaIn.MooredInPosisi)) sb.Append("        moored_in_posisi = :moored_in_posisi").Append(",");
            sb.Length--; // remove last comma
            sb.Append(" where  zona_in_id = :zona_in_id ");

            NpgsqlParameter paramBerhenti, paramBerangkat, paramKeluar,
               paramAnchorIn, paramAnchorOut, paramMooredIn, paramMooredOut,
               paramBerhentiPosisi, paramKeluarPosisi,
               paramAnchorInPosisi, paramMooredInPosisi, paramZonaInID;
            paramBerhenti = new NpgsqlParameter("berhenti", NpgsqlDbType.Timestamp);
            paramBerangkat = new NpgsqlParameter("berangkat", NpgsqlDbType.Timestamp);
            paramKeluar = new NpgsqlParameter("keluar", NpgsqlDbType.Timestamp);
            paramAnchorIn = new NpgsqlParameter("anchor_in", NpgsqlDbType.Timestamp);
            paramAnchorOut = new NpgsqlParameter("anchor_out", NpgsqlDbType.Timestamp);
            paramMooredIn = new NpgsqlParameter("moored_in", NpgsqlDbType.Timestamp);
            paramMooredOut = new NpgsqlParameter("moored_out", NpgsqlDbType.Timestamp);
            paramBerhentiPosisi = new NpgsqlParameter("berhenti_posisi", NpgsqlDbType.Text);
            paramKeluarPosisi = new NpgsqlParameter("keluar_posisi", NpgsqlDbType.Text);
            paramAnchorInPosisi = new NpgsqlParameter("anchor_in_posisi", NpgsqlDbType.Text);
            paramMooredInPosisi = new NpgsqlParameter("moored_in_posisi", NpgsqlDbType.Text);
            paramZonaInID = new NpgsqlParameter("zona_in_id", NpgsqlDbType.Double);

            if (zonaIn.Berhenti.HasValue) paramBerhenti.Value = zonaIn.Berhenti.Value;
            if (zonaIn.Berangkat.HasValue) paramBerangkat.Value = zonaIn.Berangkat.Value;
            if (zonaIn.Keluar.HasValue) paramKeluar.Value = zonaIn.Keluar.Value;
            if (zonaIn.AnchorIn.HasValue) paramAnchorIn.Value = zonaIn.AnchorIn.Value;
            if (zonaIn.AnchorOut.HasValue) paramAnchorOut.Value = zonaIn.AnchorOut.Value;
            if (zonaIn.MooredIn.HasValue) paramMooredIn.Value = zonaIn.MooredIn.Value;
            if (zonaIn.MooredOut.HasValue) paramMooredOut.Value = zonaIn.MooredOut.Value;
            if (!string.IsNullOrEmpty(zonaIn.BerhentiPosisi)) paramBerhentiPosisi.Value = zonaIn.BerhentiPosisi;
            if (!string.IsNullOrEmpty(zonaIn.KeluarPosisi)) paramKeluarPosisi.Value = zonaIn.KeluarPosisi;
            if (!string.IsNullOrEmpty(zonaIn.AnchorInPosisi)) paramAnchorInPosisi.Value = zonaIn.AnchorInPosisi;
            if (!string.IsNullOrEmpty(zonaIn.MooredInPosisi)) paramMooredInPosisi.Value = zonaIn.MooredInPosisi;
            paramZonaInID.Value = zonaIn.ZonaInID;

            NpgsqlParameter[] Params = { };
            if (zonaIn.Berhenti.HasValue) Params = _ps.Add(Params, paramBerhenti);
            if (zonaIn.Berangkat.HasValue) Params = _ps.Add(Params, paramBerangkat);
            if (zonaIn.Keluar.HasValue) Params = _ps.Add(Params, paramKeluar);
            if (zonaIn.AnchorIn.HasValue) Params = _ps.Add(Params, paramAnchorIn);
            if (zonaIn.AnchorOut.HasValue) Params = _ps.Add(Params, paramAnchorOut);
            if (zonaIn.MooredIn.HasValue) Params = _ps.Add(Params, paramMooredIn);
            if (zonaIn.MooredOut.HasValue) Params = _ps.Add(Params, paramMooredOut);
            if (!string.IsNullOrEmpty(zonaIn.BerhentiPosisi)) Params = _ps.Add(Params, paramBerhentiPosisi);
            if (!string.IsNullOrEmpty(zonaIn.KeluarPosisi)) Params = _ps.Add(Params, paramKeluarPosisi);
            if (!string.IsNullOrEmpty(zonaIn.AnchorInPosisi)) Params = _ps.Add(Params, paramAnchorInPosisi);
            if (!string.IsNullOrEmpty(zonaIn.MooredInPosisi)) Params = _ps.Add(Params, paramMooredInPosisi);
            Params = _ps.Add(Params, paramZonaInID);

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int[] GetMooredZoneID()
        {
            int[] bigzoneids = { 1, 3, 4, 5 };
            return bigzoneids;
        }

        public Entity.Zona_In GetDetailShipsOnZonaByMMSIZonaID(int mmsi, int zonaID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select * ");
            //sb.Append("     to_char(masuk, 'dd-mm-yyyy HH24:mi:ss') masuk, ");
            //sb.Append("     to_char(berhenti, 'dd-mm-yyyy HH24:mi:ss') berhenti, ");
            //sb.Append("     to_char(berangkat, 'dd-mm-yyyy HH24:mi:ss') berangkat, ");
            //sb.Append("     to_char(anchor_in, 'dd-mm-yyyy HH24:mi:ss') anchor_in, ");
            //sb.Append("     to_char(anchor_out, 'dd-mm-yyyy HH24:mi:ss') anchor_out, ");
            //sb.Append("     to_char(moored_in, 'dd-mm-yyyy HH24:mi:ss') moored_in, ");
            //sb.Append("     to_char(moored_out, 'dd-mm-yyyy HH24:mi:ss') moored_out ");
            sb.Append(" from zona_in ");
            sb.Append(" where zona_in_id = (select max(zona_in_id) from zona_in where mmsi= :valmmsi and masuk_zona_id = :valzonaid) ");

            NpgsqlParameter paramMmsi, paramZonaID;

            paramMmsi = new NpgsqlParameter("valmmsi", NpgsqlDbType.Integer);
            paramZonaID = new NpgsqlParameter("valzonaid", NpgsqlDbType.Integer);
            paramMmsi.Value = mmsi;
            paramZonaID.Value = zonaID;

            NpgsqlParameter[] Params = { paramMmsi, paramZonaID };

            _ps.OpenConnectionAndCreateCommand();
            DataTable dt = _ps.ExecuteDataTable(sb.ToString(), Params);

            Entity.Zona_In result = new Entity.Zona_In();
            if (dt.Rows.Count == 0) return result;
            DataRow dr = dt.Rows[0];
            if (dr["masuk"] != DBNull.Value) result.Masuk = DateTime.Parse(dr["masuk"].ToString());
            if (dr["berhenti"] != DBNull.Value) result.Berhenti = DateTime.Parse(dr["berhenti"].ToString());
            if (dr["berangkat"] != DBNull.Value) result.Berangkat = DateTime.Parse(dr["berangkat"].ToString());
            if (dr["keluar"] != DBNull.Value) result.Keluar = DateTime.Parse(dr["keluar"].ToString());
            if (dr["anchor_in"] != DBNull.Value) result.AnchorIn = DateTime.Parse(dr["anchor_in"].ToString());
            if (dr["anchor_out"] != DBNull.Value) result.AnchorOut = DateTime.Parse(dr["anchor_out"].ToString());
            if (dr["moored_in"] != DBNull.Value) result.MooredIn = DateTime.Parse(dr["moored_in"].ToString());
            if (dr["moored_out"] != DBNull.Value) result.MooredOut = DateTime.Parse(dr["moored_out"].ToString());

            return result;
        }


        public DataTable GetListUrutanKedatanganKapal(DateTime dateFrom, DateTime dateTo)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select to_number(to_char(received_on,'yyyyMMddHH24MIssus'),'99999999999999999999') id,mmsi,ship_lbl,to_char(received_on, 'dd-MM-yyyy HH24:mm:ss') waktu, received_on ");
            sb.Append(" from ");
            sb.Append(" ( ");
            sb.Append("  select incoming_ais.mmsi,incoming_ais.received_on, replace(substring(ship_info.log_ais from position('VesselName' in ship_info.log_ais) + 13 for (position('\",\"' in substring(ship_info.log_ais from position('VesselName' in ship_info.log_ais) + 13 for (30))) - 1)),' ','') ship_lbl, ");
            sb.Append("         row_number() over (partition by incoming_ais.mmsi order by incoming_ais.received_on) as rw ");
            sb.Append("  from incoming_ais left outer join ship_info on incoming_ais.mmsi = ship_info.mmsi ");
            sb.Append("  where     incoming_ais.received_on between to_timestamp('" + dateFrom.ToString("yyyy-MM-dd HH:mm") + "','yyyy-MM-dd HH24:MI') and to_timestamp('" + dateTo.ToString("yyyy-MM-dd HH:mm") + "','yyyy-MM-dd HH24:MI') ");
            sb.Append("     and zona_id = 5 ");
            sb.Append(" ) find_oldest_mmsi  ");
            sb.Append(" where rw = 1 ");
            sb.Append(" order by received_on ");
            DataTable result= new DataTable();
            _ps.OpenConnectionAndCreateCommand();
            result = _ps.ExecuteDataTable(sb.ToString());
            return result;
        }

        public DataTable GetTravelInZones(int mmsi, DateTime dateFrom, DateTime dateTo, Dictionary<string,string> postgisConn) {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select zi.masuk_zona_id, zo.name,zi.masuk, zi.keluar  ");
            sb.Append(" from zona_in zi join ( ");
            sb.Append(" SELECT * ");
            sb.Append(" FROM dblink('dbname=" + postgisConn["dbname"] + " port=" + postgisConn["port"] + " host=" + postgisConn["host"] + " user=" + postgisConn["user"] + " password=" + postgisConn["password"] + "','select id,name from zone') ");
            sb.Append(" AS zone( id int,name varchar)) zo on zi.masuk_zona_id = zo.id ");
            sb.Append(" where zi.mmsi = :mmsi and zi.masuk between :dateFrom and :dateTo ");
            sb.Append(" order by mmsi,masuk ");
            DataTable result = new DataTable();
            _ps.OpenConnectionAndCreateCommand();          
            _ps.Comm.Parameters.AddWithValue("mmsi", mmsi);
            _ps.Comm.Parameters.AddWithValue("dateFrom", dateFrom);
            _ps.Comm.Parameters.AddWithValue("dateTo", dateTo);
            result = _ps.ExecuteDataTable(sb.ToString());
            return result;
        }
    }
}
