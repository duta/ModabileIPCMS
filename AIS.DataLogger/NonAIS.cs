﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBProvider;
using Npgsql;
using AIS.DataLogger.Interfaces;
using AIS.DataLogger.Entity;

namespace AIS.DataLogger
{
    internal class NonAIS : INonAIS
    {
        PostgreSQL _ps;

        public NonAIS(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public int? GetOIDByMMSI(int mmsi)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select oid ");
            sb.Append(" from non_ais ");
            sb.Append(" where mmsi::text like '%" + mmsi + "%' ");

            _ps.OpenConnectionAndCreateCommand();
            string result = _ps.ExecuteScalar(sb.ToString());
            if (result == "") return null;
            return int.Parse(result);
        }

        public List<Entity.NonAIS> GetListNonAIS()
        {
            StringBuilder sb = new StringBuilder();
            List<Entity.NonAIS> result = new List<Entity.NonAIS>();
            sb.Append(" select ");
            sb.Append(" 	na.mmsi, ");
            sb.Append(" 	si.log_ais ");
            sb.Append(" from non_ais na join ship_info si on na.mmsi = si.mmsi "); ;

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            Entity.NonAIS na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entity.NonAIS();
                    na.MMSI = int.Parse(reader["mmsi"].ToString());
                    na.Log_AIS = reader["log_ais"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            return result;
        }
        
        /// <summary>
        /// Daftar non-ais yang belum keluar zona 
        /// </summary>
        /// <returns></returns>
        public List<Entity.NonAIS> GetListNonAISWithLastPosition()
        {
            StringBuilder sb = new StringBuilder();
            List<Entity.NonAIS> result = new List<Entity.NonAIS>();
            sb.Append(" select na.*,si.log_ais log_t5 ");
            sb.Append(" from non_ais na join ship_info si on na.mmsi = si.mmsi  ");
            sb.Append(" where na.keluar_zona = false "); ;
            
            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            Entity.NonAIS na;
            string info_kpl, position_kpl;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entity.NonAIS();
                    na.MMSI = int.Parse(reader["mmsi"].ToString());
                    info_kpl = reader["log_t5"].ToString().Replace("}", ",");
                    position_kpl = reader["log_ais"].ToString().Replace("{", "");
                    na.Log_AIS = info_kpl + position_kpl;
                    na.Receive_On = DateTime.Parse(reader["last_draw"].ToString());
                    na.Zone = new Entity.Zone() { ID = int.Parse(reader["zona_id"].ToString()) };
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            return result;
        }

        public int Upsert(int mmsi, DateTime last_draw, string log_ais, int zona_id, bool keluar_zona)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            if (GetOIDByMMSI(mmsi).HasValue) {
                sb.Append("update non_ais set last_draw=:vallastdraw,log_ais = :vallogais, zona_id=:valzonaid, ");
                sb.Append("    keluar_zona= :valkeluarzona where mmsi = :valmmsi");            
            } else {
                sb.Append("insert into non_ais (last_draw, log_ais, zona_id, keluar_zona,mmsi) ");
                sb.Append(" values (:vallastdraw, :vallogais, :valzonaid, :valkeluarzona, :valmmsi) ");           
            }
            
            NpgsqlParameter paramMMSI, paramLastDraw, paramLogAis, paramZonaID, paramKeluarZona;
            paramMMSI = new NpgsqlParameter("valmmsi", NpgsqlTypes.NpgsqlDbType.Integer);
            paramLastDraw = new NpgsqlParameter("vallastdraw", NpgsqlTypes.NpgsqlDbType.Timestamp);
            paramLogAis = new NpgsqlParameter("vallogais", NpgsqlTypes.NpgsqlDbType.Text);
            paramZonaID = new NpgsqlParameter("valzonaid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramKeluarZona = new NpgsqlParameter("valkeluarzona", NpgsqlTypes.NpgsqlDbType.Boolean);

            NpgsqlParameter[] Params = { paramLastDraw, paramLogAis, paramZonaID, paramKeluarZona, paramMMSI };
            paramMMSI.Value = mmsi;
            paramLastDraw.Value = last_draw;
            paramLogAis.Value = log_ais;
            paramZonaID.Value = zona_id;
            paramKeluarZona.Value = keluar_zona;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }


        public int Delete(int mmsi)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from non_ais ");
            sb.Append(" where mmsi = :valmmsi ");

            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valmmsi", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params = { paramId };
            paramId.Value = mmsi;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }
    }
}
