﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBProvider;
using Npgsql;
using AIS.DataLogger.Interfaces;

namespace AIS.DataLogger
{
    internal class CCTVPointRepo : ICCTVPoint
    {
        PostgreSQL _ps;

        public CCTVPointRepo(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public List<AIS.DataLogger.Entity.CCTVPoint> GetCCTVs()
        {
            List<AIS.DataLogger.Entity.CCTVPoint> result = new List<AIS.DataLogger.Entity.CCTVPoint>();
            StringBuilder sb = new StringBuilder();
            sb.Append("select *, st_asgeojson(geom) geom2 from cctv");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    AIS.DataLogger.Entity.CCTVPoint curr = new AIS.DataLogger.Entity.CCTVPoint();
                    curr.geom = reader["geom2"].ToString();
                    curr.name = reader["name"].ToString().Trim();
                    curr.url = reader["url"].ToString();
                    curr.live_url = reader["live_url"] == DBNull.Value ? "" : reader["live_url"].ToString();
                    result.Add(curr);
                }
            }

            reader.Close();
            _ps.CloseConnection();
            return result;
        }

        public int InsertCCTV(AIS.DataLogger.Entity.CCTVPoint c)
        {
            StringBuilder sb3 = new StringBuilder();
            sb3.Append(" INSERT INTO cctv(name, geom, url,live_url) ");
            sb3.Append(" values ('" + c.name + "',ST_GeomfromEWKT('SRID=4326;POINT(" + c.geom + ")'),'" + c.live_url + "','" + c.live_url + "') ");

            _ps.OpenConnectionAndCreateCommand();
            return _ps.ExecuteNonQuery(sb3.ToString());
        }

        public int DeleteCCTV(AIS.DataLogger.Entity.CCTVPoint c)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from cctv ");
            sb.Append(" where name = '" + c.name + "' ");

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString());
            return returnValue;
        }

        public int UpdateCCTV(AIS.DataLogger.Entity.CCTVPoint c)
        {
            StringBuilder sb3 = new StringBuilder();
            sb3.Append(" UPDATE cctv SET geom = ST_GeomfromEWKT('SRID=4326;POINT(" + c.geom + ")'), live_url = :url WHERE name = :name ");

            _ps.OpenConnectionAndCreateCommand();
            return _ps.ExecuteNonQuery(sb3.ToString(), new NpgsqlParameter[] { new NpgsqlParameter("url", c.live_url), 
                                           new NpgsqlParameter("name", c.name)});
        }
    }
}
