﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;
using System.Web.Script.Serialization;
using AIS.DataLogger.Interfaces;
using AIS.DataLogger.Entity;
using AIS.Core;
using DBProvider;

namespace AIS.DataLogger.Oracle
{
    internal class NavStatus : INavStatus
    {
          DBProvider.Oracle _ora;

          public NavStatus(string connStr)
        {
            _ora = new DBProvider.Oracle(connStr);
        }

        public Dictionary<int, Entities.NavigationalStatus> GetAll()
        {
            Dictionary<int, Core.Entities.NavigationalStatus> result = new Dictionary<int, Core.Entities.NavigationalStatus>();
            Core.Entities.NavigationalStatus nav_item;
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id_navstatus, description");
            sb.Append(" from navigation_status ");

            _ora.OpenConnectionAndCreateCommand();
            OracleDataReader reader = _ora.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    nav_item = new Core.Entities.NavigationalStatus();
                    nav_item.id_navstatus = int.Parse(reader["id_navstatus"].ToString());
                    nav_item.description = reader["description"].ToString();
                    result[nav_item.id_navstatus] = nav_item;
                }
            }

            reader.Close();
            _ora.CloseConnection();

            return result;
        }
    }
}
