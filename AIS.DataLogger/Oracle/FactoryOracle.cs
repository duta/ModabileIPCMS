﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;
using AIS.DataLogger.Interfaces; 

namespace AIS.DataLogger.Oracle
{
    public class FactoryOracle : IFactory
    {
        string _connString;

        public FactoryOracle(string connString)
        {
            _connString = connString;
        }


        public INonAIS GetNonAIS()
        {
            throw new NotImplementedException();
        }

        public IAIS_Log GetAISLog()
        {
            return new AIS_Log(_connString);
        }

        public IZone GetZone()
        {
            return new Zone(_connString);
        }


        public IShipInfo GetShipInfo()
        {
            return new ShipInfo(_connString);
        }

        public INavStatus GetMasterNavStatus()
        {
            return new NavStatus(_connString);
        }

        public IShipType GetMasterShipType()
        {
            return new ShipType(_connString);
        }

        public IZonaIn GetZonaIn()
        {
            return new ZonaIn(_connString);
        }


        public IAISReceiverPos GetAISReceiverPos()
        {
            throw new NotImplementedException();
        }


        public IMail GetMail()
        {
            throw new NotImplementedException();
        }


        public IShipClose GetShipClose()
        {
            throw new NotImplementedException();
        }


        public IAssortedStuff GetAssortedStuff()
        {
            throw new NotImplementedException();
        }

        public string ConnectionString
        {
            get { throw new NotImplementedException(); }
        }


        public IVideoCCTV GetVideoCCTV()
        {
            throw new NotImplementedException();
        }


        public IZoneKT GetZoneKT()
        {
            throw new NotImplementedException();
        }


        public ICCTVPoint GetCCTVPoint()
        {
            throw new NotImplementedException();
        }
    }
}
