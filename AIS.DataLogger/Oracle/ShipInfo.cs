﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;
using System.Web.Script.Serialization;
using AIS.DataLogger.Interfaces;
using AIS.DataLogger.Entity;
using AIS.Core;
using DBProvider;

namespace AIS.DataLogger.Oracle
{
    internal class ShipInfo : IShipInfo
    {

        DBProvider.Oracle _ora;

        public ShipInfo(string connStr)
        {
            _ora = new DBProvider.Oracle(connStr);
        }

        public bool IsThereShipInfoByMMSI(int mmsi)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(*) jml ");
            sb.Append(" from ship_info ");
            sb.Append(" where mmsi = '" + mmsi + "' ");

            _ora.OpenConnectionAndCreateCommand();
            string result = _ora.ExecuteScalar(sb.ToString());
            return (result != "0");
        }

        public int? GetOIDShipInfoByMMSI(int mmsi)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select mmsi ");
            sb.Append(" from ship_info ");
            sb.Append(" where mmsi like '%" + mmsi + "%' ");

            _ora.OpenConnectionAndCreateCommand();
            string result = _ora.ExecuteScalar(sb.ToString());
            if (result == "") return null;
            return int.Parse(result);
        }

        public int InsertShipInfo(Incoming_AIS incoming)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into ship_info (log_ais, received_on, mmsi) values (:vallogais, :valreceivedon, :valmmsi)");

            OracleParameter paramLogAIS, paramReceivedOn, paramMMSI;
            paramLogAIS = new OracleParameter("vallogais", OracleType.VarChar);
            paramReceivedOn = new OracleParameter("valreceivedon", OracleType.Timestamp);
            paramMMSI = new OracleParameter("valmmsi", OracleType.Int32);

            OracleParameter[] Params = { paramLogAIS, paramReceivedOn, paramMMSI };
            paramLogAIS.Value = incoming.Log_AIS;
            paramReceivedOn.Value = DateTime.Now;
            paramMMSI.Value = incoming.MMSI;

            _ora.OpenConnectionAndCreateCommand();
            returnValue = _ora.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int UpdateShipInfo(Incoming_AIS incoming, int oid)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("update ship_info set log_ais = :vallogais, received_on= :valreceivedon where mmsi = :valmmsi");

            OracleParameter paramLogAIS, paramReceivedOn, paramMMSI;
            paramLogAIS = new OracleParameter("vallogais", OracleType.VarChar);
            paramReceivedOn = new OracleParameter("valreceivedon", OracleType.Timestamp);
            paramMMSI = new OracleParameter("valmmsi", OracleType.Int32);

            paramLogAIS.Value = incoming.Log_AIS;
            paramReceivedOn.Value = DateTime.Now;
            paramMMSI.Value = incoming.MMSI;
            OracleParameter[] Params = { paramLogAIS, paramReceivedOn, paramMMSI };

            _ora.OpenConnectionAndCreateCommand();
            returnValue = _ora.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public List<Incoming_AIS> GetDetectedShipsInCurrentTime()
        {
            throw new NotImplementedException();
        }


        public LabelValue[] GetLabelValueOfMMSIShipName(string filter)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("  select mmsi|| ' ' ||replace(substr(ship_info.log_ais, instr(ship_info.log_ais , 'VesselName',-1) + 13,(instr(ship_info.log_ais,'\",\"AISVersion',-1)-instr(ship_info.log_ais , 'VesselName',-1))-13),' ','') lbl ");
            sb.Append("  from ship_info  ");
            sb.Append("  where to_char(mmsi) like '%" + filter + "%' ");
            sb.Append("  union  ");
            sb.Append("  select mmsi|| ' ' ||replace(substr(ship_info.log_ais, instr(ship_info.log_ais , 'VesselName',-1) + 13,(instr(ship_info.log_ais,'\",\"AISVersion',-1)-instr(ship_info.log_ais , 'VesselName',-1))-13),' ','') lbl  ");
            sb.Append("  from ship_info  ");
            sb.Append("  where substr(log_ais, instr(ship_info.log_ais , 'VesselName',-1) + 13, 20) like '%" + filter.ToUpper() + "%'  "); ;


            LabelValue[] result;
            List<LabelValue> results = new List<LabelValue>();
            _ora.OpenConnectionAndCreateCommand();
            OracleDataReader reader = _ora.ExecuteDataReader(sb.ToString());
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var lblValue = new LabelValue();
                    lblValue.label = reader["lbl"].ToString().Trim();
                    lblValue.value = reader["lbl"].ToString().Trim();
                    results.Add(lblValue);
                }
            }

            reader.Close();
            _ora.CloseConnection();

            result = results.ToArray();
            return result;
        }


        public int Delete(int mmsi)
        {
            throw new NotImplementedException();
        }


        public string GetKodeKplFromKplAISSiuk(string CallSign)
        {
            throw new NotImplementedException();
        }


		public Incoming_AIS GetShipInfo(int oid)
		{
			throw new NotImplementedException();
		}

        public List<Entity.ShipInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public Entity.ShipInfo GetShipInfoByMMSI(int mmsi)
        {
            throw new NotImplementedException();
        }

        public int InsertShipInfo(Entity.ShipInfo incoming)
        {
            throw new NotImplementedException();
        }

        public int UpdateShipInfo(Entity.ShipInfo incoming, int mmsi)
        {
            throw new NotImplementedException();
        }
    }
}
