﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;
using System.Data;
using AIS.DataLogger.Interfaces;

namespace AIS.DataLogger.Oracle
{
    internal class ZonaIn :IZonaIn
    {
        DBProvider.Oracle _ora;

        public ZonaIn(string connStr)
        {
            _ora = new DBProvider.Oracle(connStr);
        }

        public int Insert(Entity.Zona_In zonaIn)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();

            sb.Append(" insert into zona_in ");
            sb.Append("             (zona_in_id, masuk,  ");
            sb.Append("              masuk_posisi, masuk_zona_id, ");
            sb.Append("              mmsi ");
            sb.Append("             ) ");
            sb.Append("      values (zona_in_id_seq.nextval, :masuk, ");
            sb.Append("              :masuk_posisi, :masuk_zona_id, ");
            sb.Append("              :mmsi ");
            sb.Append("             ) ");

            OracleParameter  paramMasuk, paramMasukPosisi, paramMasukZonaID, paramMMSI;
           
            paramMasuk = new OracleParameter("masuk", OracleType.Timestamp);
            paramMasukPosisi = new OracleParameter("masuk_posisi", OracleType.VarChar);
            paramMasukZonaID = new OracleParameter("masuk_zona_id", OracleType.Int32);
            paramMMSI = new OracleParameter("mmsi", OracleType.Int32);

            OracleParameter[] Params = { paramMasuk,paramMasukPosisi,paramMasukZonaID, paramMMSI };
         
            paramMasuk.Value = zonaIn.Masuk.Value;
            paramMasukPosisi.Value = zonaIn.MasukPosisi;
            paramMasukZonaID.Value = zonaIn.MasukZonaID;
            paramMMSI.Value = zonaIn.MMSI;

            _ora.OpenConnectionAndCreateCommand();
            returnValue = _ora.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int Update(Entity.Zona_In zonaIn)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" update zona_in ");
            sb.Append("    set ");
            if (zonaIn.Berhenti.HasValue) sb.Append("        berhenti = :berhenti").Append(",");
            if (zonaIn.Berangkat.HasValue) sb.Append("        berangkat = :berangkat").Append(",");
            if (zonaIn.Keluar.HasValue) sb.Append("        keluar = :keluar").Append(",");
            if (zonaIn.AnchorIn.HasValue) sb.Append("        anchor_in = :anchor_in").Append(",");
            if (zonaIn.AnchorOut.HasValue) sb.Append("        anchor_out = :anchor_out").Append(",");
            if (zonaIn.MooredIn.HasValue) sb.Append("        moored_in = :moored_in").Append(",");
            if (zonaIn.MooredOut.HasValue) sb.Append("        moored_out = :moored_out").Append(",");
            if (!string.IsNullOrEmpty(zonaIn.BerhentiPosisi)) sb.Append("        berhenti_posisi = :berhenti_posisi").Append(",");
            if (!string.IsNullOrEmpty(zonaIn.KeluarPosisi)) sb.Append("        keluar_posisi = :keluar_posisi").Append(",");
            if (!string.IsNullOrEmpty(zonaIn.AnchorInPosisi)) sb.Append("        anchor_in_posisi = :anchor_in_posisi").Append(",");
            if (!string.IsNullOrEmpty(zonaIn.MooredInPosisi)) sb.Append("        moored_in_posisi = :moored_in_posisi").Append(",");
            sb.Length--; // remove last comma
            sb.Append(" where  zona_in_id = :zona_in_id ");

            OracleParameter paramBerhenti, paramBerangkat, paramKeluar, 
               paramAnchorIn,paramAnchorOut,paramMooredIn,paramMooredOut,
               paramBerhentiPosisi, paramKeluarPosisi, 
               paramAnchorInPosisi, paramMooredInPosisi, paramZonaInID;
            paramBerhenti = new OracleParameter("berhenti", OracleType.Timestamp);
            paramBerangkat = new OracleParameter("berangkat", OracleType.Timestamp);
            paramKeluar = new OracleParameter("keluar", OracleType.Timestamp);
            paramAnchorIn = new OracleParameter("anchor_in", OracleType.Timestamp);
            paramAnchorOut = new OracleParameter("anchor_out", OracleType.Timestamp);
            paramMooredIn = new OracleParameter("moored_in", OracleType.Timestamp);
            paramMooredOut = new OracleParameter("moored_out", OracleType.Timestamp);
            paramBerhentiPosisi = new OracleParameter("berhenti_posisi", OracleType.VarChar);
            paramKeluarPosisi = new OracleParameter("keluar_posisi", OracleType.VarChar);
            paramAnchorInPosisi = new OracleParameter("anchor_in_posisi", OracleType.VarChar);
            paramMooredInPosisi = new OracleParameter("moored_in_posisi", OracleType.VarChar);
            paramZonaInID = new OracleParameter("zona_in_id", OracleType.Double);

            if (zonaIn.Berhenti.HasValue) paramBerhenti.Value = zonaIn.Berhenti.Value;
            if (zonaIn.Berangkat.HasValue) paramBerangkat.Value = zonaIn.Berangkat.Value;
            if (zonaIn.Keluar.HasValue) paramKeluar.Value = zonaIn.Keluar.Value;
            if (zonaIn.AnchorIn.HasValue) paramAnchorIn.Value = zonaIn.AnchorIn.Value;
            if (zonaIn.AnchorOut.HasValue) paramAnchorOut.Value = zonaIn.AnchorOut.Value;
            if (zonaIn.MooredIn.HasValue) paramMooredIn.Value = zonaIn.MooredIn.Value;
            if (zonaIn.MooredOut.HasValue) paramMooredOut.Value = zonaIn.MooredOut.Value;
            if (!string.IsNullOrEmpty(zonaIn.BerhentiPosisi)) paramBerhentiPosisi.Value = zonaIn.BerhentiPosisi;
            if (!string.IsNullOrEmpty(zonaIn.KeluarPosisi)) paramKeluarPosisi.Value = zonaIn.KeluarPosisi;
            if (!string.IsNullOrEmpty(zonaIn.AnchorInPosisi)) paramAnchorInPosisi.Value = zonaIn.AnchorInPosisi;
            if (!string.IsNullOrEmpty(zonaIn.MooredInPosisi)) paramMooredInPosisi.Value = zonaIn.MooredInPosisi;
            paramZonaInID.Value = zonaIn.ZonaInID;

            OracleParameter[] Params = {};
            if (zonaIn.Berhenti.HasValue) Params = Add(Params, paramBerhenti);
            if (zonaIn.Berangkat.HasValue) Params = Add(Params, paramBerangkat);
            if (zonaIn.Keluar.HasValue) Params = Add(Params, paramKeluar);
            if (zonaIn.AnchorIn.HasValue) Params = Add(Params, paramAnchorIn);
            if (zonaIn.AnchorOut.HasValue) Params = Add(Params, paramAnchorOut);
            if (zonaIn.MooredIn.HasValue) Params = Add(Params, paramMooredIn);
            if (zonaIn.MooredOut.HasValue) Params = Add(Params, paramMooredOut);
            if (!string.IsNullOrEmpty(zonaIn.BerhentiPosisi)) Params = Add(Params, paramBerhentiPosisi);
            if (!string.IsNullOrEmpty(zonaIn.KeluarPosisi)) Params = Add(Params, paramKeluarPosisi);
            if (!string.IsNullOrEmpty(zonaIn.AnchorInPosisi)) Params = Add(Params, paramAnchorInPosisi);
            if (!string.IsNullOrEmpty(zonaIn.MooredInPosisi)) Params = Add(Params, paramMooredInPosisi);
            Params = Add(Params, paramZonaInID);

            _ora.OpenConnectionAndCreateCommand();
            returnValue = _ora.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        private OracleParameter[] Add(OracleParameter[] Params, OracleParameter value)
        {
            Array.Resize<OracleParameter>(ref Params, Params.Length + 1);
            Params[Params.Length - 1] = value;
            return Params;
        }

        public double GetZonaInID()
        {
            return double.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfffffff"));
        }

        public double? GetMaxZonaInIdByMMSI(int mmsi)
        {
            double returnValue;
            StringBuilder sb = new StringBuilder();
            sb.Append(" select max(zona_in_id) from zona_in where mmsi= :valmmsi ");
            OracleParameter  paramMMSI;
            paramMMSI = new OracleParameter("valmmsi", OracleType.Int32);
            paramMMSI.Value = mmsi;
            OracleParameter[] Params = { paramMMSI };

            _ora.OpenConnectionAndCreateCommand();
            double.TryParse( _ora.ExecuteScalar(sb.ToString(), Params), out returnValue);
            double? result =null;
            result = returnValue == 0.0 ? result : returnValue;
            return result;
        }


        public int[] GetMooredZoneID()
        {
            int[] bigzoneids = { 1, 3, 4, 5 };
            return bigzoneids;
        }


        public Entity.Zona_In GetDetailShipsOnZonaByMMSIZonaID(int mmsi, int zonaID)
        {           
            StringBuilder sb = new StringBuilder();
            sb.Append(" select * ");
            //sb.Append("     to_char(masuk, 'dd-mm-yyyy HH24:mi:ss') masuk, ");
            //sb.Append("     to_char(berhenti, 'dd-mm-yyyy HH24:mi:ss') berhenti, ");
            //sb.Append("     to_char(berangkat, 'dd-mm-yyyy HH24:mi:ss') berangkat, ");
            //sb.Append("     to_char(anchor_in, 'dd-mm-yyyy HH24:mi:ss') anchor_in, ");
            //sb.Append("     to_char(anchor_out, 'dd-mm-yyyy HH24:mi:ss') anchor_out, ");
            //sb.Append("     to_char(moored_in, 'dd-mm-yyyy HH24:mi:ss') moored_in, ");
            //sb.Append("     to_char(moored_out, 'dd-mm-yyyy HH24:mi:ss') moored_out ");
            sb.Append(" from zona_in ");
            sb.Append(" where zona_in_id = (select max(zona_in_id) from zona_in where mmsi='"+ mmsi +"' and masuk_zona_id = '"+ zonaID +"') ");
            
            _ora.OpenConnectionAndCreateCommand();
             DataTable dt = _ora.ExecuteDataTable(sb.ToString());
             Entity.Zona_In result = new Entity.Zona_In();
             if (dt.Rows.Count == 0) return result;
             DataRow dr = dt.Rows[0];
             if (dr["masuk"] != DBNull.Value) result.Masuk = DateTime.Parse(dr["masuk"].ToString());
             if (dr["berhenti"] != DBNull.Value) result.Berhenti = DateTime.Parse(dr["berhenti"].ToString());
             if (dr["berangkat"] != DBNull.Value) result.Berangkat = DateTime.Parse(dr["berangkat"].ToString());
             if (dr["keluar"] != DBNull.Value) result.Keluar = DateTime.Parse(dr["keluar"].ToString());
             if (dr["anchor_in"] != DBNull.Value) result.AnchorIn = DateTime.Parse(dr["anchor_in"].ToString());
             if (dr["anchor_out"] != DBNull.Value) result.AnchorOut = DateTime.Parse(dr["anchor_out"].ToString());
             if (dr["moored_in"] != DBNull.Value) result.MooredIn = DateTime.Parse(dr["moored_in"].ToString());
             if (dr["moored_out"] != DBNull.Value) result.MooredOut = DateTime.Parse(dr["moored_out"].ToString());
           
            return result;
        }


        public DataTable GetListUrutanKedatanganKapal(DateTime dateFrom, DateTime dateTo)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("  select to_char(received_on,'yyyyMMddHH24MISSFF6') id, "); 
            sb.Append("         mmsi, "); 
            sb.Append("         ship_lbl, "); 
            sb.Append("         to_char(received_on, 'dd-MM-yyyy HH24:mm:ss') waktu,  "); 
            sb.Append("         received_on  "); 
            sb.Append("  from  "); 
            sb.Append("  (  "); 
            sb.Append("     select incoming_ais.mmsi, "); 
            sb.Append("          incoming_ais.received_on, ");
            sb.Append("          replace(substr(ship_info.log_ais, instr(ship_info.log_ais , 'VesselName',-1) + 13,(instr(ship_info.log_ais,'\",\"AISVersion',-1)-instr(ship_info.log_ais , 'VesselName',-1))-13),' ','') ship_lbl,  "); 
            sb.Append("          row_number() over (partition by incoming_ais.mmsi order by incoming_ais.received_on) as rw  "); 
            sb.Append("     from incoming_ais left outer join ship_info on incoming_ais.mmsi = ship_info.mmsi  "); 
            sb.Append("     where      ");
            sb.Append("         incoming_ais.received_on between to_timestamp('" + dateFrom.ToString("yyyy-MM-dd HH:mm") + "','yyyy-MM-dd HH24:MI') and to_timestamp('" + dateTo.ToString("yyyy-MM-dd HH:mm") + "','yyyy-MM-dd HH24:MI')  "); 
            sb.Append("         and zona_id = 5  "); 
            sb.Append("  ) find_oldest_mmsi   "); 
            sb.Append("  where rw = 1  "); 
            sb.Append("  order by received_on "); 

            DataTable result = new DataTable();
            _ora.OpenConnectionAndCreateCommand();
            result = _ora.ExecuteDataTable(sb.ToString());
            return result;
        }


        public DataTable GetTravelInZones(int mmsi, DateTime dateFrom, DateTime dateTo,Dictionary<string,string> postgisConn)
        {
            throw new NotImplementedException();
        }
    }
}
