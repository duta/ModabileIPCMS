﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;
using System.Web.Script.Serialization;
using AIS.DataLogger.Interfaces;
using AIS.DataLogger.Entity;
using AIS.Core;
using DBProvider;

namespace AIS.DataLogger.Oracle
{
    internal class AIS_Log : IAIS_Log
    {
        DBProvider.Oracle _ora;

        public AIS_Log(string connStr) {
            _ora = new DBProvider.Oracle(connStr);
        }
        
        public int InsertBulkIncomingAIS(List<Incoming_AIS> incoming)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into incoming_ais (log_ais, received_on, mmsi, zona_id) ");
            sb.Append(" values (:vallogais, :valreceivedon, :valmmsi, :valzonaid) ");

            OracleParameter paramLogAIS, paramReceivedOn, paramMMSI, paramZonaID;
            paramLogAIS = new OracleParameter("vallogais",  OracleType.VarChar);
            paramReceivedOn = new OracleParameter("valreceivedon", OracleType.Timestamp);
            paramMMSI = new OracleParameter("valmmsi", OracleType.Int32);
            paramZonaID = new OracleParameter("valzonaid", OracleType.Int32);
            OracleParameter[] Params = { paramLogAIS, paramReceivedOn, paramMMSI, paramZonaID };

            OracleTransaction trans = null;

            try
            {
                _ora.OpenConnectionAndCreateCommand();

                using (trans = _ora.Comm.Connection.BeginTransaction())
                {
                    using (_ora.Comm)
                    {
                        _ora.Comm.Transaction = trans;
                        _ora.Comm.Parameters.AddRange(Params);
                        _ora.Comm.CommandText = sb.ToString();

                        foreach (var item in incoming)
                        {
                            paramLogAIS.Value = item.Log_AIS;
                            paramReceivedOn.Value = item.Receive_On;
                            paramMMSI.Value = item.MMSI;
                            paramZonaID.Value = item.Zone.ID;
                            returnValue += _ora.Comm.ExecuteNonQuery();
                        }
                        trans.Commit();
                    }
                }

            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                _ora.CloseConnection();
                throw new ApplicationException("Gagal bulk insert position", ex);
            }
            finally
            {
                _ora.CloseConnection();
            }

            return returnValue; 
        }

        public Incoming_AIS LastPositionByMMSI(string mmsi)
        {
            throw new NotImplementedException();
        }

        public Entities.ShipDisplayFeatureCollection RecordedPositionByMMSIAndDate(string mmsi, DateTime from, DateTime to)
        {
            Entities.ShipDisplayFeatureCollection result = new Entities.ShipDisplayFeatureCollection();
            Entities.ShipProperty shipProperty;
            Entities.ShipFeature shipFeature;
            List<Entities.ShipFeature> shipFeatures = new List<Entities.ShipFeature>();
            GeoJSON.Point shipPoint;
            StringBuilder sb = new StringBuilder();

            //from date dikurangi 10 menit untuk dapat meng-cover received_time (received_on selalu > received_time)
            DateTime fromMin10 = from.AddMinutes(-10);

            sb.Append(" select log_ais ");
            sb.Append(" from ");
            sb.Append(" ( select log_ais,  ");
            sb.Append("            substr(log_ais, instr(log_ais, 'TimeStampReceiver', -1) + 20, 14) received_time,            ");
            sb.Append("            substr(log_ais, instr(log_ais, 'RepeatIndicator', -1) + 17, 1) repeated,  ");
            sb.Append("          mmsi ");
            sb.Append("   from incoming_ais ");
            sb.Append("   where received_on between to_timestamp('" + fromMin10.ToString("yyyy-MM-dd HH:mm") + "','yyyy-mm-dd HH24:MI')  ");
            sb.Append("     and to_timestamp('" + to.ToString("yyyy-MM-dd HH:mm") + "','yyyy-mm-dd HH24:MI') ");
            sb.Append("     and  mmsi = '" + mmsi + "'   ");
            sb.Append(" ) mmsi_coll  ");
            sb.Append(" where  ");
            sb.Append("       repeated = '0' ");
            sb.Append("       and received_time between '" + from.ToString("yyyyMMddHHmmssffffff") + "' and '" + to.ToString("yyyyMMddHHmmssffffff") + "'  ");


            JavaScriptSerializer serializer = new JavaScriptSerializer();
            _ora.OpenConnectionAndCreateCommand();
            OracleDataReader reader = _ora.ExecuteDataReader(sb.ToString());

            Hashtable item;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    item = serializer.Deserialize<Hashtable>(reader["log_ais"].ToString());
                    shipPoint = new GeoJSON.Point();
                    shipPoint.Coordinates = new double[] { double.Parse(item["Longitude"].ToString()), double.Parse(item["Latitude"].ToString()) };

                    shipProperty = new Entities.ShipProperty();
                    shipProperty.MMSI = int.Parse(item["MMSI"].ToString());
                    shipProperty.TimeStampReceiver = double.Parse(item["TimeStampReceiver"].ToString());
                    shipProperty.HDG = item["TrueHeading"].ToString();
                    shipProperty.SOG = item["SpeedOverGround"].ToString();
                    if (item.ContainsKey("NavigationalStatus"))
                    {
                        shipProperty.NavStatus = int.Parse(item["NavigationalStatus"].ToString());
                        shipProperty.NavStatusDescription = item["NavigationalStatusDescription"].ToString();
                    }
                    if (item.ContainsKey("RateOfTurn")) shipProperty.ROT = item["RateOfTurn"].ToString();
                    shipProperty.COG = item["CourseOverGround"].ToString();

                    shipFeature = new Entities.ShipFeature();
                    shipFeature.Geometry = shipPoint;
                    shipFeature.Properties = shipProperty;

                    shipFeatures.Add(shipFeature);
                }
            }
            reader.Close();
            _ora.CloseConnection();

            result.Features = shipFeatures.ToArray<Entities.ShipFeature>();
            return result;
        }


        public int Delete(int mmsi)
        {
            throw new NotImplementedException();
        }


        public Entities.ShipDisplayFeatureCollection RecordedPositionByMMSIAndDate(string mmsi, string from, string to)
        {
            throw new NotImplementedException();
        }

        public Entities.ShipDisplayFeatureCollection RecordedPositionByDate(string mmsi, string date, string trunc)
        {
            throw new NotImplementedException();
        }

        public int DeleteLTDate(DateTime tglAkhirLog)
        {
            throw new NotImplementedException();
        }


        public double CountDistance(List<double[]> coords)
        {
            throw new NotImplementedException();
        }


        public List<double[]> RecordedCoordinatesByMMSIAndDate(string mmsi, string from, string to)
        {
            throw new NotImplementedException();
        }


        public Entities.ShipDisplayFeatureCollection RecordedSmoothByMMSIAndDate(string mmsi, string from, string to)
        {
            throw new NotImplementedException();
        }


        public int DropAisLogTempTable()
        {
            throw new NotImplementedException();
        }

        public int CreateAisLogTempTable()
        {
            throw new NotImplementedException();
        }

        public List<Incoming_AIS_Fields> GetAISByLimitOffset(DateTime start, double limit, double offset)
        {
            throw new NotImplementedException();
        }

        public int TruncateIncomingAIS()
        {
            throw new NotImplementedException();
        }

        public List<Incoming_AIS_Fields> GetAISTempByLimitOffset(double limit, double offset)
        {
            throw new NotImplementedException();
        }


        public void VacuumFull()
        {
            throw new NotImplementedException();
        }


        public int DeleteBetweenDates(DateTime start, DateTime end)
        {
            throw new NotImplementedException();
        }


        public int InsertBulkIncomingAIS(List<Incoming_AIS_Fields> incoming)
        {
            throw new NotImplementedException();
        }


        public int InsertBulkIncomingAISTemp(List<Incoming_AIS_Fields> incoming)
        {
            throw new NotImplementedException();
        }


        public int InsertBulkIncomingAISByCopyIn(List<Incoming_AIS_Fields> incoming)
        {
            throw new NotImplementedException();
        }

        public Entities.ShipDisplayFeatureCollection RecordedPositionByMMSILast10(string v, int topx)
        {
            throw new NotImplementedException();
        }
    }
}
