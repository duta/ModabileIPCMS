﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;
using System.Web.Script.Serialization;
using AIS.DataLogger.Interfaces;
using AIS.DataLogger.Entity;
using AIS.Core;
using DBProvider;

namespace AIS.DataLogger.Oracle
{
    internal class ShipType : IShipType
    {

        DBProvider.Oracle _ora;

        public ShipType(string connStr)
        {
            _ora = new DBProvider.Oracle(connStr);
        }
        public Dictionary<int, Entities.ShipType> GetAll()
        {
            Dictionary<int, Core.Entities.ShipType> result = new Dictionary<int, Core.Entities.ShipType>();
            Core.Entities.ShipType shiptype_item;
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id_shiptype, description");
            sb.Append(" from ship_type ");

            _ora.OpenConnectionAndCreateCommand();
            OracleDataReader reader = _ora.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    shiptype_item = new Core.Entities.ShipType();
                    shiptype_item.id_shiptype = int.Parse(reader["id_shiptype"].ToString());
                    shiptype_item.description = reader["description"].ToString();
                    result[shiptype_item.id_shiptype] = shiptype_item;
                }
            }

            reader.Close();
            _ora.CloseConnection();

            return result;
        }
    }
}
