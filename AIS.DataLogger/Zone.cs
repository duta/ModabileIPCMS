﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBProvider;
using Npgsql; 
using AIS.DataLogger.Interfaces;

namespace AIS.DataLogger
{
    internal class Zone : IZone
    {
        PostgreSQL _ps;

        public Zone(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public bool ThisPointWithinZone(decimal lon, decimal lat, int zoneID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select ");
	        sb.Append(" st_within(this.g1,this.g2) is_within ");
            sb.Append(" from( ");
	        sb.Append(" select st_geomfromtext('POINT("+ lon +" " + lat + ")',4326) g1, geom g2  from zone where id= "+ zoneID);
            sb.Append(" ) as this ");
            _ps.OpenConnectionAndCreateCommand();
            bool result = _ps.ExecuteScalar(sb.ToString()) == "t" ? true : false;
            return result;
        }

        public Entity.Zone GetZoneByID(int ID)
        {
            Entity.Zone result = new Entity.Zone();
            StringBuilder sb = new StringBuilder();
            sb.Append("select name, pier_left, pier_length, pier_degree, pier_watched, check_masuk, check_keluar,st_asgeojson(geom) geom, st_asgeojson(pier_line) pier  from zone where id=" + ID);

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.ID = ID;
                    result.Name = reader["name"].ToString();
                    result.Geom = reader["geom"].ToString();
                    result.Pier = reader["pier"].ToString();
                    if (reader["pier_left"].ToString().Length > 0) result.PierLeft = bool.Parse(reader["pier_left"].ToString());
                    if (reader["pier_length"].ToString().Length > 0) result.PierLength = int.Parse(reader["pier_length"].ToString());
                    if (reader["pier_degree"].ToString().Length > 0) result.PierDegree = int.Parse(reader["pier_degree"].ToString());
                    if (reader["pier_watched"].ToString().Length > 0) result.PierWatch = bool.Parse(reader["pier_watched"].ToString());
                    result.CheckMasuk = bool.Parse(reader["check_masuk"].ToString());
                    result.CheckKeluar = bool.Parse(reader["check_keluar"].ToString());
                }
            }

            reader.Close();
            _ps.CloseConnection();
            return result;
        }

        public List<Entity.Zone> GetAllZone()
        {
            List<Entity.Zone> result = new List<Entity.Zone>();
            Entity.Zone zoneItem;
            StringBuilder sb = new StringBuilder();
            sb.Append("select id, name, pier_left, pier_length, pier_degree, pier_watched, check_masuk, check_keluar, st_asgeojson(geom) geom, st_asgeojson(pier_line) pier, zone_visible, zone_colour, zone_lastupdated, zone_updatedby  from zone where gid != 1 and gid !=6 order by id ");
            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    zoneItem = new Entity.Zone();
                    zoneItem.ID = int.Parse(reader["id"].ToString());
                    zoneItem.Name = reader["name"].ToString();
                    zoneItem.Geom = reader["geom"].ToString();
                    zoneItem.Pier = reader["pier"].ToString();
                    if (reader["pier_left"].ToString().Length > 0) zoneItem.PierLeft = bool.Parse(reader["pier_left"].ToString());
                    if (reader["pier_length"].ToString().Length > 0) zoneItem.PierLength = int.Parse(reader["pier_length"].ToString());
                    if (reader["pier_degree"].ToString().Length > 0) zoneItem.PierDegree = int.Parse(reader["pier_degree"].ToString());
                    if (reader["pier_watched"].ToString().Length > 0) zoneItem.PierWatch = bool.Parse(reader["pier_watched"].ToString());
                    if (reader["zone_updatedby"].ToString().Length > 0) zoneItem.UpdatedBy = reader["zone_updatedby"].ToString();
                    if (reader["zone_lastupdated"].ToString().Length > 0) zoneItem.LastUpdated = DateTime.Parse(reader["zone_lastupdated"].ToString());
                    if (reader["zone_visible"].ToString().Length > 0) zoneItem.Visible = bool.Parse(reader["zone_visible"].ToString());
                    if (reader["zone_colour"].ToString().Length > 0) zoneItem.Colour = reader["zone_colour"].ToString();
                    zoneItem.CheckMasuk = bool.Parse(reader["check_masuk"].ToString());
                    zoneItem.CheckKeluar = bool.Parse(reader["check_keluar"].ToString());
                    result.Add(zoneItem);
                }
            }
            return result;
        }

        public Entity.Zone GetZoneByPoint(decimal lon, decimal lat)
        {
            Entity.Zone result = new Entity.Zone();
            
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id,name ");
            sb.Append(" from ");
            sb.Append(" ( ");
            sb.Append(" 	select  ");
            sb.Append(" 		g1.id,g1.name, st_within(g1.g1,g1.g2)  ");
            sb.Append(" 	from( ");
            sb.Append("         select st_geomfromtext('POINT(" + lon + " " + lat + ")',4326) g1, geom g2, id,name   ");
            sb.Append("         from zone  ");
            sb.Append("         where id in ");
            sb.Append("         (  ");
            sb.Append("         select id from zone where gid !=6 ");
            sb.Append("         ) ");
            sb.Append("     ) as g1 ");
            sb.Append(" ) unionan ");
            sb.Append(" where st_within = 't' ");
            sb.Append(" order by id ");
            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.ID = int.Parse(reader["id"].ToString());
                    result.Name = reader["name"].ToString();
                }
            }

            reader.Close();
            _ps.CloseConnection();
            return result;
        }
        
        public int Update(Entity.Zone zone)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" update zone ");
            sb.Append(" set name = :valname ");
            sb.Append("     ,pier_watched = :valpier_watched ");
            sb.Append("     ,check_masuk = :valcheck_masuk ");
            sb.Append("     ,check_keluar = :valcheck_keluar ");
            sb.Append("     ,geom = ST_GeomfromEWKT('SRID=4326;MULTIPOLYGON(((" + zone.Geom + ")))') ");
            sb.Append("     ,zone_updatedby = :valupdatedby ");
            sb.Append("     ,zone_lastupdated = now() ");
            sb.Append("     ,zone_visible = :zone_visible ");
            sb.Append("     ,zone_colour = :zone_colour ");
            sb.Append(" where id = :valid ");

            NpgsqlParameter paramName, paramId, paramPier_watched, paramUpdatedby, paramCheck_masuk, paramCheck_keluar, paramVisible, paramColour;
            paramName = new NpgsqlParameter("valname", NpgsqlTypes.NpgsqlDbType.Text);
            paramPier_watched = new NpgsqlParameter("valpier_watched", NpgsqlTypes.NpgsqlDbType.Boolean);
            paramCheck_masuk = new NpgsqlParameter("valcheck_masuk", NpgsqlTypes.NpgsqlDbType.Boolean);
            paramCheck_keluar = new NpgsqlParameter("valcheck_keluar", NpgsqlTypes.NpgsqlDbType.Boolean);
            paramId = new NpgsqlParameter("valid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramUpdatedby = new NpgsqlParameter("valupdatedby", NpgsqlTypes.NpgsqlDbType.Text);
            paramVisible = new NpgsqlParameter("zone_visible", NpgsqlTypes.NpgsqlDbType.Boolean);
            paramColour = new NpgsqlParameter("zone_colour", NpgsqlTypes.NpgsqlDbType.Text);

            NpgsqlParameter[] Params = { paramName, paramPier_watched, paramCheck_masuk, paramCheck_keluar, paramId, paramUpdatedby,paramVisible, paramColour };
            paramName.Value = zone.Name;
            paramPier_watched.Value = zone.PierWatch;
            paramCheck_masuk.Value = zone.CheckMasuk;
            paramCheck_keluar.Value = zone.CheckKeluar;
            paramId.Value = zone.ID;
            paramUpdatedby.Value = zone.UpdatedBy;
            paramVisible.Value = zone.Visible.HasValue ? zone.Visible : true;
            paramColour.Value = string.IsNullOrEmpty(zone.Colour) ? "#FFF37A" : zone.Colour; 

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int Insert(Entity.Zone zone)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" insert into zone (name, pier_watched, check_masuk,check_keluar,geom, zone_updatedby, zone_lastupdated, zone_visible, zone_colour)");
            sb.Append(" values (:valname,:valpier_watched,:valcheck_masuk,:valcheck_keluar,ST_GeomfromEWKT('SRID=4326;MULTIPOLYGON(((" + zone.Geom + ")))'), :valupdatedby, now(), :zone_visible, :zone_colour) ");

            NpgsqlParameter paramName, paramPier_watched, paramUpdatedby, paramCheck_masuk, paramCheck_keluar, paramVisible, paramColour;
            paramName = new NpgsqlParameter("valname", NpgsqlTypes.NpgsqlDbType.Text);
            paramPier_watched = new NpgsqlParameter("valpier_watched", NpgsqlTypes.NpgsqlDbType.Boolean);
            paramUpdatedby = new NpgsqlParameter("valupdatedby", NpgsqlTypes.NpgsqlDbType.Text);
            paramCheck_masuk = new NpgsqlParameter("valcheck_masuk", NpgsqlTypes.NpgsqlDbType.Boolean);
            paramCheck_keluar = new NpgsqlParameter("valcheck_keluar", NpgsqlTypes.NpgsqlDbType.Boolean);
            paramVisible = new NpgsqlParameter("zone_visible", NpgsqlTypes.NpgsqlDbType.Boolean);
            paramColour = new NpgsqlParameter("zone_colour", NpgsqlTypes.NpgsqlDbType.Text);
            NpgsqlParameter[] Params = { paramName, paramPier_watched, paramCheck_masuk, paramCheck_keluar, paramUpdatedby, paramVisible, paramColour };
            paramName.Value = zone.Name;
            paramPier_watched.Value = zone.PierWatch;
            paramUpdatedby.Value = zone.UpdatedBy;
            paramCheck_masuk.Value = zone.CheckMasuk;
            paramCheck_keluar.Value = zone.CheckKeluar;
            paramVisible.Value = zone.Visible.HasValue ? zone.Visible: true;
            paramColour.Value = string.IsNullOrEmpty(zone.Colour) ? "#FFF37A" : zone.Colour; 

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int Delete(int id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from zone ");
            sb.Append(" where id = :valid ");

            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valid", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params = { paramId };
            paramId.Value = id;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }
    }
}
