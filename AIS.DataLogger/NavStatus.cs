﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using DBProvider;

namespace AIS.DataLogger
{
    internal class NavStatus : Interfaces.INavStatus   
    {
        PostgreSQL _ps;

        public NavStatus(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }


        public Dictionary<int, Core.Entities.NavigationalStatus> GetAll()
        {
            Dictionary<int, Core.Entities.NavigationalStatus> result = new Dictionary<int, Core.Entities.NavigationalStatus>();
            Core.Entities.NavigationalStatus nav_item;
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id_navstatus, description");
            sb.Append(" from navigation_status ");

            _ps.OpenConnectionAndCreateCommand(); 
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    nav_item = new Core.Entities.NavigationalStatus();
                    nav_item.id_navstatus = int.Parse(reader["id_navstatus"].ToString());
                    nav_item.description = reader["description"].ToString();
                    result[nav_item.id_navstatus] = nav_item;
                }
            }

            reader.Close();
            _ps.CloseConnection();

            return result;
        }
    }
}
