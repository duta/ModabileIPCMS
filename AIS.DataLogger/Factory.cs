﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIS.DataLogger.Entity;
using AIS.DataLogger.Interfaces; 

namespace AIS.DataLogger
{
    public class Factory : IFactory 
    {
        string _connString;

        public Factory(string connString)
        {
            _connString = connString;
        }

        #region IFactory Members

        public string ConnectionString { get { return _connString; } }

        public IMail GetMail()
        {
            return new Mail(_connString);
        }

        public IShipClose GetShipClose()
        {
            return new ShipClose(_connString);
        }

        public INonAIS GetNonAIS()
        {
            return new NonAIS(_connString);
        }

        public IAIS_Log GetAISLog()
        {
            return new AIS_Log(_connString);
        }

        public IZone GetZone()
        {
            return new Zone(_connString);
        }
             

        public IShipInfo GetShipInfo()
        {
            return new ShipInfo(_connString);
        }

        public INavStatus GetMasterNavStatus()
        {
            return new NavStatus(_connString);
        }

        public IShipType GetMasterShipType()
        {
            return new ShipType(_connString);
        }

        public IZonaIn GetZonaIn()
        {
            return new ZonaIn(_connString);
        }

        public IAISReceiverPos GetAISReceiverPos()
        {
            return new AISReceiverPos(_connString);
        }

        public IAssortedStuff GetAssortedStuff()
        {
            return new AssortedStuff(_connString);
        }

        public IVideoCCTV GetVideoCCTV()
        {
            return new VideoCCTVRepo(_connString);
        }

        public IZoneKT GetZoneKT()
        {
            return new ZoneKT(_connString);
        }

        public ICCTVPoint GetCCTVPoint()
        {
            return new CCTVPointRepo(_connString);
        }

        #endregion
       
    }
}
