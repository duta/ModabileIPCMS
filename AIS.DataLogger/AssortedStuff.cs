﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBProvider;
using Npgsql;
using AIS.DataLogger.Interfaces;
using System.Web.Script.Serialization;


namespace AIS.DataLogger
{
    internal class AssortedStuff : IAssortedStuff
    {
        PostgreSQL _ps;

        public AssortedStuff(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public List<Entity.ASPoint> GetASPoints()
        {
            List<Entity.ASPoint> result = new List<Entity.ASPoint>();
            StringBuilder sb = new StringBuilder();
            sb.Append("select st_asgeojson(\"geomPoint\") geom, type, properties from assorted_stuff where \"geomPoint\" is not null ");
            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Entity.ASPoint item = result.Find((e) => e.Geom.Equals(reader["geom"].ToString()));
                    if (item == null)
                    {
                        item = new Entity.ASPoint(reader["geom"].ToString());
                        result.Add(item);
                    }
                    Dictionary<string, object> props = serializer.Deserialize<Dictionary<string, object>>(reader["properties"].ToString());
                    item.properties.Add(new Entity.ASProperties() { 
                        type = reader["type"].ToString(),
                        data = props
                    });
                }
            }
            return result;
        }
    }
}
