﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIS.DataLogger.Entity
{
    public class Zone
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Geom { get; set; }
        public string Pier { get; set; }
        public bool PierLeft { get; set; }
        public int PierLength { get; set; }
        public int PierDegree { get; set; }
        public bool PierWatch { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime LastUpdated { get; set; }
        public bool CheckMasuk { get; set; }
        public bool CheckKeluar { get; set; }
        public string Colour { get; set; }
        public bool? Visible { get; set; }
    }

    public class Zona_In
    {
        public double ZonaInID { get; set; }
        public DateTime? Masuk { get; set; }
        public DateTime? Keluar { get; set; }
        public DateTime? Berhenti { get; set; }
        public DateTime? Berangkat { get; set; }
        public DateTime? AnchorIn { get; set; }
        public DateTime? AnchorOut { get; set; }
        public DateTime? MooredIn { get; set; }
        public DateTime? MooredOut { get; set; }
        public string MasukPosisi { get; set; }
        public string BerhentiPosisi { get; set; }
        public string KeluarPosisi { get; set; }
        public string AnchorInPosisi { get; set; }        
        public string MooredInPosisi { get; set; }        
        public int MasukZonaID { get; set; }
        public int MMSI { get; set; }
    }
}
