﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIS.DataLogger.Entity
{
    public class CCTVPoint
    {
        public string geom { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string live_url { get; set; }
    }
}