﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIS.DataLogger.Entity
{
    public class ShipInfo
    {
        public DateTime received_on { get; set; }
        public int mmsi { get; set; }
        public int etaminute { get; set; }
        public int repeatindicator { get; set; }
        public int etamonth { get; set; }
        public int dte { get; set; }
        public int spare { get; set; }
        public double dimensiontostern { get; set; }
        public int positionfixtype { get; set; }
        public int messagetype { get; set; }
        public double draught { get; set; }
        public string vesselname { get; set; }
        public string destination { get; set; }
        public int etaday { get; set; }
        public string shiptypedesc { get; set; }
        public double dimensiontoport { get; set; }
        public double timestampreceiver { get; set; }
        public int imonumber { get; set; }
        public double dimensiontobow { get; set; }
        public int etahour { get; set; }
        public int aisversion { get; set; }
        public string callsign { get; set; }
        public double dimensiontostarboard { get; set; }
        public int shiptype { get; set; }
        public double grt { get; set; }
        public int enginehp { get; set; }
        public bool enginediesel { get; set; } 
    }
}
