﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeoJSON;
using System.Web.Script.Serialization;

namespace AIS.DataLogger.Entity
{
    public class ASPoint : Feature<Point, List<ASProperties>>
    {
        public string Geom { get; private set; }
        public List<ASProperties> properties { get; private set; }
        public ASPoint(string geom)
        {
            Geom = geom;
            this.Geometry = new Point();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            this.Geometry = serializer.Deserialize<Point>(Geom);
            properties = new List<ASProperties>();
        }
    }

    public class ASProperties
    {
        public string type { get; set; }
        public Dictionary<string, object> data = new Dictionary<string, object>();
    }
}
