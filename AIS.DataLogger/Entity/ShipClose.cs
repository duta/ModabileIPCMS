﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIS.DataLogger.Entity
{
    public class ShipCloseData
    {
        public int mmsi { get; set; }
        public string namakpl { get; set; }
        public int other_mmsi { get; set; }
        public string namakpllain { get; set; }
        public int distance { get; set; }
        public bool is_heading { get; set; }
    }

    public class ShipCloseConfig
    {
        public int yellow_range { get; set; }
        public int red_range { get; set; }
        public int check_range { get; set; }
        public int prediction_interval { get; set; }
        public int prediction_length { get; set; }
    }
}
