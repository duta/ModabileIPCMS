﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIS.DataLogger.Entity
{
    public class VideoCCTV
    {
        public int vc_id { get; set; }
        public string vc_location { get; set; }
        public string vc_path { get; set; }
        public string vc_filename { get; set; }
        public DateTime vc_recordeddate { get; set; }
        public DateTime vc_lastupdate { get; set; }
    }
}
