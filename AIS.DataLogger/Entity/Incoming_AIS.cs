﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIS.DataLogger.Entity
{
    public class Incoming_AIS
    {
        public string Log_AIS { get; set; }
        public DateTime Receive_On { get; set; }
        public int MMSI { get; set; }
        public Zone Zone { get; set; }
		public int Arp_Id { get; set; }
    }

    public class Incoming_AIS_Fields
    {
        public DateTime received_on { get; set; }
        public int mmsi { get; set; }
        public int arp_id { get; set; }
        public int messagetype { get; set; }
        public int slottimeout { get; set; }
        public int iais_timestamp { get; set; }
        public double dimensiontobow { get; set; }
        public int navigationalstatus { get; set; }
        public double trueheading { get; set; }
        public int positionaccuracy { get; set; }
        public int repeatindicator { get; set; }
        public double longitude { get; set; }
        public double courseoverground { get; set; }
        public int maneuverindicator { get; set; }
        public string navstatusdesc { get; set; }
        
        public double dimensiontostern { get; set; }
        public string destination { get; set; }
        public double dimensiontostarboard { get; set; }
        public int spare { get; set; }
        public int syncstate { get; set; }
        public double timestampreceiver { get; set; }
        public string submessage { get; set; }
        public double dimensiontoport { get; set; }
        public double draught { get; set; }
        public double rateofturn { get; set; }
        public double speedoverground { get; set; }
        public double latitude { get; set; }
        public double raimflag { get; set; }
        public int etamonth { get; set; }
        public int etaday { get; set; }
        public int etahour { get; set; }
        public int etaminute { get; set; }

        public int zona_id { get; set; }
        public string zona_name { get; set; }

    }

    public class LabelValue{
        public string label{ get;set;}
        public string value{ get;set;}
    }

    public class NonAIS  : Incoming_AIS{
        public bool KeluarZona { get; set; }
    }
    
}
