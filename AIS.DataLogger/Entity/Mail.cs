﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIS.DataLogger.Entity
{
    public class MailConfig
    {
        public string server { get; set; }
        public int port { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public bool enableSSL { get; set; }
    }
    public class MailTarget
    {
        public string address { get; set; }
        public string name { get; set; }
    }
}
