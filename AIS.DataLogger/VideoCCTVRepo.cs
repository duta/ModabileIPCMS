﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AIS.DataLogger.Interfaces;
using AIS.DataLogger.Entity;
using DBProvider;
using Dapper;
using Npgsql;

namespace AIS.DataLogger
{
    internal class VideoCCTVRepo : IVideoCCTV 
    {
        PostgreSQL _ps;

        public VideoCCTVRepo(string connStr)
        {
            _ps = new PostgreSQL(connStr);
        }

        public int Insert(VideoCCTV videoCctv)
        {
            int result = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" insert into video_cctv (vc_location,vc_path,vc_recordeddate,vc_filename,vc_lastupdate) ");
            sb.Append(" values (:vc_location, :vc_path, :vc_recordeddate, :vc_filename, :vc_lastupdate) ");
            _ps.OpenConnectionAndCreateCommand();
            result = _ps.Conn.Execute(sb.ToString(), new { vc_location = videoCctv.vc_location, 
                vc_path = videoCctv.vc_path, vc_recordeddate = videoCctv.vc_recordeddate,
                vc_filename = videoCctv.vc_filename, vc_lastupdate = videoCctv.vc_lastupdate});
            _ps.CloseConnection();
            return result;
        }

        public int Delete(int vc_id)
        {
            int result = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from video_cctv ");
            sb.Append(" where vc_id = :vc_id ");
            _ps.OpenConnectionAndCreateCommand();
            result = _ps.Conn.Execute(sb.ToString(), new { vc_id = vc_id });
            _ps.CloseConnection();
            return result;
        }

        public List<VideoCCTV> GetByDateRange(DateTime from, DateTime to)
        {
            List<VideoCCTV> cctvs = new List<VideoCCTV>();
            StringBuilder sb = new StringBuilder();
            sb.Append(" select * from video_cctv where vc_recordeddate between :from and :to ");
            _ps.OpenConnectionAndCreateCommand();
            cctvs = _ps.Conn.Query<VideoCCTV>(sb.ToString(), new { from = from, to = to }).ToList();
            _ps.CloseConnection();
            return cctvs;
        }

    }
}
