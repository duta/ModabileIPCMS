@echo off
for /F "tokens=3 delims=: " %%H in ('sc query "AISReceiverService" ^| findstr "        STATE"') do (
  if /I "%%H" NEQ "RUNNING" (
   echo starting AISReceiverService
   net start AISReceiverService
  )	
  echo done 	
)