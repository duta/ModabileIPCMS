﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeoJSON;
using DreamSongs.MongoRepository;  

namespace AIS.Core
{
    public class Entities
    {
        public class ShipProperty
        {
            public int MMSI { get; set; }
            public double  TimeStampReceiver { get; set; }
            public string HDG { get; set; }
            public string SOG { get; set; }
            public int NavStatus { get; set; }
            public string NavStatusDescription { get; set; }
            public string ROT { get; set; }
            public string COG { get; set; }
            //type 5
            public string ShipName { get; set; }
            public int ShipType { get; set; }
            public string ShipTypeDescription { get; set; }
            public string CallSign { get; set; }
            public int IMONumber { get; set; }
            public string ToPort { get; set; }
            public string ToStarBoard { get; set; }
            public string ToBow { get; set; }
            public string ToStern { get; set; }
            public string ETAMonth { get; set; }
            public string ETADay { get; set; }
            public string ETAHour { get; set; }
            public string ETAMinute { get; set; }
            public string Draught { get; set; }
            public string Destination { get; set; }
            public decimal Grt { get; set; }

            //zonasi
            public int ZoneID { get; set; }
            public string ZoneName { get; set; }

            //ais receiver position
            public int StationID { get; set; }
            public string StationName { get; set; }

            //danger score
            public int DangerDist { get; set; }
            public bool DangerHead { get; set; }

            //siuk
            public decimal GrtBySiuk {get; set;}

            //fleetmon
            public string FmBattery { get; set; }
            public double? FmWavePeriod { get; set; }
            public double? FmTempWater { get; set; }
            public double? FmIceConcentration { get; set; }
            public DateTime? FmFirstCreated { get; set; }
            public double? FmAirPressure { get; set; }
            public double? FmCurrentSpeed { get; set; }
            public int FmGpsDeviceId { get; set; }
            public int? FmWindDir { get; set; }
            public double? FmWindSpeed { get; set; }
            public int? FmWaveDir { get; set; }
            public double? FmTemperatureAir { get; set; }
            public string FmName { get; set; }
            public int? FmCurrentDir { get; set; }
            public double? FmWindSurfaceGust { get; set; }
            public bool FmAis { get; set; }
            public int? FmTotalCloudCover { get; set; }
            public int? FmRelativeHumidity { get; set; }
            public double? FmWaveHeight { get; set; }
            public string FmEsn { get; set; }
            public string FmLocation { get; set; }
        }

        public class NavigationalStatus
        { 
            public int id_navstatus{get; set;}
            public string description { get; set; }
        }

        public class ShipType
        {
            public int id_shiptype { get; set; }
            public string description { get; set; }
        }

        public class WindProperty
        {
            public string id_sensor { get; set; }
            public float speed { get; set; }
            public float direction { get; set; }
            public string name { get; set; }
            public string location { get; set; }
        }

        public class WindFeature : Feature<Point, WindProperty>
        {
            public WindFeature()
            {
                this.Properties = new WindProperty();
            }
        }

        public class WindFeatureCollection : GeoJSON.FeatureCollection<WindFeature>
        {
        }
        
        public class AISReceiverPos {
            public int arp_id { get; set; }
            public string arp_name { get; set; }
        }

        public class ShipSIUK
        {
            public double Tppkb1_Nomor { get; set; }
			public double IDJasaTambat { get; set; }
			public double NoForm { get; set; }
			public double NoBuktiTambat { get; set; }
            public int MMSI { get; set; }
            public string KodeKapal { get; set; }
            public string NamaKapal { get; set; }
            public string Asal { get; set; }
            public string Tujuan { get; set; }
            public int? KadeAwal { get; set; }
            public int? KadeAkhir { get; set; }
            public bool HeadStart { get; set; }
            public DateTime Mulai { get; set; }
        }

        public class RadarTarget
        {
            private int _id = 0;
            private string _name = "";
            private double _stw = 0; // speed in knot
            private double _cpa = 0; // closest point approach
            private decimal _range = 0;
            private decimal _truebearing = 0;
            public int ID { get { return _id; } set { _id = value; } }
            public string Name { get {return _name;} set { _name = value; } }
            public double STW { get { return _stw; } set { _stw = value; } }
            public double CPA { get { return _cpa; } set { _cpa = value; } }
            public Decimal Range { get { return _range; } set { _range = value; } }
            public Decimal TrueBearing { get { return _truebearing; } set { _truebearing = value; } }
        }

        public class RadarTargetFeature : Feature<Point, RadarTarget>
        {
            public RadarTargetFeature() {
                this.Properties = new RadarTarget();
            }
        }
        
        public class ShipFeature : Feature<Point, ShipProperty>
        {
            public ShipFeature() {
                this.Properties = new ShipProperty();                
            }
        }

        public class ShipFeatureMongoEntity : Entity
        {
            public ShipFeature ShipFeature { get; set; }
        }

        public class NavStatusMongoEntity : Entity
        {
            public NavigationalStatus NavigationalStatus { get; set; }
        }

        public class AISReceiverPosMongoEntity : Entity
        {
            public AISReceiverPos AISReceiverPos { get; set; }
        }

        public class ShipTypeMongoEntity : Entity
        {
            public ShipType  ShipType { get; set; }
        }

        public class ShipDisplayFeatureCollection : GeoJSON.FeatureCollection<ShipFeature>
        {
            public ShipDisplayFeatureCollection()
            {

            }
            public ShipDisplayFeatureCollection(List<ShipFeature> all)
            {
                this.Features = all.ToArray();
            }
        }
        public class RadarTargetFeatureCollection : GeoJSON.FeatureCollection<RadarTargetFeature>
        {
        }

        public class Symbology {
            public double SymbologyID { get; set; }
            public string SymbologyName { get; set; }
            public string SymbologyCategory { get; set; }
        }

        public class SymbologyFeature : Feature<Point, Symbology>
        {
        }

        public class SymbologyFeatureMongoEntity : Entity
        {
            public SymbologyFeature SymbologyFeature { get; set; }
        }

        public class SymbologyFeatureCollection : GeoJSON.FeatureCollection<SymbologyFeature>
        {
        }

        public class NonAIS {
            public int MMSI { get; set; }
            public string ShipName { get; set; }
            public string CallSign { get; set; }
            public string TglLokasi { get; set; }
            public string KeluarZona { get; set; }
        }

        public class NonAISFeature : Feature<Point, NonAIS>
        {
        }

        public class NonAISFeatureCollection : GeoJSON.FeatureCollection<NonAISFeature>
        {
        }

        public class PolylineGeo  : GeoJSON.Geometry<double[][]>        
        {
            public PolylineGeo() : base(ObjectType.LineString)
		    {
		    }
        }

        public class Polyline
        {
            public double PolyID { get; set; }
            public string Description { get; set; }
        }

        public class PolylineFeature : Feature<PolylineGeo, Polyline>
        {
        }

        public class PolylineFeatureMongoEntity : Entity
        {
            public PolylineFeature PolylineFeature { get; set; }
        }

        public class PolylineFeatureCollection : GeoJSON.FeatureCollection<PolylineFeature>
        {
        }
    }
}
