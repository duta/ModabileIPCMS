﻿var lon = 112.7;
var lat = -7.2;
var zoom = 10;
var mapGeoJson, zona_layerGeoJson, zonaControl, lineSelectControl, drawControls, drawButtons,
    zoneDrawFeature, zoneModifyFeature, renderer;
var hostname = location.hostname;
var url = "http://" + hostname + ":8008/mapguide/mapagent/mapagent.fcgi?USERNAME=Anonymous&";

//Adjust the scale assumptions for MapGuide layers
//Tiled layers MUST use a DPI value of 96, untiled layers can use a
//different DPI value which will be passed to the server as a parameter.
//Tiled and untiled layers must adjust the OL INCHES_PER_UNIT values
//for any degree-based projections.
var metersPerUnit = 111319.4908;  //value returned from mapguide
var inPerUnit = OpenLayers.INCHES_PER_UNIT.m * metersPerUnit;
OpenLayers.INCHES_PER_UNIT["dd"] = inPerUnit;
OpenLayers.INCHES_PER_UNIT["degrees"] = inPerUnit;
OpenLayers.DOTS_PER_INCH = 96;

var SHADOW_Z_INDEX = 10;
var MARKER_Z_INDEX = 11;

$(document).ready(function () {
    initMapGeoJson();
    $("#footer2").html("Copyright &copy; " + (new Date).getFullYear() + ". All Rights Reserved.")
    RequestZone();
    initDrawControls();
    initIconButton();
    initDialog();
    initEntryZona();
    initSavingDialog();
    toggleButtons($("#cmdDrawModeOff"));
});

function initDialog() {
    // Dialog
    $('#dialog').dialog({
        autoOpen: false,
        resizable: false,
        width: 400,
        height: 400,
        buttons: {
            "Ok": function () {
                $(this).dialog("close");
            }
        }
    });

    //color-picker
    $('#colorpicker-popup').colorpicker();
}
var entryFeature = null;
function initEntryZona() {
    $("#entryZoneDialog .sort-me").sortable({ stop: ZonaEntryUpdate });
    $('.lonlatMode').change(function () {
        $(this).siblings('.sort-me').find('.sort-li').lonlat('useDMS', $(this).val() == 1);
        $(this).siblings('.sort-li').lonlat('useDMS', $(this).val() == 1);
    });
    $("#entryZoneDialog .sort-add").click(function () {
        var llBar = $('<li class="ui-state-default sort-li" style="width:440px;height:20px;margin-bottom:2px;"></li>').lonlat({ useDMS: $('#entryZoneDialog .lonlatMode').val() == 1, changed: ZonaEntryUpdate });
        var closeDiv = $('<div id="cBtn" style="cursor:pointer;float:right;margin:2px;margin-left:0px;color:#FF3010">X</div>');
        llBar.append(closeDiv);
        $(this).siblings('.sort-me').append(llBar);
        closeDiv.click(function () {
            llBar.remove();
        });
    });
    $('#entryZoneDialog').dialog({
        autoOpen: false,
        resizable: true,
        width: 500,
        height: 250,
        modal: false,
        buttons: {
            "Save": function () {
                if (entryFeature.attributes.Name == null) {
                    //insert
                    zoneToSave = entryFeature;
                    InsertZone($('#txtNameEntryZona').val(), entryFeature.geometry.toString().replace('MULTIPOLYGON(((', '').replace(')))', ''), $('#chkZonaEntryWatch').is(':checked'), $('#chkZonaEntryMasuk').is(':checked'), $('#chkZonaEntryKeluar').is(':checked'), $('#chkZonaVisible').is(':checked'), $('#colorpicker-popup').val());
                }
                else {
                    //update
                    zoneToSave = entryFeature;
                    UpdateZone(entryFeature.attributes.ID, $('#txtNameEntryZona').val(), entryFeature.geometry.toString().replace('MULTIPOLYGON(((', '').replace(')))', ''), $('#chkZonaEntryWatch').is(':checked'), $('#chkZonaEntryMasuk').is(':checked'), $('#chkZonaEntryKeluar').is(':checked'), $('#chkZonaVisible').is(':checked'), $('#colorpicker-popup').val());
                }
                $(this).attr('forceClose', "true");
                $(this).dialog("close");
                RequestZone();
            }
        },
        open: function (event, ui) {
            $(this).attr('forceClose', "false"); //reset the flag each time
        },
        beforeClose: function (event, ui) {
            if ($(this).attr('forceClose') == "true") return true;
            var r = confirm("Batal entri zone?")
            if (r == true) {
                RequestZone();
            }
            else {
                return false;
            }
        }
    });
    $('#zoneEntryFeature').click(function () {
        $("#txtNameEntryZona").val('');
        $("#chkZonaEntryWatch").prop('checked', false);
        $("#chkZonaEntryMasuk").prop('checked', false);
        $("#chkZonaEntryKeluar").prop('checked', false);
        $("#entryZoneDialog .sort-me").children().remove();
        $('#entryZoneDialog').dialog('open');
        var ring = new OpenLayers.Geometry.LinearRing([]);
        var polygon = new OpenLayers.Geometry.Polygon([ring]);
        var multpolygon = new OpenLayers.Geometry.MultiPolygon([polygon]);
        entryFeature = new OpenLayers.Feature.Vector(multpolygon);
        zona_layerGeoJson.addFeatures([entryFeature]);
    });
}
function ZonaEntryUpdate() {
    if ($("#entryZoneDialog .sort-li").length < 3) return;
    var points = [];
    $("#entryZoneDialog .sort-li").each(function () {
        points.push(new OpenLayers.Geometry.Point($(this).find('#iLonD').val(), $(this).find('#iLatD').val()));
    });
    entryFeature.geometry.components[0].components[0] = new OpenLayers.Geometry.LinearRing(points);
    zona_layerGeoJson.setVisibility(false);
    zona_layerGeoJson.setVisibility(true);
    mapGeoJson.zoomToExtent(entryFeature.geometry.bounds);
}
function EntryZone(id) {
    $('#dialog').dialog('close');
    entryFeature = zona_layerGeoJson.getFeaturesByAttribute("ID", id)[0];
    $("#txtNameEntryZona").val(entryFeature.attributes.Name);
    $("#chkZonaEntryWatch").prop('checked', entryFeature.attributes.PierWatch == "true");
    $("#chkZonaEntryMasuk").prop('checked', entryFeature.attributes.CheckMasuk == "true");
    $("#chkZonaEntryKeluar").prop('checked', entryFeature.attributes.CheckKeluar == "true");
    $("#entryZoneDialog .sort-me").children().remove();
    $('#entryZoneDialog').dialog('open');
    mapGeoJson.zoomToExtent(entryFeature.geometry.bounds);
    for (var i = 0; i < entryFeature.geometry.components[0].components[0].components.length; i++) {
        var llBar = $('<li class="ui-state-default sort-li" style="width:440px;height:20px;margin-bottom:2px;"></li>').lonlat({ useDMS: $('#entryZoneDialog .lonlatMode').val() == 1 });
        var closeDiv = $('<div id="cBtn" style="cursor:pointer;float:right;margin:2px;margin-left:0px;color:#FF3010">X</div>');
        llBar.append(closeDiv);
        $("#entryZoneDialog .sort-me").append(llBar);
        $(llBar).find('#iLonD').val(entryFeature.geometry.components[0].components[0].components[i].x);
        $(llBar).find('#iLatD').val(entryFeature.geometry.components[0].components[0].components[i].y);
        $(llBar).find('#iLonD').trigger('change');
        $(llBar).find('#iLatD').trigger('change');
        closeDiv.click(function () {
            llBar.remove();
        });
    }
}

function initSavingDialog() {
    // Dialog
    $('#savingDialog').dialog({
        autoOpen: false,
        resizable: false,
        width: 200,
        height: 200,
        modal: true,
        buttons: {
            "Save": function () {
                SavingZone();
            }
        },
        beforeClose: function (event, ui) {
            if (!isSaveBeforeCloseSaving()) {
                alert('Simpan Zona terlebih dahulu');
                return false;
            }
            if ($('#zoneModifyFeature').is(":checked")) {
                var r = confirm("Batal merubah zone?")
                if (r == true) {
                    RequestZone();
                    toggleButtons($("#cmdDrawModeOff"));
                }
                else {
                    return false;
                }
            }
        }
    });
}

function initIconButton() {
    $('#cmdDialog').button({
        icons: {
            primary: "ui-icon-calculator"
        }
    }).click(function () {
        $('#dialog').dialog('open');
        return false;
    });
    $('#cmdDialogDftrLine').button({
        icons: {
            primary: "ui-icon-calculator"
        }
    }).click(function () {
        $('#dialogDftrPolyline').dialog('open');
        return false;
    });
}

function initDrawControls() {
    zoneDrawFeature = new OpenLayers.Control.DrawFeature(zona_layerGeoJson,
                        OpenLayers.Handler.Polygon);
    polyLDrawFeature = new OpenLayers.Control.DrawFeature(polyL_layerGeoJson,
                        OpenLayers.Handler.Path);
    drawControls = {
        zoneDrawFeature: new OpenLayers.Control.DrawFeature(zona_layerGeoJson,
                        OpenLayers.Handler.Polygon,
                        { eventListeners: { "featureadded": newZoneAdded } }),
        zoneModifyFeature: new OpenLayers.Control.ModifyFeature(zona_layerGeoJson),
        polyLDrawFeature: new OpenLayers.Control.DrawFeature(polyL_layerGeoJson,
                        OpenLayers.Handler.Path,
                        { eventListeners: { "featureadded": newPolyLAdded } }),
        polyLModifyFeature: new OpenLayers.Control.ModifyFeature(polyL_layerGeoJson),
    };

    for (var key in drawControls) {
        mapGeoJson.addControl(drawControls[key]);
    }

    drawButtons = {
        "zoneDrawFeature": $("#zoneDrawFeature"),
        "zoneModifyFeature": $("#zoneModifyFeature"),
        "cmdDrawModeOff": $("#cmdDrawModeOff")
    };

    $("#zoneDrawFeature,#zoneModifyFeature,#cmdDrawModeOff").click(function () {
        toggleControl(this);
    });
}

function toggleControl(element) {
    for (key in drawControls) {
        var control = drawControls[key];
        if (element.id == key && element.checked) {
            control.activate();
        } else {
            control.deactivate();
        }
    }
}

function toggleButtons(button) {
    for (key in drawButtons) {
        var i_button = drawButtons[key];
        if (button[0].id == key) {
            i_button.attr('checked', true);
        } else {
            i_button.attr('checked', false);
        }
    }
    button[0].click();
    $("#repeatButton").buttonset();
}

// detect polygon added events
function newZoneAdded(evt) {
    showSavingDialog(evt);
}

// detect polygon afteredited events
function zoneEdited(evt) {
    showSavingDialog(evt);
}

// detect polyline added events
function newPolyLAdded(evt) {
    showPolylineSavingDialog(evt);
}

function polylineEdited(evt) {
    showPolylineSavingDialog(evt);
}

function showSavingDialog(evt) {
    zoneToSave = evt;
    //drawing zone validation
    if (evt.feature.geometry.getVertices().length < 3) {
        alert('Tambahkan point untuk zona yang akan anda gambar.');
        RequestZone();
    }
    else {
        if (zoneToSave.feature.attributes.Name == null) {//insert
            $("#chkZonaVisible").prop('checked',true);
        }
        else {//update
            $("#chkZonaVisible").prop('checked', zoneToSave.feature.attributes.Visible == "true");
        }
        $("#txtNameZona").val(zoneToSave.feature.attributes.Name);
        $("#chkZonaWatch").prop('checked', zoneToSave.feature.attributes.PierWatch == "true");
        $("#chkZonaMasuk").prop('checked', zoneToSave.feature.attributes.CheckMasuk == "true");
        $("#chkZonaKeluar").prop('checked', zoneToSave.feature.attributes.CheckKeluar == "true");
        
        if (zoneToSave.feature.attributes.Colour != null) {
            $("#colorpicker-popup").val(zoneToSave.feature.attributes.Colour.replace('#', ''));
        } else {
            $("#colorpicker-popup").val('');
        }
        $('#savingDialog').dialog('open');
    }

    return false;
}
$("#chkZonaWatch").change(function () {
    $("#chkZonaMasuk").prop('disabled', !$("#chkZonaWatch").is(':checked'));
    $("#chkZonaKeluar").prop('disabled', !$("#chkZonaWatch").is(':checked'));
});

function showPolylineSavingDialog(evt) {
    lineToSave = evt;
    $("#txtDescLine").val(lineToSave.feature.attributes.Description);
    $('#savingLineDialog').dialog('open');
    return false;
}

function showZoneSavingDialog(evt) {
    lineToSave = evt;
    $("#txtDescLine").val(lineToSave.feature.attributes.Description);
    $('#savingLineDialog').dialog('open');
    return false;
}

var zoneToSave;
function SavingZone() {
    zoneName = $("#txtNameZona").val();
    if (zoneName == "") {
        alert("Silahkan isi Nama Zona terlebih dulu.");
        return false;
    }

    if (zoneToSave.feature.attributes.Name == null) {
        //insert
        InsertZone(zoneName, zoneToSave.feature.geometry.toString().replace('POLYGON((', '').replace('))', ''), $('#chkZonaWatch').is(':checked'), $('#chkZonaMasuk').is(':checked'), $('#chkZonaKeluar').is(':checked'), $('#chkZonaVisible').is(':checked'), $('#colorpicker-popup').val());
    }
    else {
        //update
        UpdateZone(zoneToSave.feature.attributes.ID, zoneName, zoneToSave.feature.geometry.toString().replace('MULTIPOLYGON(((', '').replace(')))', ''), $('#chkZonaWatch').is(':checked'), $('#chkZonaMasuk').is(':checked'), $('#chkZonaKeluar').is(':checked'), $('#chkZonaVisible').is(':checked'), $('#colorpicker-popup').val());
    }
    return true;
}

function isSaveBeforeCloseSaving() {
    if (zoneToSave.feature.attributes.Name == null && $('#zoneDrawFeature').is(":checked")) {
        return false;
    }
    return true;
}

function isSaveBeforeCloseSavingLine() {
    if (lineToSave.feature.attributes.Description == null && $('#polyLDrawFeature').is(":checked")) {
        return false;
    }
    return true;
}

function initMapGeoJson() {
    initTiled();

    // allow testing of specific renderers via "?renderer=Canvas", etc
    renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
    renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

    var templateStyleZone = {
        fillColor: "${Colour}",
        strokeColor: "#39BEBE",
        fillOpacity: 0.9,
        strokeOpacity: 1,
        pointRadius : "10"
    }
    var styleZone = new OpenLayers.Style(templateStyleZone);
    var templateZone = {
        styleMap: new OpenLayers.StyleMap(styleZone),
        isBaseLayer: false,
        rendererOptions: { yOrdering: true },
        renderers: renderer
    };

    var templateVector = {
        isBaseLayer: false,
        rendererOptions: { yOrdering: true },
        renderers: renderer
    };

    zona_layerGeoJson = new OpenLayers.Layer.Vector("zona_layer", templateZone);
    zona_layerGeoJson.events.on({
        "afterfeaturemodified": zoneEdited
    });
    mapGeoJson.addLayer(zona_layerGeoJson);

    polyL_layerGeoJson = new OpenLayers.Layer.Vector("polyline_layer", templateVector);
    polyL_layerGeoJson.events.on({
        "afterfeaturemodified": polylineEdited
    });
    mapGeoJson.addLayer(polyL_layerGeoJson);

    // lon lat
    mapGeoJson.addControl(new OpenLayers.Control.MousePosition());

    zonaControl = new OpenLayers.Control.SelectFeature(zona_layerGeoJson);
    mapGeoJson.addControl(zonaControl);
    zonaControl.activate();

    lineSelectControl = new OpenLayers.Control.SelectFeature(polyL_layerGeoJson);
    mapGeoJson.addControl(lineSelectControl);
    lineSelectControl.activate();
}

function onZonaSelect(feature) {//zona diselect (*click*)
}

function initTiled() {
    var extent = new OpenLayers.Bounds(95.0596541500001, -10.99740704, 141.00718685, 5.90688428000004);
    var tempScales = [1128.49722, 2256.99444, 4513.98888, 9027.977761, 18055.95552, 36111.91104, 72223.82209, 144447.6442, 288895.2884, 577790.5767, 1155581.153, 2311162.307, 4622324.614, 9244649.227, 18489298.45, 36978596.91, 73957193.82, 147914387.6, 295828775.3, 591657550.5]

    var mapOptions = {
        maxExtent: extent,
        scales: tempScales
    };
    mapGeoJson = new OpenLayers.Map('mapGeoJson', mapOptions);

    var params = {
        mapdefinition: 'Library://Jatim/MapsTiled/JatimTiled3.MapDefinition',
        basemaplayergroupname: "Basemap"
    }
    var options = {
        singleTile: false
    };
    var layer = new OpenLayers.Layer.MapGuide("MapGuide OS tiled layer", url, params, options);
    mapGeoJson.addLayer(layer);

    mapGeoJson.setCenter(new OpenLayers.LonLat(lon, lat), zoom);
}

function DeleteZone(zoneID) {
    var r = confirm("Hapus zone ini?")
    if (r == false) {
        return false;
    }
    try {
        $.ajax({
            type: "POST",
            url: 'DrawingZone.aspx/DeleteZone',
            data: JSON.stringify({ id: zoneID }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                RequestZone();
            }
        });
    } catch (e) {
        alert(e.Data);
    }
}

function RequestZone() {
    try {
        $.ajax({
            type: "POST",
            url: 'FullDesignA.aspx/GetZone',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                DrawZona(data.d);
            }
        });
    } catch (e) {
        alert(e.Data);
    }
}

function InsertZone(name, geom, watch, masuk, keluar,visible,colour) {
    try {
        $.ajax({
            type: "POST",
            url: 'DrawingZone.aspx/InsertZone',
            data: JSON.stringify({
                namaZone: name,
                geom: geom,
                pierWatch: watch,
                pierMasuk: masuk,
                pierKeluar: keluar,
                visible: visible,
                colour : colour
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                InsertZoneResult(data.d);
            }
        });
    } catch (e) {
        alert(e.Data);
    }
}

function UpdateZone(id, name, geom, watch, masuk, keluar,visible, colour) {
    try {
        $.ajax({
            type: "POST",
            url: 'DrawingZone.aspx/UpdateZone',
            data: JSON.stringify({
                id: id, namaZone: name,
                geom: geom,
                pierWatch: watch,
                pierMasuk: masuk,
                pierKeluar: keluar,
                visible: visible,
                colour : colour
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                UpdateZoneResult(data.d);
            }
        });
    } catch (e) {
        alert(e.Data);
    }
}

function InsertZoneResult(data) {
    result = data == "true" ? "Sukses" : "Gagal";
    alert("Penyimpanan Zona " + result);
    zoneToSave.feature.attributes.Name = jQuery.parseJSON("{\"Name\":\"" + $('#txtNameZona').val() + "\"}");
    RequestZone();
    toggleButtons($("#cmdDrawModeOff"));
    $('#savingDialog').dialog('close');
}

function UpdateZoneResult(data) {
    result = data == "true" ? "Sukses" : "Gagal";
    alert("Penyimpanan Zona " + result);
    RequestZone();
    toggleButtons($("#cmdDrawModeOff"));
    $('#savingDialog').dialog('close');
}

function DrawZona(data) {
    var geojson_format = new OpenLayers.Format.GeoJSON();
    var realData = jQuery.parseJSON(data);
    var realDataLength = realData.length;

    //draw tabel daftar zona
    var textHTML;
    // add header
    textHTML = '<div class="ab-data-table-search">';
    textHTML += '<div class="baris-title">';
    textHTML += '<div class="kolom-title kolom-mmsi">ID</div>';
    textHTML += '<div class="kolom-title kolom-name">Nama Zona</div>';
    textHTML += '<div style="clear:both;"></div>';
    textHTML += '</div>';

    if (zona_layerGeoJson.features.length > 0) {
        zona_layerGeoJson.removeFeatures(zona_layerGeoJson.features);
    }

    for (var i = 0; i < realDataLength; i++) {
        var itemRealData = realData[i];
        var polyZone = geojson_format.read(itemRealData.Geom);
        if (itemRealData.Colour == null) itemRealData.Colour = '#FFF37A';
        var attributes = jQuery.parseJSON("{\"ID\":" + itemRealData.ID + ",\"Name\":\"" + itemRealData.Name + "\",\"PierWatch\":\"" + itemRealData.PierWatch + "\",\"CheckMasuk\":\"" + itemRealData.CheckMasuk + "\",\"CheckKeluar\":\"" + itemRealData.CheckKeluar + "\",\"Visible\":\"" + itemRealData.Visible + "\",\"Colour\":\"" + itemRealData.Colour + "\"}");
        polyZone[0].attributes = attributes;
        zona_layerGeoJson.addFeatures(polyZone);
        var centroidPoint = polyZone[0].geometry.getCentroid();

        textHTML += '<div class="baris-isi">';
        textHTML += '<div class="kolom-mmsi terang-zone">' + itemRealData.ID + '</div>';
        textHTML += '<div class="kolom-name terang-zone" onclick="mapGeoJson.moveTo(new OpenLayers.LonLat('
                    + centroidPoint.x + ',' + centroidPoint.y + '),15,null); SelectZonaID(' + itemRealData.ID + ');"  style="cursor:pointer;"> ' + itemRealData.Name + '</div>';
        textHTML += '<div class="kolom-edit terang-zone yellow" style="cursor:pointer;padding:2px;width:16px;height:15px;float:left;position:relative;" onclick="EntryZone(' + itemRealData.ID + ')"><img src="theme/default/img/draw_point_off.png" width=12px height=12px;></div>';
        textHTML += '<div class="kolom-delete terang-zone red" style="cursor:pointer;" onclick="DeleteZone(' + itemRealData.ID + ')">X</div>';
        textHTML += '<div style="clear:both;"></div>';
        textHTML += '</div>';
    }

    $("#table-zona").html(textHTML);
}

function SelectZonaID(id) {
    var feat = zona_layerGeoJson.getFeaturesByAttribute("ID", id);
    zonaControl.unselectAll();
    zonaControl.select(feat[0]);
}