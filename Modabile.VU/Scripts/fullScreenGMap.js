var zoom = 12;
var mapGeoJson, layerGeoJson, vector_layerGeoJson, layerStar, layerNavStatus, renderer;
//start trail
var trail_layerGeoJson, trailBanks = {};
var trailLength = 10; //panjang trail
var trailTime = 60; //kapal diam selama 60 detik akan kehilangan trail
var trailColour = "#FF33DD"; //warna trail
//end trail
var featuresShipOnDisplay;
var epsg4326 = new OpenLayers.Projection('EPSG:4326');
var epsg900913 = new OpenLayers.Projection('EPSG:900913');

var selectedFeatShip;
var SHADOW_Z_INDEX = 10;
var MARKER_Z_INDEX = 11;

var detailClosed = true;

$(document).ready(function () {
    initMapGeoJson();

    var refreshId = setInterval(function () {
        RequestAjaxShips();
    }, 5000);

    $('.mn-start,.mn-stop').append('<span class="hover"></span>').each(function () {
        var $span = $('> span.hover', this).css('opacity', 0);
        $(this).hover(function () {
            $span.stop().fadeTo(500, 1);
        }, function () {
            $span.stop().fadeTo(500, 0);
        });
    });

    $("#footer2").html("Copyright &copy; " + (new Date).getFullYear() + ". All Rights Reserved.")

    $("#reqCari").keyup(function () {
        WriteSearchShipResult(this.value, featuresShipOnDisplay);
    });

    uncheckedTools();
    $.cookie.json = true;
    $.cookie.expires = 7;
});

function RequestAjaxShips() {
    try {
        $.ajax({ type: "POST",
            url: 'FullDesignA.aspx/GetShipsAndUpdatedToClient',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                updateMapWithGeoJsonColl(data.d);
            }
        });
    } catch (e) {
        alert(e.Data);
    }
}

function uncheckedTools() {
    $("#CBrad,#CBline,#CBloa").removeAttr('checked');
}


function FileNameNavStatus(feature) {
    var knot = feature.attributes.SOG / 10;
    var navstatus = feature.attributes.NavStatus;
    if ((navstatus == 0 || navstatus == 8) && (knot > 0)) { return "_titik"; }
    return "";
}

//tiled version
function initMap() {
    mapGeoJson = new OpenLayers.Map('mapGeoJson', {
        projection: epsg900913,
        displayProjection: epsg4326
    });

}

function initGoogleLayers() {    
    var gmap = new OpenLayers.Layer.Google(
        "Google Streets", // the default
        {numZoomLevels: 20}
    );

    var gmapHyb = new OpenLayers.Layer.Google("Google Hybrid",
        { type: google.maps.MapTypeId.HYBRID, numZoomLevels: 20 }
    );

    mapGeoJson.addLayers([gmap, gmapHyb]);

}    

function initMapGeoJson() {
    initMap();
    initGoogleLayers();
    
    // allow testing of specific renderers via "?renderer=Canvas", etc
    renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
    renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

    mapGeoJson.setCenter(new OpenLayers.LonLat(lon, lat).transform(epsg4326,epsg900913), zoom);

    var context = {
        getShipStyle: function (ft) {
            //update x,y selected ships
            DrawSelectedShipStar(ft);
            //DrawShipNavStatus(ft); 
            if (isMmsiGedung(ft.attributes.MMSI)) {
                return "Styles/house.png";
            }
            if (isMmsiTundaIPC(ft.attributes.MMSI)) {
                return "Styles/tunda_ipc" + FileNameNavStatus(ft) + ".png";
            }
            if (ft.attributes.ShipTypeDescription != null) {
                if (ft.attributes.ShipTypeDescription.indexOf("Cargo") != -1) {
                    return "Styles/kargo" + FileNameNavStatus(ft) + ".png";
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Passenger") != -1) {
                    return "Styles/penumpang" + FileNameNavStatus(ft) + ".png";
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Tanker") != -1) {
                    return "Styles/tanker" + FileNameNavStatus(ft) + ".png";
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Tug") != -1) {
                    return "Styles/tunda" + FileNameNavStatus(ft) + ".png";
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Search") != -1) {
                    return "Styles/SAR" + FileNameNavStatus(ft) + ".png";
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Container") != -1) {
                    return "Styles/kargo" + FileNameNavStatus(ft) + ".png";
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Pleasure") != -1) {
                    return "Styles/pesiar" + FileNameNavStatus(ft) + ".png";
                }
            }
            return "Styles/tidak-terdaftar" + FileNameNavStatus(ft) + ".png";
        },
        getShipWidth: function (ft) {
            if (isMmsiGedung(ft.attributes.MMSI)) {
                return 36;
            }
            defaultWidth = 10;
            return defaultWidth;
                if ($("#CBloa").attr('checked') == null) return defaultWidth;
            if ((ft.attributes.ToPort == null && ft.attributes.ToStarBoard == null) ||
                (ft.attributes.ToPort == '' && ft.attributes.ToStarBoard == '')) return defaultWidth;
            wide = (parseInt(ft.attributes.ToPort) + parseInt(ft.attributes.ToStarBoard));
            //AAV:countHeight
            var boun = mapGeoJson.calculateBounds();
            var distLeft = OpenLayers.Util.distVincenty(new OpenLayers.LonLat(boun.left, boun.top), new OpenLayers.LonLat(boun.right, boun.top)) * 1000; //result in km, so we change it to m to make it easier to count
            var width = 36; //image width
            return prevWithLoa ? mapGeoJson.size.w * wide / distLeft : width;
         
        },
        getShipHeight: function (ft) {
            if (isMmsiGedung(ft.attributes.MMSI)) {
                return 36;
            }
            defaultHeight = 26;
            if ($("#CBloa").attr('checked') == null) return defaultHeight;
            if ((ft.attributes.ToBow == null && ft.attributes.ToStern == null) ||
             (ft.attributes.ToBow == '' && ft.attributes.ToStern == '')) return defaultHeight;
            loa = (parseInt(ft.attributes.ToBow) + parseInt(ft.attributes.ToStern));
            //AAV:countHeight
            var boun = mapGeoJson.calculateBounds();
            var distLeft = OpenLayers.Util.distVincenty(new OpenLayers.LonLat(boun.left, boun.top), new OpenLayers.LonLat(boun.left, boun.bottom)) * 1000; //result in km, so we change it to m to make it easier to count
            var height = 40; //image height
            return prevWithLoa ? mapGeoJson.size.h * loa / distLeft : height;
        },
        getShipVisibility: function (ft) {
            if (ft.attributes.ShipTypeDescription != null) {
                if (ft.attributes.ShipTypeDescription.indexOf("Cargo") != -1) {
                    return $("#cbShowKargo").is(":checked") && GrtViewable(ft) ? true : 'none';
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Passenger") != -1) {
                    return $("#cbShowPenumpang").is(":checked") && GrtViewable(ft) ? true : 'none';
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Tanker") != -1) {
                    return $("#cbShowTanker").is(":checked") && GrtViewable(ft) ? true : 'none';
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Tug") != -1) {
                    return $("#cbShowTunda").is(":checked") && GrtViewable(ft) ? true : 'none';
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Search") != -1) {
                    return $("#cbShowSar").is(":checked") && GrtViewable(ft) ? true : 'none';
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Container") != -1) {
                    return $("#cbShowPetikemas").is(":checked") && GrtViewable(ft) ? true : 'none';
                }
                else if (ft.attributes.ShipTypeDescription.indexOf("Pleasure") != -1) {
                    return $("#cbShowPesiar").is(":checked") && GrtViewable(ft) ? true : 'none';
                }
                else {
                    return $("#cbShowNoReg").is(":checked") && GrtViewable(ft) ? true : 'none';
                }
            }
            else {
                return $("#cbShowNoReg").is(":checked") && GrtViewable(ft) ? true : 'none';
            }
         },
        getShipLabel: function (ft) {
            if (isMmsiGedung(ft.attributes.MMSI)) {
                return "AIS Base";
            }
            var knot = (ft.attributes.SOG == undefined) ? 0 : (ft.attributes.SOG / 10).toFixed(2);
            return ft.attributes.ShipName == null ? ft.attributes.MMSI + "\n" + knot + " k" : ft.attributes.ShipName + "\n" + knot + " k";
        }
    };


    var templateShip = {
        externalGraphic: "${getShipStyle}", // using context.getSize(feature)
        // Makes sure the background graphic is placed correctly relative
        // to the external graphic.
        backgroundXOffset: 0,
        backgroundYOffset: -7,
        //AAV:set size
        graphicWidth: "${getShipWidth}",
        graphicHeight: "${getShipHeight}",

        //show label and rotate image
        label: "${getShipLabel}",
        labelYOffset: 15,
        rotation: "${HDG}",
        fontColor: "#595959",
        fontSize: "12px",
        labelAlign : "l",
        display: "${getShipVisibility}",

        // Set the z-indexes of both graphics to make sure the background
        // graphics stay in the background (shadows on top of markers looks
        // odd; let's not do that).
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX,
        pointRadius: 10
    };

    var contextNavStatus = {
        getIcon: function (ft) {
            var navstatus = ft.attributes.NavStatus;
            if (navstatus == 7) { return "Styles/ikan.png"; }
            if (navstatus == 5) { return "Styles/tambat.png"; }
            if (navstatus == 1) { return "Styles/jangkar.png"; }
            return "Styles/empty.png";
        }
    }
    var templateNavStatus = {
        styleMap: new OpenLayers.Style({
            externalGraphic: "${getIcon}",
            graphicWidth: 16,
            graphicHeight: 36,
            labelYOffset: 15,
            pointRadius: 0
        }, { context: contextNavStatus }),
		projection: mapGeoJson.displayProjection,
        preFeatureInsert: function (feature) {
            feature.geometry.transform(epsg4326, epsg900913);
        },
        isBaseLayer: false,
        rendererOptions: { yOrdering: true },
        renderers: renderer,
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX
    }
    layerNavStatus = new OpenLayers.Layer.Vector("navstatus_layer", templateNavStatus);
    mapGeoJson.addLayer(layerNavStatus);
    
    var layerListeners = {
        featureclick: function (e)
        {
            window.open(urlwind, '_blank');
            return false;
        },
        nofeatureclick: function (e)
        {
        }
    };
    
    var styleShip = new OpenLayers.Style(templateShip, { context: context });
    vector_layerGeoJson = new OpenLayers.Layer.Vector(
                "ship_layer",
                {
                    styleMap: new OpenLayers.StyleMap(styleShip),
                    isBaseLayer: false,
                    projection: mapGeoJson.displayProjection,
                     preFeatureInsert: function (feature) {
                       feature.geometry.transform(epsg4326,epsg900913);
                       },
                    rendererOptions: { yOrdering: true },
                    renderers: renderer
                });
    mapGeoJson.addLayer(vector_layerGeoJson);

var styleStar = new OpenLayers.StyleMap({
        "default": {
            graphicName: "circle",
            pointRadius: 20,
            strokeColor: "red",
            strokeWidth: 2,
            fillColor: "white",
            fillOpacity: 0.6,
            strokeDashstyle: 'dot'
        },
        "select": {
            pointRadius: 20,
            fillOpacity: 1,
            rotation: 45
        }
    });
    layerStar = new OpenLayers.Layer.Vector("SelectedShip", {
        styleMap: styleStar,
        isBaseLayer: false,
        projection: mapGeoJson.displayProjection,
        preFeatureInsert: function (feature) {
            feature.geometry.transform(epsg4326, epsg900913);
        },
        renderers: renderer
    });   
    mapGeoJson.addLayer(layerStar);

    //start trail
    var templateTrail = {
        styleMap: new OpenLayers.Style({
            fillColor: trailColour,
            strokeColor: trailColour,
            fillOpacity: 0.5,
            strokeOpacity: 0.8,
            pointRadius: 2
        }),
        isBaseLayer: false,
        rendererOptions: { yOrdering: true },
        renderers: renderer
    };
    trail_layerGeoJson = new OpenLayers.Layer.Vector(
                "trail_layer", templateTrail);
    mapGeoJson.addLayer(trail_layerGeoJson);
    //end trail

    selectGeoJsonFeat = new OpenLayers.Control.SelectFeature(vector_layerGeoJson, { onSelect: onFeatureGJSelect }); //this one should allow us to handle clicks
    mapGeoJson.addControl(selectGeoJsonFeat);
    selectGeoJsonFeat.activate();

    mapGeoJson.addControl(new OpenLayers.Control.LayerSwitcher());

	clickControl = new OpenLayers.Control.Click();
    mapGeoJson.addControl(clickControl);

    mapGeoJson.events.register("mousemove", mapGeoJson, MouseMove);
    // lon lat
    mapGeoJson.addControl(new OpenLayers.Control.MousePosition());
}

function GrtViewable(feature) {
    if (feature.attributes.GrtBySiuk != null) {
        if (feature.attributes.GrtBySiuk <= 5000 && $("#cbGrtLTE5000").is(":checked")) { return true; }
        if (feature.attributes.GrtBySiuk > 5000 && $("#cbGrtGT5000").is(":checked")) { return true; }
        return false;
    }
    return true;
}

function GrtCount(feature) {
    if (feature.attributes.GrtBySiuk != null) {
        if (feature.attributes.GrtBySiuk <= 5000) {
            $('#cfdsGrtLTE5000').html(parseInt($('#cfdsGrtLTE5000').html()) + 1);
        }
        else {
            $('#cfdsGrtGT5000').html(parseInt($('#cfdsGrtGT5000').html()) + 1);
        }        
    }    
}


function updateMapWithGeoJsonColl(featColl, countDetectedShips) {   
    vector_layerGeoJson.removeFeatures(vector_layerGeoJson.features);
    var geojson_format = new OpenLayers.Format.GeoJSON();
    featuresShipOnDisplay = geojson_format.read(featColl);
    vector_layerGeoJson.addFeatures(featuresShipOnDisplay);   
    $("#jumlahKapal").html("<div id='jumlahKapal' class='small-font'>Ada " + vector_layerGeoJson.features.length + " kapal terdeteksi</div>");
    WriteAllDetectedShips(featuresShipOnDisplay);
    DrawSelectedShipStar(selectedFeatShip);
    RedrawTrail(featuresShipOnDisplay);
}

function RedrawTrail(featuresShipOnDisplay) {
    for (var i = 0; i < featuresShipOnDisplay.length; i++) {
        feature = featuresShipOnDisplay[i];
        if (trailBanks[feature.attributes.MMSI] == undefined) {
            trailBanks[feature.attributes.MMSI] = [[new Date(), feature.geometry.x, feature.geometry.y]];
        } else {
            masterBank = trailBanks[feature.attributes.MMSI];
            if (feature.geometry.x == masterBank[masterBank.length - 1][1] &&
                feature.geometry.y == masterBank[masterBank.length - 1][2]) {
                if (new Date() - masterBank[masterBank.length - 1][0] > trailTime * 1000) {
                    //did not move over a minute
                    trailBanks[feature.attributes.MMSI].splice(0, masterBank.length - 1);
                }
            } else {
                if (feature.geometry.x == 0 && feature.geometry.y == 0) continue;
                trailBanks[feature.attributes.MMSI].push([new Date(), feature.geometry.x, feature.geometry.y]);
                //check trail length
                if (trailBanks[feature.attributes.MMSI].length > trailLength) {
                    trailBanks[feature.attributes.MMSI].splice(0, 1);
                }
            }
        }
    }
    trail_layerGeoJson.removeFeatures(trail_layerGeoJson.features);
    $.each(trailBanks, function (i, e) {
        for (var ie = 1; ie < e.length; ie++) {
            var line = new OpenLayers.Geometry.LineString([new OpenLayers.Geometry.Point(e[ie - 1][1], e[ie - 1][2]), new OpenLayers.Geometry.Point(e[ie][1], e[ie][2])]);
            trail_layerGeoJson.addFeatures(new OpenLayers.Feature.Vector(line));
        }
    });
}

function WriteAllDetectedShips(AllShipsOnDisplay) {
    $("#table-all-ships").html(ListToTableAllShips(AllShipsOnDisplay)); 
}

$('.check-filter-data-table').click(function(){
    $("#table-all-ships").html(ListToTableAllShips(vector_layerGeoJson.features));
});

function FeatCanListToTableAllShips(cbDicek, grtDlmRange) {
    if (!(cbDicek && grtDlmRange)) {
        return false;
    }
    return true;
}

function ListToTableAllShips(AllShipsOnDisplay) {
    if (AllShipsOnDisplay == null) return "";

    var feature;
    var shipName = "";
    var textHTML = "";

    $('.span-ship-count').html('0');

    var tipe = null;
    var kplIniDitunda = false;    for (var i = 0; i < AllShipsOnDisplay.length; i++) {
        feature = AllShipsOnDisplay[i];
		GrtCount(feature);
        tipe = null;
        if (feature.attributes.ShipTypeDescription != null) {
            if (feature.attributes.ShipTypeDescription.indexOf("Cargo") != -1) {
                tipe = "Kargo";
                $('#cfds1').html(parseInt($('#cfds1').html()) + 1);
                if (!FeatCanListToTableAllShips($("#cbShowKargo").is(":checked"),GrtViewable(feature))) continue;
            }
            else if (feature.attributes.ShipTypeDescription.indexOf("Passenger") != -1) {
                tipe = "Penumpang";
                $('#cfds2').html(parseInt($('#cfds2').html()) + 1);
                if (!FeatCanListToTableAllShips($("#cbShowPenumpang").is(":checked"), GrtViewable(feature))) continue;
            }
            else if (feature.attributes.ShipTypeDescription.indexOf("Tanker") != -1) {
                tipe = "Tanker";
                $('#cfds4').html(parseInt($('#cfds4').html()) + 1);
                if (!FeatCanListToTableAllShips($("#cbShowTanker").is(":checked"), GrtViewable(feature))) continue;
            }
            else if (feature.attributes.ShipTypeDescription.indexOf("Tug") != -1) {
                tipe = "Tunda";
                $('#cfds8').html(parseInt($('#cfds8').html()) + 1);
                if (!FeatCanListToTableAllShips($("#cbShowTunda").is(":checked"), GrtViewable(feature))) continue;
            }
            else if (feature.attributes.ShipTypeDescription.indexOf("Search") != -1) {
                tipe = "SAR";
                $('#cfds16').html(parseInt($('#cfds16').html()) + 1);
                if (!FeatCanListToTableAllShips( $("#cbShowSar").is(":checked"), GrtViewable(feature))) continue;
            }
            else if (feature.attributes.ShipTypeDescription.indexOf("Container") != -1) {
                tipe = "Petikemas";
                $('#cfds32').html(parseInt($('#cfds32').html()) + 1);
                if (!FeatCanListToTableAllShips( $("#cbShowPetikemas").is(":checked"), GrtViewable(feature))) continue;
            }
            else if (feature.attributes.ShipTypeDescription.indexOf("Pleasure") != -1) {
                tipe = "Pesiar";
                $('#cfds64').html(parseInt($('#cfds64').html()) + 1);
                if (!FeatCanListToTableAllShips( $("#cbShowPesiar").is(":checked"), GrtViewable(feature))) continue;
            }
            else {
                tipe = "Tidak Terdaftar";
                $('#cfds128').html(parseInt($('#cfds128').html()) + 1);
                if (!FeatCanListToTableAllShips( $("#cbShowNoReg").is(":checked"), GrtViewable(feature))) continue;
            }
        } else {
            tipe = "Tidak Terdaftar";
            $('#cfds128').html(parseInt($('#cfds128').html()) + 1);
            if (!$("#cbShowNoReg").is(":checked")) continue;
        }

        if (feature.attributes.ShipName == null) { shipName = "-"; }
        else { shipName = feature.attributes.ShipName; }
        var xyPos = feature.geometry != null ? ' onclick="mapGeoJson.moveTo(new OpenLayers.LonLat(' + feature.geometry.x + ',' + feature.geometry.y + '),16,null);"' : '';
        var gelapTerang = (i % 2 == 0) ? 'gelap' : 'terang';
        textHTML += '<div class="baris-isi" ' + xyPos + ' onmouseover="FindFeatureAndSelect(' + feature.attributes.MMSI + ');" style="cursor:pointer;">';
        textHTML += '<div class="kolom-mmsi ' + gelapTerang + '">' + feature.attributes.MMSI + '</div>';
        textHTML += '<div class="kolom-name ' + gelapTerang + '">' + shipName + ' [' + feature.attributes.GrtBySiuk + ']</div>';
        textHTML += '<div class="kolom-tipe ' + gelapTerang + '">' + tipe + '</div>';
        textHTML += '<div style="clear:both;"></div>';
        textHTML += '</div>';
    }
    return textHTML;
}

function WriteSearchShipResult(textToSearch,AllShipsOnDisplay) {
    var textHTMLSearch = '';
    textHTMLSearch = FindFeaturesShip(textToSearch, AllShipsOnDisplay);
    $("#table-search-result").html(textHTMLSearch);
}

function CreateHTMLSearchAISShipResult(jmlKapalterpilih, xyPos, feature, find_ais) {
    var textHTMLCreate = '';
    //write to table
    if (jmlKapalterpilih == 0) {
        // add header
        textHTMLCreate = '<div class="ab-data-table-search">';
        textHTMLCreate += '<div class="baris-title">';
        textHTMLCreate += '<div class="kolom-title kolom-mmsi">MMSI</div>';
        textHTMLCreate += find_ais == true ? '<div class="kolom-title kolom-name">Nama Kapal</div>' : '<div class="kolom-title kolom-name">Nama Kapal NonAIS</div>';
        textHTMLCreate += '<div style="clear:both;"></div>';
        textHTMLCreate += '</div>';
    }

    if (jmlKapalterpilih % 2 == 0) {
        textHTMLCreate += find_ais == true ? '<div class="baris-isi" ' + xyPos + ' onmouseover="FindFeatureAndSelect(' + feature.attributes.MMSI + ');" style="cursor:pointer;">' :
           '<div class="baris-isi" ' + xyPos + ' onmouseover="FindFeatureAndSelectNonAIS(' + feature.attributes.MMSI + ');" style="cursor:pointer;">';
        textHTMLCreate += '<div class="kolom-mmsi gelap">' + feature.attributes.MMSI + '</div>';
        textHTMLCreate += '<div class="kolom-name gelap">' + feature.attributes.ShipName + '</div>';
        textHTMLCreate += '<div style="clear:both;"></div>';
        textHTMLCreate += '</div>';
    }
    else {
        textHTMLCreate += find_ais ? '<div class="baris-isi" ' + xyPos + ' onmouseover="FindFeatureAndSelect(' + feature.attributes.MMSI + ');" style="cursor:pointer;">' :
           '<div class="baris-isi" ' + xyPos + ' onmouseover="FindFeatureAndSelectNonAIS(' + feature.attributes.MMSI + ');" style="cursor:pointer;">';
        textHTMLCreate += '<div class="kolom-mmsi terang">' + feature.attributes.MMSI + '</div>';
        textHTMLCreate += '<div class="kolom-name terang">' + feature.attributes.ShipName + '</div>';
        textHTMLCreate += '<div style="clear:both;"></div>';
        textHTMLCreate += '</div>';
    }
    return textHTMLCreate;


}

function FindFeaturesShip(textToSearch,AllShipsOnDisplay) {
    if (textToSearch == "" || textToSearch.length == 1) return "";
    if (AllShipsOnDisplay == null) return "";

    var feature;
    var jmlKapalterpilih = 0;
	var textHTMLAIS = '';
    var xyPos;
for (var i = 0; i < AllShipsOnDisplay.length; i++) {
        feature = AllShipsOnDisplay[i];
        if (feature.attributes.MMSI == null) continue;
        xyPos = feature.geometry != null ? ' onclick="mapGeoJson.moveTo(new OpenLayers.LonLat(' + feature.geometry.x + ',' + feature.geometry.y + '),16,null);"' : '';
        if (feature.attributes.MMSI.toString().indexOf(textToSearch) != -1) {
            textHTMLAIS += CreateHTMLSearchAISShipResult(jmlKapalterpilih, xyPos,feature,true);            
            jmlKapalterpilih++;
        }
    }

    if (jmlKapalterpilih > 0) {
        textHTMLAIS += '</div>';
        return textHTMLAIS;
    }
    else {
        for (var i = 0; i < AllShipsOnDisplay.length; i++) {
            feature = AllShipsOnDisplay[i];
            if (feature.attributes.ShipName == null) continue;
            xyPos = feature.geometry != null ? ' onclick="mapGeoJson.moveTo(new OpenLayers.LonLat(' + feature.geometry.x + ',' + feature.geometry.y + '),16,null);"' : '';
            if (feature.attributes.ShipName.toString().indexOf(textToSearch.toUpperCase()) != -1) {
                textHTMLAIS += CreateHTMLSearchAISShipResult(jmlKapalterpilih, xyPos, feature, true);            
                jmlKapalterpilih++;
            }
        }
    }

    if (jmlKapalterpilih > 0) {
        textHTMLAIS += '</div>';
        return textHTMLAIS;
    }
    else {
        for (var i = 0; i < AllShipsOnDisplay.length; i++) {
            feature = AllShipsOnDisplay[i];
            if (feature.attributes.CallSign == null) continue;
            xyPos = feature.geometry != null ? ' onclick="mapGeoJson.moveTo(new OpenLayers.LonLat(' + feature.geometry.x + ',' + feature.geometry.y + '),16,null);"' : '';
            if (feature.attributes.CallSign.toString().replace(/ /g, '').toUpperCase().indexOf(textToSearch.replace(/ /g, '').toUpperCase()) != -1) {
                textHTMLAIS += CreateHTMLSearchAISShipResult(jmlKapalterpilih, xyPos, feature, true);            
                jmlKapalterpilih++;
            }
        }
    }


    if (jmlKapalterpilih > 0) {
        textHTMLAIS += '</div>';
        return textHTMLAIS;
    }
	else { return ""; }
}

function closeButton_clicked() {
    detailClosed = true;
    toggleDetail();
}

function FindFeatureAndSelect(mmsi) {
    feat = vector_layerGeoJson.getFeaturesByAttribute("MMSI", mmsi);
    onFeatureGJSelect(feat[0]);
}

function FindFeatureAndSelectNonAIS(mmsi) {
    feat = nonAIS_layerGeoJson.getFeaturesByAttribute("MMSI", mmsi);
    onFeatureGJSelect(feat[0]);
}

function DrawSelectedShipStar(feature) {
    if (selectedFeatShip != null) {
        if (feature.attributes.MMSI == selectedFeatShip.attributes.MMSI) {
            selectedFeatShip.geometry.x = feature.geometry.x;
            selectedFeatShip.geometry.y = feature.geometry.y;

            //create selected star
            layerStar.removeFeatures(layerStar.features);
            var vectorStar = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(feature.geometry.x, feature.geometry.y), {
                type: 'circle'
            });
            layerStar.addFeatures(vectorStar);
        }        
    }    
}

function DrawShipNavStatus(feature) {
    var vectorNavStatus = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(feature.geometry.x, feature.geometry.y));
    vectorNavStatus.attributes.NavStatus = feature.attributes.NavStatus;
    layerNavStatus.addFeatures(vectorNavStatus);
    mapGeoJson.raiseLayer(layerNavStatus, mapGeoJson.layers.length-1);
}

function onFeatureGJSelect(feature) {    
    selectedFeatShip = feature;
    DrawSelectedShipStar(selectedFeatShip);

     //create html 
    var text = '';
    text += '<div id="closeButton" onclick="closeButton_clicked();return false;" style="cursor:pointer;">X</div><table id="tbl-info" cellpadding="2" cellspacing="1">';
    text += '<tr style="position:relative;z-index:1;"><td colspan="3" width=190>' +
    '</td></tr><tr><td></td><td></td><td></td></tr>';
    text += '<tr class="row-judul bg-utama"><td>Name</td><td>MMSI</td><td>Callsign</td></tr>';
    text += '<tr class="row-isi bg-utama"><td>' + feature.attributes.ShipName + '</td><td>' + feature.attributes.MMSI + '</td><td><div id="callsignSel">' + feature.attributes.CallSign + '</div></td></tr>';
    text += '<tr class="row-judul bg-utama"><td>LOA</td><td>Wide</td><td>GRT (SIUK)</td></tr>';
    text += '<tr class="row-isi bg-utama"><td >' + (parseInt(feature.attributes.ToBow) + parseInt(feature.attributes.ToStern)) + ' meter</td><td>' + (parseInt(feature.attributes.ToPort) + parseInt(feature.attributes.ToStarBoard)) + '</td><td>' + feature.attributes.GrtBySiuk + '</td></tr>';
    text += '<tr class="row-judul bg-second"><td>Destination</td><td colspan="2">Type</td></tr>';
    text += '<tr class="row-isi bg-second">    <td>' + feature.attributes.Destination + '</td><td colspan="2">' + feature.attributes.ShipTypeDescription + ' meter</td></tr>';
    text += '<tr class="row-judul bg-second"><td>SOG</td><td>COG</td><td>Heading</td></tr>';
    text += '<tr class="row-isi bg-second"><td>' + (feature.attributes.SOG / 10).toFixed(2) + ' knots</td><td>' + feature.attributes.COG.replace("-", '') + '&deg;</td><td>' + feature.attributes.HDG + '&deg;</td></tr>';
    text += '<tr class="row-judul bg-second"><td colspan="2">Nav Status</td>><td colspan="1">Draught</td></tr>';
    text += '<tr class="row-isi bg-second"><td colspan="2">' + feature.attributes.NavStatusDescription + '</td><td colspan="1">' + feature.attributes.Draught + '</td></tr>';
    text += '<tr class="row-judul bg-second"><td colspan="2">Posisi</td><td>TimeStamp</td></tr>';
    text += '<tr class="row-isi bg-second"><td colspan="2">' + getLonLat(feature) + '</td><td>' + feature.attributes.TimeStampReceiver.toString().substring(6, 8) +
    '-' + feature.attributes.TimeStampReceiver.toString().substring(4, 6) +
    '-' + feature.attributes.TimeStampReceiver.toString().substring(0, 4) +
    ' ' + feature.attributes.TimeStampReceiver.toString().substring(8, 10) +
    ':' + feature.attributes.TimeStampReceiver.toString().substring(10, 12) +
    ':' + feature.attributes.TimeStampReceiver.toString().substring(12, 14) + '</td></tr>';    
    text += '</table>';

    //show div
    $("#detailKapal").html(text);
    toggleDetail();
    detailClosed = false;
}

function getLonLat(feature) {
    var transformedLonLat = new OpenLayers.LonLat(feature.geometry.x, feature.geometry.y).transform(epsg900913, epsg4326);
    var point = new GeoPoint(transformedLonLat.lon, transformedLonLat.lat);
    var LatDeg = point.getLatDeg();
    if (LatDeg.indexOf('-') > -1) {
        LatDeg = LatDeg + " S";
        LatDeg = LatDeg.replace("-", '');
    } else {
        LatDeg = LatDeg + " N";
    }
    var LonDeg = point.getLonDeg();
    if (LonDeg.indexOf('-') > -1) {
        LonDeg = LonDeg + " W";
        LonDeg = LonDeg.replace("-", '');
    } else {
        LonDeg = LonDeg + " E";
    }
    var xyPos = feature.geometry != null ? 'Lat ' + LatDeg + ' Lon ' + LonDeg : 'Lat - Lon -';
    return xyPos;
}

function ShowImageUpdate(mmsi) {
    window.open("UpdateImage.aspx?mmsi=" + mmsi, "pop", "width=250,height=50");
}

function toggleDetail() {
    if (detailClosed) $("#detailKapal").toggle('fast');
}

var centerM, circleFeat, circleRadFeat, clickControl, measureControls, selectGeoJsonFeat, distance;
function MouseMove(e) {
    var lonlat = mapGeoJson.getLonLatFromViewPortPx(e.xy).transform(epsg900913, epsg4326);
    if (centerM != undefined) {
        distance = OpenLayers.Util.distVincenty(lonlat, centerM);
        if (prevClickTool) {
            var boun = mapGeoJson.calculateBounds().transform(epsg900913, epsg4326);
            var difX = (lonlat.lat - centerM.lat);
            var difY = (lonlat.lon - centerM.lon);
            var radius = Math.sqrt((difX * difX) + (difY * difY)) * mapGeoJson.size.h / (boun.top - boun.bottom);
            var style_blue = OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']);
            style_blue.pointRadius = radius;
            style_blue.strokeWidth = 1;
            style_blue.strokeColor = "grey";
            style_blue.fillColor = "white";
            vector_layerGeoJson.removeFeatures(circleRadFeat);
            circleRadFeat = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(centerM.lon, centerM.lat), null, style_blue);
            vector_layerGeoJson.addFeatures(circleRadFeat);
        } else if (prevLineTool) {
            var line = new OpenLayers.Geometry.LineString([new OpenLayers.Geometry.Point(centerM.lon, centerM.lat), new OpenLayers.Geometry.Point(lonlat.lon, lonlat.lat)]);
            var style = { strokeColor: "grey", strokeWidth: 2 };
            vector_layerGeoJson.removeFeatures(circleRadFeat);
            circleRadFeat = new OpenLayers.Feature.Vector(line, null, style);
            vector_layerGeoJson.addFeatures(circleRadFeat);
        }
    }
}

//untuk pengukuran jarak lakukan di proyeksi 4326
function MouseClick(e) {
    if (centerM != undefined) {
        centerM = undefined;
        vector_layerGeoJson.removeFeatures(circleRadFeat);
        valueKm = Math.round(distance * 1000) / 1000;
        valueNauMile = valueKm * 0.54;
        if (circleRadFeat.geometry.CLASS_NAME == "OpenLayers.Geometry.LineString") {
            alert("That line's length is about " + valueNauMile + " NMiles/" + valueKm + " KM");
        }
        else {
            alert("That circle's radius is about " + valueNauMile + " NMiles/" + valueKm + " KM");
        }      
    } else {
        centerM = mapGeoJson.getLonLatFromViewPortPx(e.xy).transform(epsg900913, epsg4326);
    }
}

OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
    defaultHandlerOptions: {
        'single': true,
        'double': false,
        'pixelTolerance': 0,
        'stopSingle': false,
        'stopDouble': false
    },

    initialize: function (options) {
        this.handlerOptions = OpenLayers.Util.extend(
                        {}, this.defaultHandlerOptions
                    );
        OpenLayers.Control.prototype.initialize.apply(
                        this, arguments
                    );
        this.handler = new OpenLayers.Handler.Click(
                        this, {
                            'click': this.trigger
                        }, this.handlerOptions
                    );
    },

    trigger: MouseClick

});

var prevClickTool = false, prevLineTool = false, prevWithLoa = false;
function tools() {
    centerM = undefined;
    vector_layerGeoJson.removeFeatures(circleRadFeat);
    if (prevClickTool != $("#CBrad").is(":checked")) {
        prevClickTool = $("#CBrad").is(":checked");
        if (prevClickTool) {
            prevLineTool = false; 
            $("#CBline").attr('checked', false);
        }
    }
    if (prevLineTool != $("#CBline").is(":checked")) {
        prevLineTool = $("#CBline").is(":checked");
        if (prevLineTool) {
            prevClickTool = false; 
            $("#CBrad").attr('checked', false);
        }
    }
    for (controlItem in mapGeoJson.controls) {
        mapGeoJson.controls[controlItem].deactivate();
    }
    if (prevClickTool || prevLineTool) {
        clickControl.activate();
    }
    else {
        selectGeoJsonFeat.activate();
        for (controlItem in mapGeoJson.controls) {
            mapGeoJson.controls[controlItem].activate();
        }
        clickControl.deactivate();
    }
}

function loaset() {
    prevWithLoa = document.getElementById("CBloa").checked;
} 

function isMmsiGedung(mmsi) {
    for (var i = 0; i < gedungAis.length; i++) {
        if (mmsi === gedungAis[i]) {
            return true;
        }
    }
    return false;
}

function isMmsiTundaIPC(mmsi) {
    for (var i = 0; i < tundaIPC.length; i++) {
        if (mmsi === tundaIPC[i]) {
            return true;
        }
    }
    return false;
}