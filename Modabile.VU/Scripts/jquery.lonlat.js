﻿$.widget("nmk.lonlat", {
    _create: function () {
        var self = this;
        var sD = $('<span class="lonlat-sD"></span>');
        sD.append('<label for="iLatD">Latitude :</label>');
        var iLatD = $('<input id="iLatD" name="iLatD" type="text" size="12" />').appendTo(sD);
        sD.append('° ');
        sD.append(' / <label for="iLonD">Longitude :</label>');
        var iLonD = $('<input id="iLonD" name="iLonD" type="text" size="12" />').appendTo(sD);
        sD.append('° ');
        this.element.append(sD);
        var sM = $('<span class="lonlat-sM"></span>');
        sM.append('<label for="iLatAD">Latitude :</label>');
        var iLatAD = $('<input id="iLatAD" name="iLatAD" type="text" size="1" maxlength=2 />').appendTo(sM);
        sM.append('° ');
        var iLatAM = $('<input id="iLatAM" name="iLatAM" type="text" size="1" maxlength=2 />').appendTo(sM);
        sM.append('′ ');
        var iLatAS = $('<input id="iLatAS" name="iLatAS" type="text" size="2" maxlength=5 />').appendTo(sM);
        sM.append('″');
        var iLatAL = $('<select id="iLatAL" name="iLatAL"><option value="1">LU</option><option value="2">LS</option></select>').appendTo(sM);
        sM.append(' / <label for="iLonAD">Longitude :</label>');
        var iLonAD = $('<input id="iLonAD" name="iLonAD" type="text" size="1" maxlength=3 />').appendTo(sM);
        sM.append('° ');
        var iLonAM = $('<input id="iLonAM" name="iLonAM" type="text" size="1" maxlength=2 />').appendTo(sM);
        sM.append('′ ');
        var iLonAS = $('<input id="iLonAS" name="iLonAS" type="text" size="2" maxlength=5 />').appendTo(sM);
        sM.append('″');
        var iLonAL = $('<select id="iLonAL" name="iLonAL"><option value="1">BT</option><option value="2">BB</option></select>').appendTo(sM);
        this.element.append(sM);
        if (!this.options.useDMS) {
            sM.css('display', 'none');
        } else {
            sD.css('display', 'none');
        }
        $([iLatAD, iLatAM, iLatAS, iLonAD, iLonAM, iLonAS]).each(function () {
            $(this).change(function () {
                var lat = $.converttoDegree(iLatAD.val(), iLatAM.val(), iLatAS.val(), iLatAL.val() == 1 ? 1 : -1);
                var lon = $.converttoDegree(iLonAD.val(), iLonAM.val(), iLonAS.val(), iLonAL.val() == 1 ? 1 : -1);
                if (lat + lon == NaN) return;
                iLatD.val(lat);
                iLonD.val(lon);
                self._trigger("changed", null, { value: [lat, lon] });
            });
        });
        $([iLatD, iLonD]).each(function () {
            $(this).change(function () {
                var lat = $.converttoDMS(iLatD.val());
                var lon = $.converttoDMS(iLonD.val());
                if (lat[0] + lon[0] == NaN) return;
                iLatAD.val(lat[0]);
                iLatAM.val(lat[1]);
                iLatAS.val(lat[2]);
                iLatAL.val(lat[3] == 1 ? 1 : 2);
                iLonAD.val(lon[0]);
                iLonAM.val(lon[1]);
                iLonAS.val(lon[2]);
                iLonAL.val(lon[3] == 1 ? 1 : 2);
                self._trigger("changed", null, { value: [iLatD.val(), iLonD.val()] });
            });
        });
        $([iLatAD, iLatAM, iLatAS, iLonAD, iLonAM, iLonAS]).each(function () {
            $(this).on('paste', function () {
                //remove maxlength attribute
                $(this).val('');
                var ml = $(this).attr('maxlength');
                $(this).removeAttr('maxlength');
                $(this).one("input.fromPaste", function () {
                    var regD = RegExp("\\d+\\.?\\d*", 'g');
                    var regs = $(this).val().match(regD);
                    if (regs.length == 6) {
                        iLatAL.val($(this).val().indexOf('LS') == -1 ? 1 : 2);
                        iLonAL.val($(this).val().indexOf('BB') == -1 ? 1 : 2);
                        iLatAD.val(regs[0]);
                        iLatAM.val(regs[1]);
                        iLatAS.val(regs[2]);
                        iLonAD.val(regs[3]);
                        iLonAM.val(regs[4]);
                        iLonAS.val(regs[5]);
                        iLatAD.trigger('change');
                    }
                    if (regs.length == 3) {
                        if ($(this).val().indexOf('LS') == -1 && $(this).val().indexOf('LU') == -1) {
                            iLonAL.val($(this).val().indexOf('BB') == -1 ? 1 : 2);
                            iLonAD.val(regs[0]);
                            iLonAM.val(regs[1]);
                            iLonAS.val(regs[2]);
                            iLonAD.trigger('change');
                        } else {
                            iLatAL.val($(this).val().indexOf('LS') == -1 ? 1 : 2);
                            iLatAD.val(regs[0]);
                            iLatAM.val(regs[1]);
                            iLatAS.val(regs[2]);
                            iLatAD.trigger('change');
                        }
                    }
                    $(this).attr('maxlength', ml);
                });
            });
        });
    },
    useDMS: function (useDMS) {
        if (!useDMS) {
            this.element.find('.lonlat-sM').css('display', 'none');
            this.element.find('.lonlat-sD').css('display', 'inline-block');
        } else {
            this.element.find('.lonlat-sD').css('display', 'none');
            this.element.find('.lonlat-sM').css('display', 'inline-block');
        }
    }
});
$.converttoDMS = function (degree) {
    var lowsalong = (degree - Math.round(degree)) * 3600;
    return [Math.abs(Math.round(degree)), Math.abs(Math.floor(lowsalong / 60)), Math.abs((lowsalong % 60)).toFixed(2), degree >= 0 ? 1 : -1];
};
$.converttoDegree = function (degree, minute, second, sign) {
    return (parseInt(degree) + parseFloat(minute) / 60 + parseFloat(second) / 3600) * sign;
};