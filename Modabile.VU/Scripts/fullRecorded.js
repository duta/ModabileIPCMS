var zoom = 10;
var mapGeoJson, vectorLayer, layerGeoJson, zona_layerGeoJson, renderer, featuresShipOnDisplay;
var hostname = location.hostname;
var url = "http://" + hostname + ":8008/mapguide/mapagent/mapagent.fcgi?USERNAME=Anonymous&";

var selectFeatureControl;
var selectedFeature;
var markerLayer;

var metersPerUnit = 111319.4908;  //value returned from mapguide
var inPerUnit = OpenLayers.INCHES_PER_UNIT.m * metersPerUnit;
OpenLayers.INCHES_PER_UNIT["dd"] = inPerUnit;
OpenLayers.INCHES_PER_UNIT["degrees"] = inPerUnit;
OpenLayers.DOTS_PER_INCH = 96;

var SHADOW_Z_INDEX = 10;
var MARKER_Z_INDEX = 11;

var jmlKapalAdded = 0;
var selectedCCTV = '';
var selectedVR = '';

$(document).ready(function () {
    initMapGeoJson();
    OpenCloseNotif();
    $('#From').datetimepicker({ dateFormat: 'd-m-yy' });
    $('#To').datetimepicker({ dateFormat: 'd-m-yy' });
    $('#MMSI').autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetLabelValue",
                data: JSON.stringify({
                    mmsiOrShipNames: $('#MMSI').val()
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var js = new OpenLayers.Format.JSON();
                    var resultArr = js.read(data.d);
                    response(resultArr);
                }
            });
        },
        minLength: 2
    });

   
    $("#slider").slider({
        value: 10,
        min: 10,
        max: 50,
        step: 1,
        slide: function (event, ui) {
            $("#amount").val(ui.value);
        }
    });

    $("#amount").val($("#slider").slider("option", "min"));
    $('#intervalUnit').val("Second");
    $('#intervalUnit').change(function () {
        var selected = $('#intervalUnit option:selected').text();
        if (selected == "Second") {
            $("#slider").slider("option", "max", 50); $("#slider").slider("option", "min", 10)
        }
        else if (selected == "Minute") {
            $("#slider").slider("option", "max", 30); $("#slider").slider("option", "min", 1)
        }
        else if (selected == "Hour") {
            $("#slider").slider("option", "max", 4); $("#slider").slider("option", "min", 1)
        }
        $("#slider").slider("option", "value", $("#slider").slider("option", "min"));
        $("#amount").val($("#slider").slider("option", "min"));
    });

    $("#cmdAddShips").click(function () {
        if ($('#MMSI').val() == "") {
            return;
        }
        // cek jml kapal
        if ($("[id*='addedShipsRow']").length + 1 > 5) {
            alert('Jumlah kapal kami batasi <= 5.');
            return;
        }

        var duplicateFound = false;
        // cek duplikasi daftar
        $("[id*='addedShipsRow']").each(function () {
            if ($(this).attr('title') == $('#MMSI').val()) {
                duplicateFound = true;
                return;
            }
        });

        if (duplicateFound) {
            alert('Kapal sudah ada dalam daftar');
            return;
        }
        jmlKapalAdded += 1;
        var textHTML = '<div id="addedShipsRow' + jmlKapalAdded + '" class="baris-isi" title="' + $('#MMSI').val() + '">';
        textHTML += '<div class="kolom-name-ship-list terang-zone" style="font-size:12px">' + $('#MMSI').val() + '</div>';
        textHTML += '<div class="kolom-id-zone terang-zone" style="cursor:pointer; font-size:12px; color: #FF0000" onclick="RemoveDaftarKapalId(' + jmlKapalAdded + ');">X</div>';
        textHTML += '<div style="clear:both;"></div>';
        textHTML += '</div>';
        $("#daftarShips").html($("#daftarShips").html() + textHTML);
    });

    $("#reqDisplay").click(function () {
        if (!validateInputsDate()) return false;
        featuresShipOnDisplay = null;
        $("#cmdAddShips").attr("disabled", "disabled");
        vectorLayer.destroyFeatures();
        $('#lblLoading').html('Loading Data ...');
        getListOfRequestDate();
        if (dateQueue.length() > 0) {
            animationQueue = [];
            var firstQueue = dateQueue.dequeue();
            if ($("[id*='addedShipsRow']").length == 0) RequestToServer("-1", firstQueue.dateFrom, firstQueue.dateTo);
            else RequestToServer(firstQueue.mmsi.split(" ")[0], firstQueue.dateFrom, firstQueue.dateTo);
        }
        $('#lblLoading').html('Done!');
        return false;
    });

    $("#cmdGetKml").click(function () {
        var params = "";
        currentDate = getDateExpression($('#From').val());
        params += "dateStart=" + currentDate[3] + "-" + currentDate[2] + "-" + currentDate[1] + "-" + currentDate[4] + "-" + currentDate[5];
        endDate = getDateExpression($('#To').val());
        params += "&dateEnd=" + endDate[3] + "-" + endDate[2] + "-" + endDate[1] + "-" + endDate[4] + "-" + endDate[5];
        params += "&Interval" + $('#intervalUnit option:selected').text() + "=" + $('#amount').val();
        var i = 0;
        $("[id*='addedShipsRow']").each(function () {
            params += "&mmsi[" + i + "]=" + $(this).attr('title').split(" ")[0];
            i++;
        });
        window.location = "RecordedKML.ashx?" + params;
    });

    $('#smoothSpeed').spinner({ min: 1, value: 1, suffix: 'x' });
    $('.ui-spinner.ui-widget').css('position', 'absolute');
    $('#smoothSpeed').css('margin-right', 0);
    $("#cbSmooth").change(function () {
        if ($("#cbSmooth").is(':checked')) {
            $('#smoothControls').css('display', 'block');
            $('#nonSmoothControls').css('display', 'none');
        } else {
            $('#nonSmoothControls').css('display', 'block');
            $('#smoothControls').css('display', 'none');
        }
    });
    if ($("#cbSmooth").is(':checked')) {
        $('#smoothControls').css('display', 'block');
        $('#nonSmoothControls').css('display', 'none');
    } else {
        $('#nonSmoothControls').css('display', 'block');
        $('#smoothControls').css('display', 'none');
    }

});

function RemoveDaftarKapalId(id) {
    $('#addedShipsRow' + id).remove();
};

function createArrowLayer() {
    var context = {
        getShipStyle: function (ft) {
            if (ft.attributes.ShipTypeDescription != null) {
                if (ft.attributes.ShipTypeDescription.indexOf("Tug") != -1) {
                    return "Styles/bluetritunda.png";
                }
            }
            return "Styles/greentri_titik.png";
        },
        getShipWidth: function (ft) {
            defaultWidth = 26;
            if ($("#cbLoa").attr('checked') == null) return defaultWidth;
            if ((ft.attributes.ToPort == null && ft.attributes.ToStarBoard == null) ||
             (ft.attributes.ToPort == '' && ft.attributes.ToStarBoard == '')) return defaultWidth;
            wide = (parseInt(ft.attributes.ToPort) + parseInt(ft.attributes.ToStarBoard));
            //AAV:countHeight
            var boun = mapGeoJson.calculateBounds();
            var distLeft = OpenLayers.Util.distVincenty(new OpenLayers.LonLat(boun.left, boun.top), new OpenLayers.LonLat(boun.right, boun.top)) * 1000; //result in km, so we change it to m to make it easier to count
            var width = 36; //image width
            return document.getElementById("cbLoa").checked ? mapGeoJson.size.w * wide / distLeft : width;
        },
        getShipHeight: function (ft) {
            //defaultHeight = 15;
            defaultHeight = 46;
            if ($("#cbLoa").attr('checked') == null) return defaultHeight;
            if ((ft.attributes.ToBow == null && ft.attributes.ToStern == null) ||
             (ft.attributes.ToBow == '' && ft.attributes.ToStern == '')) return defaultHeight;
            loa = (parseInt(ft.attributes.ToBow) + parseInt(ft.attributes.ToStern));
            //AAV:countHeight
            var boun = mapGeoJson.calculateBounds();
            var distLeft = OpenLayers.Util.distVincenty(new OpenLayers.LonLat(boun.left, boun.top), new OpenLayers.LonLat(boun.left, boun.bottom)) * 1000; //result in km, so we change it to m to make it easier to count
            var height = 40; //image height
            return document.getElementById("cbLoa").checked ? mapGeoJson.size.h * loa / distLeft : height;
        },
        getShipLabel: function (ft) {
            var timeStamp = +ft.attributes.TimeStampReceiver.toString().substring(6, 8) +
	'-' + ft.attributes.TimeStampReceiver.toString().substring(4, 6) +
	'-' + ft.attributes.TimeStampReceiver.toString().substring(0, 4) +
	' ' + ft.attributes.TimeStampReceiver.toString().substring(8, 10) +
	':' + ft.attributes.TimeStampReceiver.toString().substring(10, 12) +
	':' + ft.attributes.TimeStampReceiver.toString().substring(12, 14);

            $("[id*='addedShipsRow']").each(function () {
                this_mmsi = $(this).attr('title');
                if (this_mmsi.split(' ')[0] == ft.attributes.MMSI) {
                    ft.attributes.ShipName = this_mmsi.split(' ')[1];
                }
            });
            var speed = ft.attributes.SOG / 10;
            var finalLabel = mapGeoJson.getScale() < 50000 ? (ft.attributes.ShipName == null ? ft.attributes.MMSI + " " + speed + "k" + " " + timeStamp : ft.attributes.ShipName + " " + speed + "k" + " " + timeStamp) : "";
            return $('#cbShowLabel').is(':checked') ? finalLabel : "";
        }
    };

    var templateShip = {
        //externalGraphic: "Styles/arrow_up.png", // using context.getSize(feature)
        externalGraphic: "${getShipStyle}",
        // Makes sure the background graphic is placed correctly relative
        // to the external graphic.
        backgroundXOffset: 0,
        backgroundYOffset: -7,
        graphicWidth: "${getShipWidth}",
        graphicHeight: "${getShipHeight}",
        //show label and rotate image
        //label: "${MMSI} ${ShipName}",
        labelYOffset: 15,
        label: "${getShipLabel}",
        rotation: "${HDG}",

        // Set the z-indexes of both graphics to make sure the background
        // graphics stay in the background (shadows on top of markers looks
        // odd; let's not do that).
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX,

        pointRadius: 10
    };

    // allow testing of specific renderers via "?renderer=Canvas", etc
    renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
    renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

    var styleShip = new OpenLayers.Style(templateShip, { context: context });

    vectorLayer = new OpenLayers.Layer.Vector("ship_layer",
                {
                    styleMap: new OpenLayers.StyleMap(styleShip),
                    isBaseLayer: false,
                    rendererOptions: { yOrdering: true },
                    renderers: renderer
                });
    mapGeoJson.addLayer(vectorLayer);
}

function displayInfo(event) {
    var text = '<strong>'
							+ event.feature.attributes.location
							+ '</strong>'
							+ ': '
							+ event.feature.attributes.description
							+ ' rotation '
							+ event.feature.attributes.rotation;
    popup = new OpenLayers.Popup.FramedCloud(
									"chicken",
                                    event.feature.geometry.getBounds().getCenterLonLat(),
                                    null,
                                    "<div style='font-size:.8em'>Feature: " + text + "</div>",
                                    null,
									true,
									onPopupClose);
    event.feature.popup = popup;
    mapGeoJson.addPopup(popup);
    selectedFeature = event.feature;
}

function onPopupClose() {
    selectFeatureControl.unselect(selectedFeature);
}

function readGeoJSON(data) {
    var geojson_format = new OpenLayers.Format.GeoJSON();
    var features = geojson_format.read(data);
    if (features.length > 0) {
        if (featuresShipOnDisplay == null) {
            featuresShipOnDisplay = features;
        }
        else {
            featuresShipOnDisplay = featuresShipOnDisplay.concat(features);
        }
    }
}

function drawFeature(mmsi) {
    if (featuresShipOnDisplay == null) return;
    var jmlFeature = featuresShipOnDisplay.length;

    if (jmlFeature == 0) { $('#lblLoading').html('No Data Found'); isDrawing = false; return; }
    $('#lblLoading').html('Drawing data ... [Receive approx. ' + jmlFeature + ' data]');

    if (markerLayer != null) {
        markerLayer.destroy();
        markerLayer = null;
    }

    var showTrail = $("#cbTrail").attr('checked') == null ? false : true;
    if (mmsi == "-1") vectorLayer.destroyFeatures();
    time = 0;
    var size = new OpenLayers.Size(100, 100);
    var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
    var centeredYet = false;
    for (var i = 0; i < jmlFeature; i++) {
        if (!showTrail) {
            vectorLayer.removeFeatures(vectorLayer.getFeaturesByAttribute("MMSI", featuresShipOnDisplay[i].attributes.MMSI));
        }
        vectorLayer.addFeatures(featuresShipOnDisplay[i]);
    }
    featuresShipOnDisplay = null;
    $('#lblLoading').html('Done.');
}

//tiled version
function initTiled() {
    var extent = new OpenLayers.Bounds(95.0596541500001, -10.99740704, 141.00718685, 5.90688428000004);
    var tempScales = [1128.49722, 2256.99444, 4513.98888, 9027.977761, 18055.95552, 36111.91104, 72223.82209, 144447.6442, 288895.2884, 577790.5767, 1155581.153, 2311162.307, 4622324.614, 9244649.227, 18489298.45, 36978596.91, 73957193.82, 147914387.6, 295828775.3, 591657550.5]

    var mapOptions = {
        maxExtent: extent,
        scales: tempScales
    };
    mapGeoJson = new OpenLayers.Map('mapGeoJson', mapOptions);

    var params = {
        mapdefinition: 'Library://Jatim/MapsTiled/JatimTiled3.MapDefinition',
        basemaplayergroupname: "Basemap"
    }
    var options = {
        singleTile: false
    }
    var layer = new OpenLayers.Layer.MapGuide("MapGuide OS tiled layer", url, params, options);
    mapGeoJson.addLayer(layer);

    mapGeoJson.setCenter(new OpenLayers.LonLat(lon, lat), zoom);
}

function initMapGeoJson() {
    initTiled();
    createArrowLayer();
    mapGeoJson.addControl(new OpenLayers.Control.LayerSwitcher());    
}

function compareDate(dateFromStr, dateEndStr) {
    dateFrom = getDateObject(dateFromStr)
    dateEnd = getDateObject(dateEndStr);

    return (dateEnd.getTime() - dateFrom.getTime()) / 1000; // returned in millisecond base so we need to convert it to second, just for convenience
}

function getDateObject(dateStr) {
    dateExpr = getDateExpression(dateStr);
    return new Date(dateExpr[3], dateExpr[2] - 1, dateExpr[1], dateExpr[4], dateExpr[5]);
}

function getDateExpression(dateString) {
    format = /^(\d{1,2})\-(\d{1,2})\-(\d{4}) (\d{1,2}):(\d{1,2})$/;
    return dateString.match(format);
}

function validateInputsDate() {
    var From = $('#From').val();
    var To = $('#To').val();
    var intervalAmount = $("#amount").val();

    if (From == '' & To == '') {
        alert("Tanggal tidak boleh kosong.");
        $('#From').focus();
        return false;
    }

    if (compareDate(From, To) <= 0) {
        alert("Tanggal akhir harus lebih dari tanggal mulai.");
        $('#To').focus();
        return false;
    }
    else {
        diffSecond = compareDate(From, To);
        intervalInSecond = 0;
        var selected = $('#intervalUnit option:selected').text();
        if (selected == "Second") {
            if (diffSecond < intervalAmount) {
                alert("Jarak waktu yang dipilih kurang dari " + $("#amount").val() + " second(s)");
                return false;
            } else {
                intervalInSecond = intervalAmount;
            }
        }
        else if (selected == "Minute") {
            diffMinute = diffSecond / 60;
            if (diffMinute < 1) {
                alert("Date range is not applicable to minute interval.");
                return false;
            } else if (diffMinute < intervalAmount) {
                alert("Jarak waktu yang dipilih kurang dari " + $("#amount").val() + " minute(s)");
                return false;
            } else {
                intervalInSecond = $("#amount").val() * 60;
            }
        }
        else if (selected == "Hour") {
            diffMinute = diffSecond / 3600;
            if (diffMinute < 1) {
                alert("Date range is not applicable to hour interval.");
                return false;
            } else if (diffMinute < intervalAmount) {
                alert("Jarak waktu yang dipilih kurang dari " + $("#amount").val() + " hour(s)");
                return false;
            } else {
                intervalInSecond = $("#amount").val() * 3600;
            }
        }
    }
    return true;
}

var dateQueue;
function getListOfRequestDate() {
    var iUnit = $('#intervalUnit option:selected').val();
    if ($('#cbSmooth').is(':checked')) {
        intervalInSecond = 60;
        iUnit = 'second';
    }
    currentDate = getDateObject($('#From').val());
    endDate = getDateObject($('#To').val());
    dateQueue = new Queue();
    while (currentDate < endDate) {
        var fromStr =
            +currentDate.getFullYear() + "-"
            + (currentDate.getMonth() + 1) + "-"
            + currentDate.getDate() + " "
            + currentDate.getHours() + ":"
            + (iUnit != 'hour' ? currentDate.getMinutes() : '00') + ":"
            + (iUnit == 'second' ? currentDate.getSeconds() : '00');

        currentDate.setTime(currentDate.getTime() + intervalInSecond * 1000);
        if (currentDate > endDate) currentDate = endDate;

        var toStr =
        +currentDate.getFullYear() + "-"
        + (currentDate.getMonth() + 1) + "-"
        + currentDate.getDate() + " "
        + currentDate.getHours() + ":"
        + (iUnit != 'hour' ? currentDate.getMinutes() : '00') + ":"
        + (iUnit == 'second' ? currentDate.getSeconds() : '00') + '.000000';

        //loop each mmsi selected
        if ($("[id*='addedShipsRow']").length == 0) {
            dateQueue.enqueue({ dateFrom: fromStr, dateTo: toStr });
        }
        $("[id*='addedShipsRow']").each(function () {
            this_mmsi = $(this).attr('title');
            dateQueue.enqueue({ mmsi: this_mmsi, dateFrom: fromStr, dateTo: toStr });
        });
    }
    $('#lblLoading').html('Finish collecting');
}

function RequestToServer(mmsiVal, dateFromVal, dateToVal) {
    try {
        $.ajax({
            type: "POST",
            url: 'FullRecorded.aspx/GetRecorded' + ($('#cbSmooth').is(':checked') ? 'Smooth' : ''),
            data: JSON.stringify({
                mmsi: mmsiVal,
                "dateFrom": dateFromVal,
                "dateTo": dateToVal
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                if ($('#cbSmooth').is(':checked')) {
                    QueueGeoJSON(mmsiVal, data.d);
                } else {
                    readGeoJSON(data.d);
                    drawFeature(mmsiVal);
                }
                if (dateQueue.length() > 0) {
                    var firstQueue = dateQueue.dequeue();
                    $('#lblLoading').html('Request ' + (mmsiVal == "-1" ? '' : firstQueue.mmsi + ' from ') + firstQueue.dateFrom);
                    setTimeout(RequestToServer(mmsiVal == "-1" ? "-1" : firstQueue.mmsi.split(" ")[0], firstQueue.dateFrom, firstQueue.dateTo), 800);
                } else {
                    if ($('#cbSmooth').is(':checked')) {
                        TriggerQueue();
                    } else {
                        //enable button add ships
                        $('#lblLoading').html('Finished drawing.');
                        $('#cmdAddShips').removeAttr("disabled");
                    }
                }
            }
        });
    } catch (e) {
        alert(e.Data);
    }
}

var animationQueue = [];
function QueueGeoJSON(mmsi, data) {
    var geojson_format = new OpenLayers.Format.GeoJSON();
    var features = geojson_format.read(data);
    if (features.length > 0) {
        features.sort(function (a, b) { return a.data.TimeStampReceiver - b.data.TimeStampReceiver; });
        if (mmsi == "-1") {
            for (var i = 0; i < features.length; i++) {
                var currInQueue = $.grep(animationQueue, function (e) { return e.mmsi == features[i].data.MMSI; });
                if (currInQueue.length != 0) {
                    currInQueue[0].queue.push(features[i]);
                } else {
                    animationQueue.push({ "mmsi": features[i].data.MMSI, "queue": [features[i]], "index": -1 });
                }
            }
        } else {
            var currInQueue = $.grep(animationQueue, function (e) { return e.mmsi == mmsi; });
            if (currInQueue.length != 0) {
                currInQueue[0].queue.concat(features);
            } else {
                animationQueue.push({ "mmsi": mmsi, "queue": features, "index": -1 });
            }
        }
    }
}
var animStart = 0, animEnd = 0, animInc;
function TriggerQueue() {
    currentDate = getDateObject($('#From').val());
    animStart = parseInt(
        currentDate.getFullYear() + ""
        + pad((currentDate.getMonth() + 1), 2) + ""
        + pad(currentDate.getDate(), 2) + ""
        + pad(currentDate.getHours(), 2) + ""
        + pad(currentDate.getMinutes(), 2) + ""
        + pad(currentDate.getSeconds(), 2) + "000000");
    currentDate = getDateObject($('#To').val());
    animEnd = parseInt(
        currentDate.getFullYear() + ""
        + pad((currentDate.getMonth() + 1), 2) + ""
        + pad(currentDate.getDate(), 2) + ""
        + pad(currentDate.getHours(), 2) + ""
        + pad(currentDate.getMinutes(), 2) + ""
        + pad(currentDate.getSeconds(), 2) + "000000");
    animInc = $('#smoothSpeed').spinner('value') * 30000;
    vectorLayer.destroyFeatures();
    DoAnimQueue();
}
function DoAnimQueue() {
    animStart += animInc;
    for (var i = 0; i < animationQueue.length; i++) {
        if (animationQueue[i].queue.length == (animationQueue[i].index + 1)) continue;
        if (animationQueue[i].queue[animationQueue[i].index + 1].data.TimeStampReceiver < animStart) {
            animationQueue[i].index += 1;
            if (animationQueue[i].index == 0) {
                vectorLayer.addFeatures(animationQueue[i].queue[animationQueue[i].index]);
                continue;
            }
            var feat = $.grep(vectorLayer.features, function (e) { return e.data.MMSI == animationQueue[i].mmsi; });
            feat[0].geometry.x = animationQueue[i].queue[animationQueue[i].index].geometry.x;
            feat[0].geometry.y = animationQueue[i].queue[animationQueue[i].index].geometry.y;
            continue;
        }
        if (animationQueue[i].index >= 0) {
            var feat = $.grep(vectorLayer.features, function (e) { return e.data.MMSI == animationQueue[i].mmsi; });
            var scalous = (animStart - animationQueue[i].queue[animationQueue[i].index].data.TimeStampReceiver) /
                (animationQueue[i].queue[animationQueue[i].index + 1].data.TimeStampReceiver - animationQueue[i].queue[animationQueue[i].index].data.TimeStampReceiver);
            feat[0].geometry.x = animationQueue[i].queue[animationQueue[i].index].geometry.x +
                (animationQueue[i].queue[animationQueue[i].index + 1].geometry.x - animationQueue[i].queue[animationQueue[i].index].geometry.x) * scalous;
            feat[0].geometry.y = animationQueue[i].queue[animationQueue[i].index].geometry.y +
                (animationQueue[i].queue[animationQueue[i].index + 1].geometry.y - animationQueue[i].queue[animationQueue[i].index].geometry.y) * scalous;
        }
    }
    vectorLayer.setVisibility(false);
    vectorLayer.setVisibility(true);
    if (animStart > animEnd) {
        $('#lblLoading').html('Finished drawing.');
        $('#cmdAddShips').removeAttr("disabled");
        return;
    }
    $('#lblLoading').html('Drawing ' + animStart);
    setTimeout(DoAnimQueue, 30);
}
function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function Queue() {
    this.stac = new Array();
    this.dequeue = function () {
        return this.stac.pop();
    }
    this.enqueue = function (item) {
        this.stac.unshift(item);
    }
    this.length = function () {
        return this.stac.length;
    }
}

$('#notifsTitle').click(function () {
    OpenCloseNotif();
});

function OpenCloseNotif() {
    if ($('#notifsTitle').html() == '\u25BC') {//if open
        $('#notifsTitle').html('&#x25B2;');
        $('#notifsContent').css('display', 'none');//close
    } else {
        $('#notifsTitle').html('&#x25BC;');
        $('#notifsContent').css('display', 'block');//open
    }
}