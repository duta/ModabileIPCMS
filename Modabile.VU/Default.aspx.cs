﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using AIS.DataLogger.Entity;
using LetMeIn;

namespace Modabile.VU
{
    public partial class Default : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (this.Session["sec_temp"].ToString() == "modabileduta") { Response.Redirect("fulldesigna.aspx"); return; }            
            cmdSignIn.ServerClick += new EventHandler(cmdSignIn_ServerClick);
            AutoLoginModabile();
        }

        private void AutoLoginModabile()
        {
            if (Request.Params["user"] != null)
            {
                if (Request.Params["user"].ToString().ToUpper() == "MODABILE")
                {
                    txtUser.Value = "modabile";
                    txtPwd.Value = "P3rkapalan";
                    cmdSignIn_ServerClick(null, null);
                }
            }
        }

        void cmdSignIn_ServerClick(object sender, EventArgs e)
        {
            //if (txtUser.Value == "modabile" & txtPwd.Value == "pelindo3")
            //{
            //    this.Session["sec_temp"] = "modabileduta";
            //    Response.Redirect("FullDesignA.aspx");
            //}         
 
            //get user_id from user_name
            int? user_id = CreateFactoryLeyfi().GetUsers().GetUserIDByUserName(txtUser.Value);
            if (!user_id.HasValue) return;
            byte[] savedPassword = CreateFactoryLeyfi().GetUsers().GetPasswordByUserId(user_id.Value);
            LetMeIn.Password p = new LetMeIn.Password();
            bool pass = p.Pass(txtPwd.Value, savedPassword);
            if (pass)
            {
                ////checking user ini apakah sebagai role admin
                //pass = (CreateFactoryLeyfi().GetUsers().GetRolesByUserID(user_id.Value).Find(r => r.role_name == "Admin") != null);
                //panelUserName.Visible = pass;
                //panelLogin.Visible = !pass;
                //if (pass)
                //{
                    CreateFactoryLeyfi().GetUsers().SetUserLogIn(user_id.Value);
                    //lblUser.Text = txtUserName.Value;
                    HttpCookie logged = Request.Cookies["leyfi-log"];
                    if (logged == null)
                    {
                        logged = new HttpCookie("leyfi-log");
                    }
                    logged.Expires = DateTime.Now.AddDays(1);
                    logged.Values["who"] = user_id.Value.ToString();
                    logged.Values["name"] = CreateFactoryLeyfi().GetUsers().GetUserByUserID(user_id.Value).user_name;
                    logged.Values["logged"] = "true";
                    Response.Cookies.Add(logged);
                //}
                    Response.Redirect("FullDesignA.aspx");
            }
        }


        [WebMethod]
        public static string GetLabelValue(string mmsiOrShipNames)
        {
            AIS.DataLogger.Entity.LabelValue[] lblValues = CreateFactoryLogPos().GetShipInfo().GetLabelValueOfMMSIShipName(mmsiOrShipNames);
            return lblValues.Length > 0 ? new JsonHelper().ToStringJson(lblValues) : "";
        }      
    }
}