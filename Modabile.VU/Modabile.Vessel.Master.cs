﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace Modabile.VU
{
    public partial class ModabileVesselMaster : System.Web.UI.MasterPage
    {       

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
       
        protected void cmdSignOut_Click(object sender, EventArgs e)
        {
            HttpCookie logged = Request.Cookies["leyfi-log"];
            if (logged.Values["who"].ToString() == "")
            {
                Response.Redirect("Default.aspx");
            }
            int user_id = int.Parse(logged.Values["who"].ToString());
            BasePage.CreateFactoryLeyfi().GetUsers().SetUserLogOut(user_id);
            logged.Values["who"] = "";
            logged.Values["name"] = "";
            logged.Values["logged"] = "false";
            logged.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(logged);
            Response.Redirect("Default.aspx");
        }
    
        
    }
}
