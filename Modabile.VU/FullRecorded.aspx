﻿<%@ Page Title="Modabile Recorded" Language="C#" MasterPageFile="~/Modabile.Vessel.Master" AutoEventWireup="true" CodeBehind="FullRecorded.aspx.cs" Inherits="Modabile.VU.FullRecorded" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Scripts/jquery-tab/vanilla-tab-style.css" rel="stylesheet" type="text/css" />
    <link href="theme/fullscreen/map-fullscreen.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="Styles/table-detail.css" />
    <link rel="stylesheet"  type="text/css" href="css/ui-darkness/jquery-ui-1.8.21.custom.css" />
    <link href="Styles/hover-menu.css" rel="stylesheet" type="text/css" />
    <link href="Styles/gaya.css" rel="stylesheet" type="text/css" />
    <link href="Styles/google.css" rel="stylesheet" type="text/css" />
    <link href="theme/fullscreen/map-fullrecorded.css" rel="stylesheet" type="text/css" />    
	<link rel="stylesheet" type="text/css" href="css/jquery-ui-timepicker-addon.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div id="ab-header">
         <div id="ab-tabvanilla">
             <div id="tabvanilla" class="widget">                
                <ul class="tabnav">
                    <li><a href="#tiga">Filter</a></li>                                                
                </ul>                            
                <div id="tiga" class="tabdiv">                                   
                    <input type="text" name="MMSI" id="MMSI"/>  
                    <label for="MMSI">MMSI</label>                                                                                
                    <input type="text" name="From" id="From" />
                    <label for="From">From</label>                       
                    <input type="text" name="To" id="To" />
                    <label for="To">To</label>
					<br />
                    <input type="checkbox" name="cbSmooth" id="cbSmooth" />
                    <label for="cbSmooth">Smooth</label>
                    <br />
                    <br />
                    <div id="smoothControls" style="position:relative;">
                    <label for="smoothSpeed">Percepatan</label>
                    <input type="text" id="smoothSpeed" name="smoothSpeed" />
                    <br />
                    <br />
                    </div>
                    <div id="nonSmoothControls" style="position:relative;">
                    <div id="slider"></div>
                    <label for="Interval">Interval</label>
                    <input type="text" readonly="readonly" id="amount"  />
                    <select name="intervalUnit" id="intervalUnit">
                        <option value="second">Second</option>
                        <option value="minute">Minute</option>
						<option value="hour">Hour</option>
                    </select>
					</div>
                    <div id="daftarShips" ></div>         
                    <input type="checkbox" name="cbShowLabel" id="cbShowLabel" checked /><label for="cbShowLabel">Show Label</label>
                    <br />
                    <input type="checkbox" name="cbTrail" id="cbTrail" checked="checked" /><label for="cbTrail">Show Trail</label><br />
                    <input type="checkbox" name="cbLoa" id="cbLoa" /><label for="cbLoa">Show LOA</label>                    
                    <div id="separator"></div>     
                    <input type="button" id="cmdGetKml" value="Download KML" />
                    <input type="submit" name="reqDisplay" id="reqDisplay" value="Track" />                     
                    <input type="button" id="cmdAddShips" value="Add" />         
                    <div id="lblLoading"></div>
                </div>
             </div>
         </div>
        <div><img src="Styles/logo.png"></div>         
     </div>
    <div id="mapGeoJson" ></div>
    <div id="footer2"></div>
    <script>
        lon = <%= ConfigurationManager.AppSettings["centerlon"] %>;
        lat = <%= ConfigurationManager.AppSettings["centerlat"] %>;
    </script>
    <script src="Scripts/proj4js/proj4js-compressed.js" type="text/javascript"></script>
    <script src="Scripts/proj4js/defs/EPSG900913.js" type="text/javascript"></script>    
    <script src="Scripts/jquery-tab/jquery-ui-personalized-1.5.2.packed.js" type="text/javascript"></script>
    <script src="Scripts/jquery-tab/sprinkle.js" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="Scripts/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="Scripts/jquery-ui-sliderAccess.js"></script>
	<script type="text/javascript" src="Scripts/jquery.ui.spinner.min.js"></script>
    <script src="Scripts/fullRecorded.js" type="text/javascript"></script>
</asp:Content>
