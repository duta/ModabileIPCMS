﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration; 
using AIS.Core;
using AIS.DataLogger;
using AIS.DataLogger.Entity;
using AIS.DataLogger.Interfaces;

namespace Modabile.VU
{
    public partial class FullRecorded : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {            
        }
        
        [WebMethod]
		public static string GetRecorded(string mmsi, string dateFrom, string dateTo)
        {
			Entities.ShipDisplayFeatureCollection result = BasePage.CreateFactoryLogPos().GetAISLog().RecordedPositionByMMSIAndDate(mmsi, dateFrom, dateTo);
            return new JsonHelper().ToStringJson(result); 
        }

        [WebMethod]
        public static string GetRecordedSmooth(string mmsi, string dateFrom, string dateTo)
        {
            Entities.ShipDisplayFeatureCollection result = BasePage.CreateFactoryLogPos().GetAISLog().RecordedSmoothByMMSIAndDate(mmsi, dateFrom, dateTo);
            return new JsonHelper().ToStringJson(result);
        }
       
    }
}