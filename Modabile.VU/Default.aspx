﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Modabile.VU.Default" %>

<!DOCTYPE html >

<html lang="en">
    <meta charset="utf-8">
    <title>Modabile Home Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ptdmc">
    <!-- Le styles -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="Styles/defaultPage.css" rel="stylesheet">
   
<head runat="server">
    <title></title>
 
</head>
<body>
    <form id="form1" runat="server">
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container" >
          
          <a class="brand" href="#" ></a>
          <div id="logo"></div>
          <div class="nav-collapse collapse">
            
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
        <div class="content">
			<div class="row">
				<div class="login-form">
					<h2>Login</h2>
						<fieldset>
							<div class="clearfix">
								<input type="text" id="txtUser" runat="server" placeholder="Username">
							</div>
							<div class="clearfix">
								<input type="password" id="txtPwd" runat="server" placeholder="Password">
							</div>
							<button id="cmdSignIn" class="btn btn-primary" type="submit" runat="server" >Sign in</button>
						</fieldset>
				</div>
			</div>
		</div>
    </div> <!-- /container -->

    <script src="Scripts/jquery-1.7.2.min.js" ></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="Scripts/defaultPage.js" ></script>
    </form>
</body>
</html>
