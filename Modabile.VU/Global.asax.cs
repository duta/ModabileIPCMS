﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;

namespace Modabile.VU
{
    public class Global : System.Web.HttpApplication, IContainerAccessor
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Create the Windsor Container for IoC.
            // Supplying "XmlInterpreter" as the parameter tells Windsor 
            // to look at web.config for any necessary configuration.
            windsorContainer = new WindsorContainer(new XmlInterpreter());

        }

        void Application_End(object sender, EventArgs e)
        {
            windsorContainer.Dispose();
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }


        public IWindsorContainer Container
        {
            get { return windsorContainer; }
        }

         /// <summary>
           /// Provides a globally available access to the <see cref="IWindsorContainer" /> instance.
           /// </summary>
           public static IWindsorContainer WindsorContainer {
               get { return windsorContainer; }
           }

        /// <summary>
           /// Gets instantiated on <see cref="Application_Start" />.
           /// </summary>
           private static IWindsorContainer windsorContainer;
    }
}
