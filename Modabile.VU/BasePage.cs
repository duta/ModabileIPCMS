﻿using AIS.DataLogger.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Modabile.VU
{
    public class BasePage : System.Web.UI.Page
    {
        public bool Pass { get; set; }

        public bool PassAdminMenu { get; set; }

        public bool PassOnlyAdminMenu { get; set; }

        public static string UserNameLoggedIn
        {
            get
            {
                HttpCookie logged = HttpContext.Current.Request.Cookies["leyfi-log"];
                if (logged == null) return "";
                return logged.Values["name"].ToString();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void OnInit(EventArgs e)
        {
            try
            {
                HttpCookie logged = Request.Cookies["leyfi-log"];
                string path = this.Request.Path.ToLower();
                //logger.Info("Opening " + path);
                if (logged != null)
                {
                    bool isLogged = bool.Parse(logged.Values["logged"].ToString());
                    if (isLogged)
                    {
                        int user_id = int.Parse(logged.Values["who"].ToString());
                        //cek di DB,  lastlogin expired?
                        if (BasePage.CreateFactoryLeyfi().GetUsers().LastLoginExpired(user_id, int.Parse(ConfigurationManager.AppSettings["max_hour_loginexpired"].ToString())))
                        {
                            // cek apakah last_logout == null?
                            if (BasePage.CreateFactoryLeyfi().GetUsers().LastLogoutNull(user_id))
                            {
                                //set logout ke DB
                                BasePage.CreateFactoryLeyfi().GetUsers().SetUserLogOut(user_id);
                            }
                            logged.Values["logged"] = "false";
                            Pass = false;
                        }
                        else
                        {
                            // jika belum expired, cek apakah sudah logout?
                            if (!BasePage.CreateFactoryLeyfi().GetUsers().LastLogoutNull(user_id))
                            {
                                logged.Values["logged"] = "false";
                                Pass = false;
                            }
                            else
                            {
                                //checking user ini apakah sesuai role-nya
                                List<Leyfi.Data.Entities.Roles> roles = BasePage.CreateFactoryLeyfi().GetUsers().GetRolesByUserID(user_id);
                                List<Leyfi.Data.Entities.Elements> elements = new List<Leyfi.Data.Entities.Elements>();
                                roles.ForEach(item => elements.AddRange(BasePage.CreateFactoryLeyfi().GetRoles().GetElementsPermittedByRoleID(item.role_id.Value).ToList()));

                                if (path.Contains("/fulldesigna.aspx") || path.Contains("/fullgoogle.aspx") ||
                                    path.Contains("/integrasi.aspx") || path.Contains("/daftarkedatangankapal.aspx") ||
                                    path.Contains("/zonedetail.aspx") || path.Contains("/kapaltravelzones.aspx") ||
                                    path.Contains("/cctvrecorded.aspx") || path.Contains("/drawingzone.aspx"))
                                {
                                    Pass = elements.Exists(elm => elm.element_type == "Modabile Member");
                                    PassAdminMenu = roles.Exists(role => role.role_name == "Admin" || role.role_name == "Superuser");
                                    PassOnlyAdminMenu = roles.Exists(role => role.role_name == "Admin");
                                }
                                else if (path.Contains("/drawingzone.aspx") || path.Contains("/updateimage.aspx") ||
                                    path.Contains("/tundarecord.aspx") || path.Contains("/uploadexcel.aspx") || path.Contains("/uploadcctv.aspx") ||
                                    path.Contains("/ktzonesetting.aspx"))
                                {
                                    Pass = elements.Exists(elm => elm.element_type == "Modabile Drawing");
                                }
                                else if (path.Contains("/fullrecorded.aspx"))
                                {
                                    Pass = elements.Exists(elm => elm.element_type == "Modabile Recorded");
                                }
                                else if (path.Contains("/mailconfigpage.aspx"))
                                {
                                    Pass = elements.Exists(elm => elm.element_type == "Modabile Mail");
                                }
                                else if (path.Contains("/ktzonesetting.aspx"))
                                {
                                    Pass = elements.Exists(elm => elm.element_type == "Modabile Zone KT");
                                }
                                else if (path.Contains("/masterkt.aspx"))
                                {
                                    Pass = elements.Exists(elm => elm.element_type == "Modabile Master KT");
                                }
                                else if (path.Contains("/default.aspx"))
                                {
                                    Pass = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        Pass = false;
                    }
                }
                else
                {
                    logged = new HttpCookie("leyfi-log");
                    logged.Values.Add("who", "");
                    logged.Values.Add("name", "");
                    logged.Values.Add("logged", "false");
                    logged.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(logged);
                    Pass = false;
                }

                if (!Pass)
                {
                    if (!path.Contains("/default.aspx")) Response.Redirect("Default.aspx");
                }
                else
                {
                    if (path.Contains("/default.aspx")) Response.Redirect("fulldesigna.aspx");
                    if (this.Master is ModabileVesselMaster)
                    {
                        ((System.Web.UI.WebControls.Label)this.Master.FindControl("lblUser")).Text = logged.Values["name"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message + ex.StackTrace);
            }
        }

        public static IFactory CreateFactoryGIS()
        {
            IFactory factoryGIS;

            if (HttpContext.Current.Session["factoryGIS"] == null)
                HttpContext.Current.Session["factoryGIS"] = (IFactory)Global.WindsorContainer["FactorySpatialData"];
            factoryGIS = (IFactory)HttpContext.Current.Session["factoryGIS"];

            return factoryGIS;
        }

        public static IFactory CreateFactoryLogPos()
        {
            IFactory factoryLogPos;
            if (HttpContext.Current.Session["factoryLogPos"] == null)
                HttpContext.Current.Session["factoryLogPos"] = (IFactory)Global.WindsorContainer["FactoryLogPos"];
            factoryLogPos = (IFactory)HttpContext.Current.Session["factoryLogPos"];
            return factoryLogPos;
        }

        public static Leyfi.Data.Interfaces.IFactory CreateFactoryLeyfi()
        {
            Leyfi.Data.Interfaces.IFactory factoryLeyfi;
            if (HttpContext.Current.Session["factoryLeyfi"] == null)
                HttpContext.Current.Session["factoryLeyfi"] = (Leyfi.Data.Interfaces.IFactory)Global.WindsorContainer["FactoryLeyfi"];
            factoryLeyfi = (Leyfi.Data.Interfaces.IFactory)HttpContext.Current.Session["factoryLeyfi"];
            return factoryLeyfi;
        }      

    }
}