﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateImage.aspx.cs" Inherits="Modabile.VU.UpdateImage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Scripts/jquery-tab/vanilla-tab-style.css" rel="stylesheet" type="text/css" />
    <link href="Styles/gaya.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/ui-darkness/jquery-ui-1.8.21.custom.css" />
    <script src="Scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui-1.8.21.custom.min.js" type="text/javascript"></script>
    <title>Upload Image</title>
</head>
<body style="background-color: #F0F0F0; padding: 10px">
<form id="upForm" enctype="multipart/form-data" method="post" action="UpdateImage.aspx">
<input type="file" name="newImage" />
<input type="hidden" name="mmsi" value="<%= Request.QueryString["mmsi"] %>" />
<input type="submit" value="Upload" />
</form>
</body>
</html>
