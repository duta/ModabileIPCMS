﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

namespace Modabile.VU
{
    public partial class UpdateImage : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Files.Count == 1)
            {
                try
                {
                    HttpRequest request = HttpContext.Current.Request;
                    string serDir = HttpContext.Current.Server.MapPath("Uploaded/" + request["mmsi"] + ".png");
                    request.Files[0].SaveAs(serDir);
                    Response.Write(serDir);
                    Response.Write("<script>window.opener.FindFeatureAndSelect(" + request["mmsi"] + ");window.close()</script>");
                    //return serDir;
                }
                catch (Exception ex)
                {
                    Response.Write(ex.ToString());
                }
            }
        }
    }
}