﻿<%@ Page Title="Penggambaran Objek" Language="C#" AutoEventWireup="true" CodeBehind="DrawingZone.aspx.cs" Inherits="Modabile.VU.DrawingZone" 
    ViewStateMode="Disabled" EnableViewState="false" EnableViewStateMac="false" MasterPageFile="~/Modabile.Vessel.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="theme/fullscreen/map-fullscreen.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="Styles/table-detail.css" />
    <link href="Styles/gaya.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet"  type="text/css" href="css/ui-darkness/jquery-ui-1.8.21.custom.css" />
    <link href="Styles/drawingZone.css" rel="stylesheet" type="text/css" />
    <link href="Styles/jquery.colorpicker.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="ab-header">
        <div id="logo"></div>
        <div id="toolbarDiv" class="toolbarDraw">
            <span id="toolbar" class="ui-widget-header ui-corner-all">
                <button id="cmdDialog" title="Daftar Zona">Daftar Zona</button>
            </span>
        </div>
        <div id="toolbarDrawDiv" class="toolbarDraw">
            <span id="toolbar" class="ui-widget-header ui-corner-all">
                <span id="repeatButton">
                    <input type="radio" id="cmdDrawModeOff" name="repeat" title="Matikan Mode Gambar" checked="checked" /><label for="cmdDrawModeOff">Pan</label>
                    <input type="radio" id="zoneDrawFeature" name="repeat" title="Gambar Zona Baru" /><label for="zoneDrawFeature">Draw Zone</label>
                    <input type="radio" id="zoneEntryFeature" name="repeat" title="Entri Zona Baru" /><label for="zoneEntryFeature">Entry Zone</label>
                    <input type="radio" id="zoneModifyFeature" name="repeat" title="Edit Zona" /><label for="zoneModifyFeature">Edit Zone</label>
                </span>
            </span>
        </div>
    </div>
    <!-- ui-dialog -->
	<div id="dialog" title="Daftar Zona">
        <div id="table-zona"></div>
    </div>
	<div id="entryZoneDialog" title="Entry Zona">
        <label for="txtNameEntryZona" >Nama Zona : </label><input id="txtNameEntryZona" type="text" /><br />
        <input id="chkZonaEntryWatch" type="checkbox" /><label for="chkZonaEntryWatch" >Awasi Zona</label><br />
        <input id="chkZonaEntryMasuk" type="checkbox" /><label for="chkZonaEntryMasuk" >Awasi Masuk</label><br />
        <input id="chkZonaEntryKeluar" type="checkbox" /><label for="chkZonaEntryKeluar" >Awasi Keluar</label><br />
        <label for="lonlatMode" >Input mode : </label><select class="lonlatMode"><option value="1">Degree Minute Second</option><option value="2">Decimal Degree</option></select><br />
        <ul class="sort-me" style="list-style-type: none; margin-top: 2px; padding: 0;">
        </ul>
        <button class="sort-add">Add coordinate</button>
    </div>
    <div id="savingDialog" title="Simpan Zona">
        <label for="txtNameZona" >Nama Zona</label><input id="txtNameZona" type="text" /><br />
        <input id="chkZonaWatch" type="checkbox" /><label for="chkZonaWatch" >Awasi Zona</label><br />
        <input id="chkZonaMasuk" type="checkbox" /><label for="chkZonaMasuk" >Awasi Masuk</label><br />
        <input id="chkZonaKeluar" type="checkbox" /><label for="chkZonaKeluar" >Awasi Keluar</label>        
        <input id="chkZonaVisible" type="checkbox" checked /><label for="chkZonaVisible" >Visible</label>
        <label for="colorpicker-popup" >Warna #</label><input type="text" id="colorpicker-popup" value="" />
    </div>
    <div id="mapGeoJson" ></div>
    <div id="footer2"></div>
    <script src="Scripts/jquery-ui-1.8.21.custom.min.js" ></script>
    <script src="Scripts/jquery-ui-timepicker-addon.js" ></script>
    <script src="Scripts/jquery-ui-sliderAccess.js" ></script>
	<script src="Scripts/jquery.lonlat.js" type="text/javascript"></script>
    <script src="Scripts/drawingZone.js" ></script>
    <script src="Scripts/jquery.colorpicker.js" ></script>
    <script src="Scripts/jquery.ui.colorpicker-en.js" ></script>
    <script src="Scripts/jquery.ui.colorpicker-memory.js" ></script>
    <script src="" ></script>
</asp:Content>