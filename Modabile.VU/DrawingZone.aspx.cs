﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Linq.Expressions;
using System.Threading;
using System.Reflection;
using System.Globalization;
using System.Text;
using AIS.Core;
using AIS.DataLogger;
using AIS.DataLogger.Entity;
using AIS.DataLogger.Interfaces;
using Newtonsoft.Json;
using SharpAIS;

namespace Modabile.VU
{
    public partial class DrawingZone : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod]
        public static string InsertZone(string namaZone, string geom, bool pierWatch, bool pierMasuk, bool pierKeluar,
           bool visible, string colour)
        {
            string result;
            Zone zone = new Zone();
            zone.Name = namaZone; zone.Geom = geom; zone.PierWatch = pierWatch; zone.CheckMasuk = pierMasuk; zone.CheckKeluar = pierKeluar;
            zone.Visible = visible; zone.Colour = "#" + colour.ToUpper();
            zone.UpdatedBy = UserNameLoggedIn;
            int inserted = BasePage.CreateFactoryGIS().GetZone().Insert(zone);
            result = inserted > 0 ? "true" : "false";
            return result;
        }

        [WebMethod]
        public static string UpdateZone(int id, string namaZone, string geom, bool pierWatch, bool pierMasuk, bool pierKeluar,
            bool visible, string colour)
        {
            string result;
            Zone zone = new Zone();
            zone.ID = id; zone.Name = namaZone; zone.Geom = geom; zone.PierWatch = pierWatch; zone.CheckMasuk = pierMasuk; zone.CheckKeluar = pierKeluar;
            zone.Visible = visible; zone.Colour = "#" + colour.ToUpper();
            zone.UpdatedBy = UserNameLoggedIn;
            int inserted = BasePage.CreateFactoryGIS().GetZone().Update(zone);
            result = inserted > 0 ? "true" : "false";
            return result;
        }

        [WebMethod]
        public static string DeleteZone(int id)
        {
            string result;
            int deleted = BasePage.CreateFactoryGIS().GetZone().Delete(id);
            result = deleted > 0 ? "true" : "false";
            return result;
        }
    }
}