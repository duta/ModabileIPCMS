﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using AIS.Core;
using AIS.DataLogger;
using AIS.DataLogger.Entity;
using AIS.DataLogger.Interfaces;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using DBProvider;

namespace Modabile.VU
{
    public partial class FullDesignA : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Pass) Response.Redirect("default.aspx");
            if (!IsPostBack)
            {}
        }
                
        [WebMethod]
        public static string GetAssortedStuff()
        {
            string result = "";
            List<ASPoint> zones = BasePage.CreateFactoryGIS().GetAssortedStuff().GetASPoints();
            result = new JsonHelper().ToStringJson(zones);
            return "{\"features\":" + result + ",\"type\":\"FeatureCollection\"}";
        }

        [WebMethod]
        public static string GetZone()
        {
            string result = "";
            List<Zone> zones = BasePage.CreateFactoryGIS().GetZone().GetAllZone();
            result = new JsonHelper().ToStringJson(zones);
            return result;
        }

        [WebMethod]
        public static string GetShipsAndUpdatedToClient()        
        {
            var result = HttpRuntime.Cache["shiplastweek"];
            if (result == null)
            {
                result = new JsonHelper().ToStringJson(GetShips());
                HttpRuntime.Cache.Insert("shiplastweek", result, null,
                    DateTime.Now.AddSeconds(300), System.Web.Caching.Cache.NoSlidingExpiration,
                    System.Web.Caching.CacheItemPriority.NotRemovable, null);
            }
            return result.ToString();
        }

        public static Entities.ShipDisplayFeatureCollection GetShips()
        {
            IFactory thisFactory = BasePage.CreateFactoryLogPos();
            List<ShipInfo> sil = thisFactory.GetShipInfo().GetAll();
            List<double[][]> line = new List<double[][]>();
            List<Entities.ShipFeature> all = new List<Entities.ShipFeature>();
            Entities.ShipDisplayFeatureCollection shipFeatures;

            for (int i = 0; i < sil.Count; i++)
            {                
                var topx = 1;
                shipFeatures = thisFactory.GetAISLog().RecordedPositionByMMSILast10(sil[i].mmsi.ToString(), topx);                             
                if (shipFeatures.Features.Length == 0) continue;
                all.Add(shipFeatures.Features[0]);                
            }
            return new Entities.ShipDisplayFeatureCollection(all);
        }

        [WebMethod]
        public static string GetAllCCTV()
        {
            return new JsonHelper().ToStringJson(BasePage.CreateFactoryGIS().GetCCTVPoint().GetCCTVs());
        }

        
    }
}