﻿<%@ Page Title="Modabile" Language="C#" MasterPageFile="~/Modabile.Vessel.Master"
    AutoEventWireup="true" CodeBehind="FullDesignA.aspx.cs" Inherits="Modabile.VU.FullDesignA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Scripts/jquery-tab/vanilla-tab-style.css" rel="stylesheet" type="text/css" />
    <link href="theme/fullscreen/map-fullscreen.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="Styles/table-detail.css" />
    <link rel="stylesheet" type="text/css" href="css/ui-darkness/jquery-ui-1.8.21.custom.css" />
    <link href="Styles/hover-menu.css" rel="stylesheet" type="text/css" />
    <link href="Styles/gaya.css" rel="stylesheet" type="text/css" />
    <link href="Styles/google.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-ui-1.8.21.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-tab/jquery-ui-personalized-1.5.2.packed.js" type="text/javascript"></script>
    <script src="Scripts/jquery-tab/sprinkle.js" type="text/javascript"></script>
    <script src="Scripts/jquery.timer.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.cookie.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="ab-header">
        <div id="ab-tabvanilla">
            <div id="tabvanilla" class="widget">
                <ul class="tabnav" style="">
                    <li><a href="#satu">Search</a></li>
                    <li><a href="#duwa">View</a></li>
                    <li><a href="#empat">Tools</a></li>
                    <% if (PassAdminMenu)
                       { %>
                    <li><a href="#admintabmenu">Admin</a></li>
                    <% } %>
                    <li><a href="#helptabmenu">Help</a></li>
                </ul>
                <div id="satu" class="tabdiv">
                    <div id="ab-search-area">
                        <%--<form id="search" action="" name="" method="post" enctype="multipart/form-data">--%>
                        <div id="kolom-textfield">
                            <input class="search-isian" type="text" name="reqCari" id="reqCari" value="MMSI/Nama Kapal/CallSign (min. 2 karakter)"
                                onblur="if(this.value=='') this.value='MMSI/Nama Kapal/CallSign (min. 2 karakter)'"
                                onfocus="if(this.value =='MMSI/Nama Kapal/CallSign (min. 2 karakter)' ) this.value=''" />
                        </div>
                        <div id="kolom-btnsearch">
                            <input class="search-tombol" type="submit" name="reqSubmit" id="reqSubmit" value=""
                                onclick="WriteSearchShipResult( $('#reqCari').val());return false;" />
                        </div>
                        <div id="table-search-result">
                        </div>
                        <%-- </form>--%>
                    </div>
                </div>
                <!--/satu-->
                <div id="duwa" class="tabdiv">
                    <div id="jumlahKapal" class="small-font">
                        Belum ada kapal terdeteksi
                    </div>
                    <div id="filter-data-table">
                        <table>
                            <tr>
                                <td width="100px">
                                    <input type="checkbox" id="cbShowKargo" class="check-filter-data-table" value="1" title="Kargo" checked="checked" /><label for="cbShowKargo" style="padding: 0; margin: 0;">Kargo (<span id="cfds1" class="span-ship-count"></span>)</label>
                                </td>
                                <td width="100px">
                                    <input type="checkbox" id="cbShowPenumpang" class="check-filter-data-table" value="2" title="Penumpang" checked="checked" /><label for="cbShowPenumpang" style="padding: 0; margin: 0;">Penumpang (<span id="cfds2" class="span-ship-count"></span>)</label>
                                </td>
                                <td width="100px">
                                    <input type="checkbox" id="cbShowTanker" class="check-filter-data-table" value="4" title="Tanker" checked="checked" /><label for="cbShowTanker" style="padding: 0; margin: 0;">Tanker (<span id="cfds4" class="span-ship-count"></span>)</label>
                                </td>
                            </tr>
                            <tr>
                                <td width="100px">
                                    <input type="checkbox" id="cbShowTunda" class="check-filter-data-table" value="8" title="Tunda" checked="checked" /><label for="cbShowTunda" style="padding: 0; margin: 0;">Tunda (<span id="cfds8" class="span-ship-count"></span>)</label>
                                </td>
                                <td width="100px">
                                    <input type="checkbox" id="cbShowSar" class="check-filter-data-table" value="16" title="SAR" checked="checked" /><label for="cbShowSar" style="padding: 0; margin: 0;">SAR (<span id="cfds16" class="span-ship-count"></span>)</label>
                                </td>
                                <td width="100px">
                                    <input type="checkbox" id="cbShowPetikemas" class="check-filter-data-table" value="32" title="Petikemas" checked="checked" /><label for="cbShowPetikemas" style="padding: 0; margin: 0;">Petikemas (<span id="cfds32" class="span-ship-count"></span>)</label>
                                </td>
                            </tr>
                            <tr>
                                <td width="100px">
                                    <input type="checkbox" id="cbShowPesiar" class="check-filter-data-table" value="64" title="Pesiar" checked="checked" /><label for="cbShowPesiar" style="padding: 0; margin: 0;">Pesiar (<span id="cfds64" class="span-ship-count"></span>)</label>
                                </td>
                                <td width="100px">
                                    <input type="checkbox" id="cbShowNoReg" class="check-filter-data-table" value="128" title="Tidak Terdaftar" checked="checked" /><label for="cbShowNoReg" style="padding: 0; margin: 0;">Tidak Terdaftar (<span id="cfds128" class="span-ship-count"></span>)</label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="ab-data-table-search">
                        <div class="baris-title">
                            <div class="kolom-title kolom-mmsi">
                                MMSI
                            </div>
                            <div class="kolom-title kolom-name">
                                Nama Kapal
                            </div>
                            <div class="kolom-title kolom-tipe">
                                Tipe Kapal
                            </div>
                            <div style="clear: both;">
                            </div>
                        </div>
                    </div>
                    <div class="ab-data-table">
                        <div id="table-all-ships">
                        </div>
                    </div>
                </div>
                <div id="empat" class="tabdiv">
                    <div><a href="FullGoogle.aspx" target="_new">Basemap Google Map</a></div>
                    <div><a href="FullRecorded.aspx" target="_new">Recorded</a></div>
                    <input type="checkbox" id="CBrad" onclick="tools()">Measure radius</input><br />
                    <input type="checkbox" id="CBline" onclick="tools()">Measure line</input><br />
                    <input type="checkbox" id="CBloa" onclick="loaset()">Show LOA (Panjang Kapal)</input><br />
                </div>
                <% if (PassAdminMenu)
                   { %>
                <div id="admintabmenu" class="tabdiv">
                    <% if (PassOnlyAdminMenu)
                       { %>
                    <div></div>
                    <% } %>
                </div>
                <% } %>
                <div id="helptabmenu" class="tabdiv">
                    <div><a href="UserGuide/User Guide Modabile VTIS AIS.html" target="_new">Help ?</a></div>
                </div>
            </div>
            <!--/widget-->
        </div>
        <!-- /tabvanilla -->
        <div id="logo"></div>       
    </div>
    <div id="mapGeoJson">
    </div>
    <div id="detailKapal">
    </div>
    <div id="footer2"></div>
    <div style="display: none;">
    </div>
    <script>
        lon = <%= ConfigurationManager.AppSettings["centerlon"] %>;
        lat = <%= ConfigurationManager.AppSettings["centerlat"] %>;
        gedungAis = [<%= ConfigurationManager.AppSettings["gedung-ais"] %>];
        tundaIPC = [<%= ConfigurationManager.AppSettings["tundaipc"] %>];
    </script>
    <script src="Scripts/geopoint.js" type="text/javascript"></script>
    <script src="Scripts/fullScreenMG.js" type="text/javascript"></script>
</asp:Content>