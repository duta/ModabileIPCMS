﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modabile.VU
{
    /// <summary>
    /// Summary description for RecordedKML
    /// </summary>
    public class RecordedKML : IHttpHandler
    {

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "application/vnd.google-earth.kml+xml";
			context.Response.Buffer = false;
			context.Response.AddHeader("Content-Disposition", "attachment; filename=RecordedKML_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm") + ".kml");
			context.Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?><kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\"><Document>");
			DateTime start = DateTime.ParseExact(context.Request["dateStart"], "yyyy-M-d-HH-mm", System.Globalization.CultureInfo.InvariantCulture);
			DateTime end = DateTime.ParseExact(context.Request["dateEnd"], "yyyy-M-d-HH-mm", System.Globalization.CultureInfo.InvariantCulture);
			TimeSpan interval = new TimeSpan(int.Parse(context.Request["IntervalHour"] ?? "0"), int.Parse(context.Request["IntervalMinute"] ?? "0"), int.Parse(context.Request["IntervalSecond"] ?? "0"));
			List<string> freeColour = new List<string>(colourList);
			Random r = new Random();
			foreach(var item in context.Request.Params)
			{
				if(!item.ToString().StartsWith("mmsi")) continue;
				AIS.Core.Entities.ShipDisplayFeatureCollection result;
				System.Text.StringBuilder sb = new System.Text.StringBuilder();
				int resultfeat = 0, mmsi = 0;
				string name = "";
				start = DateTime.ParseExact(context.Request["dateStart"], "yyyy-M-d-HH-mm", System.Globalization.CultureInfo.InvariantCulture);
				while(start < end)
				{
					DateTime next = start.Add(interval);
					if(next > end) next = end;
					result = ((AIS.DataLogger.Interfaces.IFactory)Global.WindsorContainer["FactoryLogPos"]).GetAISLog().RecordedPositionByMMSIAndDate(context.Request[item.ToString()], start.ToString("yyyy-M-dd HH:mm:ss"), next.ToString("yyyy-M-dd HH:mm:ss"));
					foreach(var ship in result.Features)
					{
						context.Response.Write("<Placemark>");
						context.Response.Write("<name>" + ship.Properties.ShipName + "</name>");
						context.Response.Write("<description><table><tr><td>MMSI</td><td>: " + ship.Properties.MMSI +
                            "</td></tr><tr><td>Latitude</td><td>: " + LatLonDecimalToDMS.DDtoDMS(ship.Geometry.Lat, LatLonDecimalToDMS.CoordinateType.latitude) +
                            "</td></tr><tr><td>Longitude</td><td>: " + LatLonDecimalToDMS.DDtoDMS(ship.Geometry.Lon, LatLonDecimalToDMS.CoordinateType.longitude) +
                            "</td></tr><tr><td>Speed</td><td>: " + decimal.Parse(ship.Properties.SOG) / 10 + " k" +
							"</td></tr><tr><td>Date</td><td>: " + DateTime.ParseExact(ship.Properties.TimeStampReceiver.ToString("#").Substring(0, 14), "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MM-yyyy HH:mm:ss") +
							"</td></tr></table></description>");
						context.Response.Write("<Point><coordinates>" + ship.Geometry.Lon + "," + ship.Geometry.Lat + ",0</coordinates></Point>");
						context.Response.Write("</Placemark>");
						sb.Append(ship.Geometry.Lon + "," + ship.Geometry.Lat + ",0 ");
						name = ship.Properties.ShipName;
						mmsi = ship.Properties.MMSI;
					}
					resultfeat += result.Features.Length;
					start = next;
				}
				if(resultfeat == 0) continue;
				context.Response.Write("<Style id=\"style" + item + "\">");
				context.Response.Write("<LineStyle>");
				string colour = freeColour[r.Next(freeColour.Count)];
				freeColour.Remove(colour);
				context.Response.Write("<color>" + colour + "</color>");
				context.Response.Write("<width>4</width>");
				context.Response.Write("<gx:labelVisibility>1</gx:labelVisibility>");
				context.Response.Write("</LineStyle>");
				context.Response.Write("</Style>");
				context.Response.Write("<Placemark>");
				context.Response.Write("<name>" + name + "</name>");
				context.Response.Write("<styleUrl>#style" + item + "</styleUrl>");
				context.Response.Write("<description><table><tr><td>MMSI</td><td>: " + mmsi + "</td></tr></table></description>");
				context.Response.Write("<LineString>");
				context.Response.Write("<coordinates>" + sb.ToString() + "</coordinates>");
				context.Response.Write("</LineString>");
				context.Response.Write("</Placemark>");
			}
			context.Response.Write("</Document>");
			context.Response.Write("</kml>");
		}

		private string[] colourList = {"7F000000","7F00FF00","7F0000FF","7FFF0000","7FFEFF01",
                                          "7FFEA6FF","7F66DBFF","7F016400","7F670001","7F3A0095",
                                          "7FB57D00","7FF600FF","7FE8EEFF","7F004D77","7F92FB90",
                                          "7FFF7600","7F00FFD5","7F7E93FF","7F6C826A","7F9D02FF" };

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}